<!-- Portion de Vue spécifique à l'affichage du bandeau -->

	<!-- Pseudo de l'utilisateur -->
	<li class="disabled"><a href="#"><span class="glyphicon glyphicon-user"></span> <?=$this->nettoyer($pseudo) ?></a></li>

	<!-- lien de deconnexion de l'utilisateur -->
	<?php if ($connexion === "déconnexion"):?>
	    <li><a href="<?= "connexion/deconnecter/"?>"><?= $this->nettoyer($connexion) ?></a></li>
	<?php else :?>
	    <li><a href="<?= "connexion"?>"><?= $this->nettoyer($connexion) ?></a></li>
	<?php endif;?>

	<!-- Etat du panier -->
	<li class="disabled"><a href="#">Panier</a></li>

