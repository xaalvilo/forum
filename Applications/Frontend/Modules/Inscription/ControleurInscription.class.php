<?php
/**
 *
 * @author Frédéric Tarreau
 *
 * 14 dec. 2014 - ControleurInscription.class.php
 *
 * la classe ControleurUser hérite de la classe abstraite Controleur du framework.
*  elle utilise également la methode genererVue pour générer la vue associée à l'action
*  Elle permet de :
*  - créer un nouveau compte pour accès à l'espace membre via l'action "enregistrer"
*  - inscrire un visiteur du blog via l'action "inscrire"
*  - supprimer un compte
*  - modifier les données d'un compte
 *
 */
namespace Applications\Frontend\Modules\Inscription;
use Framework\Modeles\ManagerUser;
require_once './Framework/autoload.php';

class ControleurInscription extends \Framework\Controleur
{
    /* @var Gestionnaire de Login */
    protected $_loginHandler;

    /* @var gestionnaire de mail */
    protected $_mailHandler;

    /* @var gestionnaire de téléchargement de fichier */
    protected $_uploadFileHandler;

    /* @var manager de billets et de commentaires */
    private $_managerBillet;
    private $_managerCommentaire;

    /**
    * le constructeur instancie les classes "mod�les" requises
    * Il est notamment nécessaire d'instancier un loginHandler pour générer un hash
    *
    */
    public function __construct(\Framework\Application $app)
    {
    	parent::__construct($app);
        $this->_loginHandler = new \Framework\LoginHandler($app);
        $this->_mailHandler = new \Framework\MailHandler($app);
        $this->_managerBillet= \Framework\FactoryManager::createManager('Billet');
        $this->_managerCommentaire= \Framework\FactoryManager::createManager('Commentaire');
    }

    /**
    *
    * Méthode index
    *
    * Action par d�faut consistant � afficher le formulaire de création d'un compte
    * la vue associée donne un lien vers le module d'Inscription d'un nouvel utilisateur (module User)
    *
    * @param array $donnees tableau de données éventuellement passé en paramètre, permettant d'afficher dans le formulaire les champs valides saisis lors d'une
    * requête précédente
    *
    */
    public function index(array $donnees = array())
    {
        $forms = array();
        if(!$this->_app->securiteHandler()->verifConnexion()){ //nouveau compte

            $type1 = \Framework\Formulaire\FormBuilder::LONG;// type de formulaire pour formbuilder

            $allValeurForm[0] = array('methode'=>'post','action'=>'inscription/enregistrer','enctype'=>'multipart/form-data','type'=>$type1);// tableau des valeurs pour le formulaire
        }
        else if($this->_app->securiteHandler()->verifPermission('membre')) {// modification de compte

        	$type1 = \Framework\Formulaire\FormBuilder::MOYEN;// spécification du type de formulaire pour  le formbuilder
        	$type2 = \Framework\Formulaire\FormBuilder::PETIT;

        	//modification par admin
        	if($this->_app->securiteHandler()->verifPermission('admin')
            	&& ($this->_requete->existeParametre('id')|| array_key_exists('idMembre',$donnees))){
        		if ($this->_requete->existeParametre('id')){
        			$idMembre = $this->_requete->getParametre('id');
        			$membre=$this->_app->userHandler()->managerUser()->getUser($idMembre);
        		} else {
        			$idMembre = $donnees['idMembre'];
        			$membre=$this->_app->userHandler()->managerUser()->getUser($idMembre);
        		}

        		$allValeurForm[0] = array ('nom'=>$membre['_nom'],// même tableau avec les données du membre selectionné par l'administrateur
        							'prenom'=> $membre['_prenom'],
        							'mail'=>$membre['_mail'],
        							'telephone'=>$membre['_telephone'],
        							'pays'=>$membre['_pays'],
        							'naissance'=>$membre['_naissance'],
        							'avatar'=>$membre['_avatar'],
        							'value'=>$idMembre,
        							'methode'=>'post',
        							'action'=>'inscription/modifierData',
        							'type'=>$type1);

        		$allValeurForm[1] = array('pseudo'=> $membre['_pseudo'],
        							'value'=>$idMembre,
        							'methode'=>'post',
        							'action'=>'inscription/modifierLogin',
        							'type'=>$type2);

        		$donneesVue['avatar'] = $membre['_avatar']; // pour affichage de l'image
        	}
        	// modification par l'utilisateur
        	else {
        		$allValeurForm[0] = array ('nom'=>$this->_app->userHandler()->user()->nom(),// même tableau avec les données de l'utilisateur connecté
                                    'prenom'=> $this->_app->userHandler()->user()->prenom(),
                                    'mail'=>$this->_app->userHandler()->user()->mail(),
                                    'telephone'=>$this->_app->userHandler()->user()->telephone(),
                                    'pays'=>$this->_app->userHandler()->user()->pays(),
                                    'naissance'=>$this->_app->userHandler()->user()->naissance(),
                                    'avatar'=>$this->_app->userHandler()->user()->avatar(),
                                    'methode'=>'post',
        							'enctype'=>'multipart/form-data',
                                    'action'=>'inscription/modifierData',
                                    'type'=>$type1);

        		$allValeurForm[1] = array('pseudo'=> $this->_app->userHandler()->user()->pseudo(),
                                   'methode'=>'post',
                                   'action'=>'inscription/modifierLogin',
                                   'type'=>$type2);

        		$donneesVue['avatar'] = $this->_app->userHandler()->user()->avatar(); // pour affichage de l'image
        	}
        	$forms[1]=$this->initForm('User',$allValeurForm[1]);// création du formulaire de modification de login

        	$allValeurForm[2] = array('methode'=>'post','action'=>'inscription/supprimer'); // creation d'un formulaire limité à un bouton
        	$forms[2]=$this->initForm(NULL,$allValeurForm[2]);
        }

        if(!empty($donnees))
            $allValeurForm[0]=array_merge($allValeurForm[0],$donnees);

        $forms[0]=$this->initForm('User',$allValeurForm[0]);

        for($i=0;$i<count($forms);$i++)
            $donneesVue['formulaire'.$i]=$forms[$i]->createView();

        $vue = $this->genererVue($donneesVue);
        $this->envoyerReponse(array('vue'=>$vue));
    }

    /**
     *
     * Méthode enregistrer
     *
     * cette méthode correspond à l'action "enregistrer" permettant de confirmer les éléments de création d'un compte de l'utilisateur
     * et de les stocker en BDD
     * Elle ne doit être exécutée que si les données insérées dans le formulaire sont valides
     * la vérification de l'existence d'un même utilisateur (pseudo et mail ) en BDD est laissée au manager en utilisant les index UNIQUE de MySQL
     *
     */
    public function enregistrer()
    {
        if(!$this->_app->securiteHandler()->verifConnexion()){

            $type = \Framework\Formulaire\FormBuilder::LONG;// spécification du type de formulaire pour  le formbuilder

            // récupération des paramètres de la requête
            $allNoms = array('nom','prenom','pays','naissance','mail','telephone','pseudo','avatar','mdp');
            $allParams = $this->_requete->getAllParametres($allNoms);

            $this->_uploadFileHandler = new \Framework\UploadFileHandler($this->_app,'avatar');
            if($this->_uploadFileHandler->isValid())
            	$allParams['avatar'] = strval(rand(1,200)).$this->_uploadFileHandler->getFilename(); // ajout d'une chaîne aléatoire dans le nom

            $allValeurForm = array('methode'=>'post','action'=>'inscription/enregistrer','type'=>$type);
            $allValeurForm = array_merge($allValeurForm, $allParams);

            // création du formulaire d'inscription en l'hydratant avec les valeurs de la requête
            $form=$this->initForm('User',$allValeurForm);

            $options = array();

            if (($this->_requete->getMethode() ==='POST')) { // si la methode est bien POST et que le formulaire est valide, enregistrement en BDD

                if ($form->isValid()){ // le formulaire est valide
                    // initialisation de certaines données
                    //TODO a changer une fois que validation par mail
                    $statut = \Framework\Configuration::get('candidat',1);
                    $lienValidation = \Framework\Configuration::get('lienValidation');
                    $nbreCommentairesBlog = 0;
                    $nbreCommentairesForum = 0;
                    $nbreBilletsForum = 0;
                    $mdp = $allParams['mdp'];
                    unset($allParams['mdp']);
                    $pseudo = $allParams['pseudo'];
                    $mail = $allParams['mail'];
                    if(empty($allParams['avatar']))
                    	$allParams['avatar'] =\Framework\Configuration::get('nomAvatarParDefaut');

                    $hash = $this->_loginHandler->creerHash($mdp);
                    $hashValidation = $this->_loginHandler->creerHash($pseudo);
                    $ip = $this->_app->userHandler()->getUserIp();
                    $browser = $this->_app->userHandler()->getUserBrowserVersion();

                    if(is_int($result=$this->_app->securiteHandler()->blackListHandler()->checkBlackList($ip,$browser)))
                        $this->_app->securiteHandler()->blocageInscription($result);

                    // enregistrer le nouvel utilisateur en BDD
                    else {
                        $params = array_values($allParams);
                        $params = array_merge($params,array($statut,$nbreCommentairesBlog,$nbreCommentairesForum,$nbreBilletsForum,$ip,$hash,$hashValidation));
                        $idUser = $this->_app->userHandler()->managerUser()->ajouterUser($params);

                        if(ctype_digit($idUser)){// il n'y a pas de doublon
                            $contenu = \Framework\Configuration::get('contenuPreInscription');
                            $objet = \Framework\Configuration::get('objetForum');
                            $lien['url']= $lienValidation.$idUser;
                            $lien['nom']='ici';

                            if($this->_mailHandler->myMail($mail, $objet, $contenu, $lien)){ // envoi de mail
                                $this->_app->userHandler()->setFlash('Vous êtes enregistré, vous allez recevoir un email de validation');
                                $this->_app->userHandler()->initUser($idUser);

                                if($this->_uploadFileHandler->isValid()){
                                	$uploadFile = $_SERVER['DOCUMENT_ROOT']
                                				.substr_replace(\Framework\Configuration::get('racineWeb'),'',0,1)
                                				.\Framework\Configuration::get('repertoireUploads')
                                				.$allParams['avatar'];
                                	$this->_uploadFileHandler->moveTo($uploadFile);
                                }

                                //TODO il faut rediriger sur la page d'accueil dans l'attente de la validation par mail

                                //FIXME pour test redirection interne vers la methode valider avec l'id
                                $donnees = array('hashValidation'=>$hashValidation,'id'=>$idUser);
                                $this->executerAction("valider",$donnees);

                            } else { //echec envoi du mail
                                $this->_app->userHandler()->setFlash('Problème d\'envoi de l\'email de validation, merci de recommencer');
                                $this->_app->userHandler()->managerUser()->supprimerUser($idUser);

                                if($this->_uploadFileHandler->isValid())
                                	$this->_uploadFileHandler->deleteFile();// destruction du fichier avatar téléchargé
                                $this->executerAction("index",$allParams);
                            }
                        } else {// il y a un doublon en BDD
                        	if($this->_uploadFileHandler->isValid())
                        		$this->_uploadFileHandler->deleteFile();// destruction du fichier avatar téléchargé

                            $options=$form->validField();// préparer le pré remplissage du formulaire avec les champs valides
                            $options = array_merge($options,$this->traiteDoublon($idUser,$pseudo,$mail));
                            $this->executerAction("index",$options);
                        }
                    }
                } else { // le formulaire est invalide
                    $options=$form->validField();

                    unset($options['mdp']); // le champ de password ne devra pas être pré-rempli
                    if($this->_uploadFileHandler->isValid())
                    	$this->_uploadFileHandler->deleteFile();// destruction du fichier avatar téléchargé

                    $this->_app->userHandler()->setFlash("L'inscription a échoué, certains champs ne sont pas valides");
                    $this->executerAction("index",$options);
                 }
             } else {// la requete ne contenait pas de méthode POST
                 $this->_app->userHandler()->setFlash("La requête d'inscription a échoué");
                 $this->executerAction("index",$options);
             }
        } else $this->notify(array('code'=>\Framework\SecuriteHandler::ALREADY_CONNECTED));
    }

    /**
     *
     * Méthode modifierData
     *
     * Correspond à l'action "modifierData" permettant à un membre ou à l'administrateur de modifier les données d'un compte
     * hors éléments du login/mdp. le membre doit déjà être connecté.
     *
     */
    public function modifierData()
    {
        if($this->_app->securiteHandler()->verifConnexion()){
            $type = \Framework\Formulaire\FormBuilder::MOYEN;// spécification du type de formulaire pour le formbuilder

            $options = array();

            if($this->_app->securiteHandler()->verifPermission('admin') && $this->_requete->existeParametre('idMembre')){
            	$idUser = $this->_requete->getParametre('idMembre');
            	$modifParAdmin=TRUE;
            	$options['idMembre']=$idUser;
            }
            else $idUser =  $this->_app->userHandler()->user()->id();

            // récupération des paramètres de la requête
            $allNoms = array('nom','prenom','pays','naissance','mail','telephone','avatar');
            $allParams = $this->_requete->getAllParametres($allNoms);

			$this->_uploadFileHandler = new \Framework\UploadFileHandler($this->_app,'avatar');
			if($this->_uploadFileHandler->isValid())
				$allParams['avatar'] = strval(rand(1,200)).$this->_uploadFileHandler->getFilename(); // ajout d'une chaîne aléatoire dans le nom

			$allValeurForm = array('methode'=>'post','action'=>'inscription/modifier','type'=>$type);
            $allValeurForm = array_merge($allValeurForm, $allParams);

            // création du formulaire d'inscription en l'hydratant avec les valeurs de la requête
            $form = $this->initForm('User',$allValeurForm);

            // si la methode est bien POST
            if (($this->_requete->getMethode()==='POST')){
                // le formulaire est valide
                if ($form->isValid()){
                    //récupération des attributs modifiés
                    $allModifs =[];
                    if(isset($modifParAdmin) && $modifParAdmin){
                    	$membre = $this->_app->userHandler()->managerUser()->getUser($idUser);
                    	$ancienAvatar = $membre['_avatar']; // conserver le nom de l'avatar pour détruire le fichier en cas de modification d'avatar
                    	foreach ($allParams as $cle=>$valeur){
                    		$cle = '_' .$cle; // necessaire en raison convention de nommage des attributs protégés
                    		if ($membre[$cle]!= $valeur)
                    			$allModifs[$cle] = $valeur;
                    	}
                    } else {
                    	foreach ($allParams as $cle=>$valeur){
                    		if ($this->_app->userHandler()->user()->{$cle}() != $valeur){
                    			if($cle == 'avatar') // conserver le nom de l'avatar pour détruire le fichier après modification
                    				$ancienAvatar=$this->_app->userHandler()->user()->avatar();
                    			$cle = '_' .$cle; // necessaire en raison convention de nommage des attributs protégés
                    			$allModifs[$cle] = $valeur;
                    		}
                    	}
                    }

                    if (empty($allModifs)) {
                        $this->_app->userHandler()->setFlash('Aucune donnée modifiée');
                        $this->executerAction("index", $options);
                    }
                    // il y a des modifications
                    else {
                        $objet = \Framework\Configuration::get('objetForum');// champ "objet" du mail  d'information ou de confirmation

                        // si modification de l'image de l'avatar
                        if (array_key_exists('_avatar', $allModifs)){
                        	if($this->_uploadFileHandler->isValid()){
                        		$uploadFile = $_SERVER['DOCUMENT_ROOT']
                        		.substr_replace(\Framework\Configuration::get('racineWeb'),'',0,1)
                        		.\Framework\Configuration::get('repertoireUploads')
                        		.$allParams['avatar'];
                        		$this->_uploadFileHandler->moveTo($uploadFile);

                        		if(array_key_exists('_pseudo', $allModifs))
                        			$allModifs['_avatar'] = $this->creerImageAvatar($allModifs['_avatar'],$allModifs['_pseudo']);
                        		else {
                        			if(isset($modifParAdmin) && $modifParAdmin)
                        				$allModifs['_avatar'] = $this->creerImageAvatar($allModifs['_avatar'],$membre['_pseudo']);
                        			else $allModifs['_avatar'] = $this->creerImageAvatar($allModifs['_avatar'],$this->_app->userHandler()->user()->pseudo());
                        		}
                        	} else throw new \Framework\Exceptions\ControleurException('incoherence sur modification avatar');
                        }

                        // si modification de l'adresse email
                        if (array_key_exists('_mail',$allModifs)) {
                            // mise à jour préalable des autres données
                            if (count($allModifs)>1) {
                                $contenu = \Framework\Configuration::get('contenuModifCompte');
                                $mail = $allParams['mail'];

                                // prise en compte des modifications sauf email
                                if ($this->_app->userHandler()->managerUser()->actualiserUser($idUser, array_replace($allModifs,array('_mail'=>$mail)))) {
                                	if(!isset($modifParAdmin)){
                                		$this->_app->userHandler()->initUser($idUser);
                                		$this->_mailHandler->myMail($mail, $objet, $contenu);
                                	}
                                	if($this->_uploadFileHandler->isValid() && array_key_exists('_avatar', $allModifs))
                                		unlink(\Framework\Configuration::get('repertoireImagesAvatar').$ancienAvatar);
                                			//TODO peut être le signaler à l'admin ou le logguer ...une exception planterait le système
                                }
                                // TODO le else ???
                            }

                           // procédure de mise à jour de l'email sauf si effectué par l'admin
                           if(!isset($modifParAdmin)){
                           		$contenu = \Framework\Configuration::get('contenuModifSecure');
                           		$url = \Framework\Configuration::get('lienValidation');
                           		$hashValidation = $this->_loginHandler->creerHash($this->_app->userHandler()->user()->pseudo());
                           		$this->_app->userHandler()->managerUser()->actualiserUser($idUser,array('_hashValidation'=>$hashValidation));

                           		$lien['nom']='ici';
                           		//TODO il faudra éventuellement changer le htaccess
                           		$lien['url']= $url.'/'.$hashValidation;
                           		$newUserMail = $allModifs['_mail'];

                           		if($this->_mailHandler->myMail($newUserMail, $objet, $contenu,$lien)) {
                           			$this->_app->userHandler()->setFlash('vous allez recevoir un email de validation');

                           			// TODO pour test en l'absence de validation par mail
                           			// normalement vaut :
                           			// $this->executerAction("index", $options);
                           			$donnees = array('hashValidation'=>$hashValidation,'id'=>$idUser,'mail'=>$newUserMail);
                           			$this->executerAction("valider",$donnees);
                           		} else {
                           			$this->_app->userHandler()->setFlash('Problème d\'envoi de l\'email de validation, merci de recommencer');
                           			$this->_app->userHandler()->managerUser()->actualiserUser($idUser,array('_hashValidation'=>NULL));
                           			$this->executerAction("index", $options);
                           		}
                           } else {
                           		$this->_app->userHandler()->setFlash('Données du compte mises à jour');
                           		$this->executerAction("index", $options);
                           }

                        } else { // seules modifications de données non sensibles

                            if ($this->_app->userHandler()->managerUser()->actualiserUser($idUser, $allModifs)){
                            	if(!isset($modifParAdmin)){
                            		$contenu = \Framework\Configuration::get('contenuModifCompte');
                            		$mail = $allParams['mail'];

                            		$this->_app->userHandler()->initUser($idUser);
                            		$this->_mailHandler->myMail($mail, $objet, $contenu);
                            	}
                            	if($this->_uploadFileHandler->isValid() && array_key_exists('_avatar', $allModifs))
                            		unlink(\Framework\Configuration::get('repertoireImagesAvatar').$ancienAvatar);
                            	//TODO peut être le signaler à l'admin ou le logguer ...une exception planterait le système
                            	$this->_app->userHandler()->setFlash('Données du compte mises à jour');
                                $this->executerAction("index", $options);
                            }
                            //TODO le else ...
                        }
                    }
                }
                // le formulaire est invalide
                else {
                    $options=$form->validField();
                    $this->_app->userHandler()->setFlash('Format de données saisies invalide');
                    $this->executerAction("index",$options);
                }
            }
            // la requete ne contenait pas de méthode POST
            else {
                $this->_app->userHandler()->setFlash("Echec de la modification");
                $this->executerAction("index",$options);
            }
        }
        // l'utilisateur n'est pas connecté - fonctionnement anormal
        else $this->notify(array('code'=>\Framework\SecuriteHandler::NOT_CONNECTED));
    }

    /**
     *
     * Méthode valider
     *
     * Traite la confirmation par mail réalisée par le membre qui créé un compte ou modifie ses données
     * sensibles (mdp, pseudo, email)
     *
     * TODO pas de param normalement excepté pour tester la methode sans le mail
     */
    public function valider($donnees)
    {
        $objet = \Framework\Configuration::get('objetForum');
        $isValid = FALSE ;
        // TODO en version avec mail
        // if($this->_requete->existeParametre('hashValidation')
        //      && $this->_requete->existeParametre('id')) {
        //   $this->_requete->getParametre('hashValidation')
        //   $this->_requete->getParametre('id') }
        $hashValidation = $donnees['hashValidation'];
        $idUser = $donnees['id'];

        // le hash de validation est correct
        if($this->_app->userHandler()->managerUser()->validerUser($hashValidation)){
            $now= $odate = new \DateTime();
            $delai = \Framework\Configuration::get('delaiInscription',1);
            $delai = 'P'.$delai.'D';
            $limit = $odate->sub(new \DateInterval($delai))->format('Y-m-d H:i:s');

            $user = $this->_app->userHandler()->managerUser()->getUser($idUser);

            // validation d'une inscription
            if($user['_statut']==2 && $user['_dateInscription']>$limit){

            	$nomAvatarParDefaut = \Framework\Configuration::get('nomAvatarParDefaut');
            	$nomRepertoireAvatar = \Framework\Configuration::get('repertoireImagesAvatar');

            	if(($user['_avatar'] != $nomAvatarParDefaut) || (!is_file($nomRepertoireAvatar .$nomAvatarParDefaut)))
            		$avatar = $this->creerImageAvatar($user['_avatar'],$user['_pseudo']);
            	else $avatar = $nomAvatarParDefaut;

                $newVals= array('_statut'=>\Framework\Configuration::get('membre',3),
                				'_dateInscription'=>$now,
                				'_avatar'=>$avatar);

                $this->_app->userHandler()->ManagerUser()->actualiserUser($idUser,$newVals);

                $mail = $user['_mail'];
                $contenu = \Framework\Configuration::get('contenuInscriptionOk');
                $this->_mailHandler->myMail($mail, $objet, $contenu);

                $adminMail = \Framework\Configuration::get('adminMail');
                $adminContenu = 'Nouveau membre ' .$user['_pseudo'].' inscrit';
                $this->_mailHandler->myMail($adminMail, $objet, $adminContenu);

                $isValid = TRUE;
            }
            // validation d'un changement de mail
            // TODO avec le mail il s'agira plutôt de $this->_requete->existeParametre('mail')
            elseif ($user['_statut']>2 && array_key_exists('mail',$donnees)){
                $newVals=array('_mail'=>$donnees['mail']);
                if(is_array($erreur=$this->_app->userHandler()->ManagerUser()->actualiserUser($idUser,$newVals))){
                    $options = $this->traiteDoublon($erreur,NULL,$donnees['mail']);
                    $this->executerAction("index",$options);
                } else $isValid=TRUE;
            }
        } else {
            //TODO
            echo "validation pas bon";
            //TODO si delai dépasser détruire en bdd les données membre d'inscription'
        }
        // la validation s'est bien déroulée
        if($isValid){
            $this->_app->userHandler()->initUser($idUser);
            $this->_app->routeur()->routerInterne($this->_app,array('controleur'=>'Connexion', 'action'=>'','id'=>''));
        }
    }

    /**
     *
     * Méthode creerImageAvatar
     *
     * créé une image redimensionnée à partir d'un fichier téléchargé et détruit le fichier original
     *
     * @param string $filename
     * @param string $pseudo
     * @return string $newFilename
     * @throws \InvalidArgumentException
     */
    private function creerImageAvatar($filename ="",$pseudo ="")
    {
    	if(!empty($filename) && !empty($pseudo)) {
    		if (is_string($filename) && is_string($pseudo)){
    			if($this->_uploadFileHandler->isValid())
    				$newFilename = $pseudo .'_avatar_'.$filename;
    			else $newFilename = \Framework\Configuration::get('nomAvatarParDefaut');
    		} else throw new \InvalidArgumentException('les paramètres doivent être de type string');
    	} else $newFilename = \Framework\Configuration::get('nomAvatarParDefaut');

    	$newFilenameWithPath = \Framework\Configuration::get('repertoireImagesAvatar').$newFilename;
    	$uploadFile = \Framework\Configuration::get('repertoireUploads').$filename;

    	$image = new \Framework\Entites\Image(array('filename'=>$uploadFile));

    	$largeurMax = \Framework\Configuration::get('largeurMaxAvatar');
    	$hauteurMax = \Framework\Configuration::get('hauteurMaxAvatar');

		$image->resize($largeurMax,$hauteurMax);
		$image->saveImageAs($newFilenameWithPath);

		if($this->_uploadFileHandler->isValid())
			$this->_uploadFileHandler->deleteFile($uploadFile);

		return $newFilename;
    }


    /**
     *
     * Méthode modifierLogin
     *
     * action permettant de modifier le login et/ou le mot de passe du membre
     *
     */
    public function modifierLogin()
    {
        if (array_key_exists('mdp', $allModifs)) {
            // TODO condition validation de new mdp
            // TODO prepa maj du mdp si ok
        }
    }

    /**
     *
     * Méthode supprimer
     *
     * action de suppression du compte d'un membre
     *
     */
    public function supprimer()
    {
        if($this->_app->securiteHandler()->verifConnexion()){
            $idUser = $this->_app->userHandler()->getAttribute('user','id');
            $user = $this->_app->userHandler()->managerUser()->getUser($idUser);

            if($this->_app->userHandler()->managerUser()->supprimerUser($idUser)){
                $objet = \Framework\Configuration::get('objetForum','Forum Atelier Blanc Nord Ouest');
                $contenu = \Framework\Configuration::get('contenuSuppressionCompte','Compte supprimé');
                $adminMail = \Framework\Configuration::get('adminMail');
                $statutMembre = \Framework\Configuration::get('membre',3);
                $nomAvatarParDefaut = \Framework\Configuration::get('nomAvatarParDefaut');
                $nomRepertoireAvatar = \Framework\Configuration::get('repertoireImagesAvatar');

                $userMail = $this->_app->userHandler()->user()->mail();
                $this->_mailHandler->myMail($userMail,$objet,$contenu);

                $pseudo = $this->_app->userHandler()->getAttribute('user','pseudo');
                $contenu.= ' - Cette suppression émane de' .$pseudo;

                if($user['_avatar'] != $nomAvatarParDefaut) // il ne faut pas détruire l'avatar par défaut
                	if(!unlink(\Framework\Configuration::get('repertoireImagesAvatar').$user['_avatar']))
                		$contenu.= '- Echec de suppression du fichier avatar';

                $this->_mailHandler->myMail($adminMail,$objet,$contenu);
            } else $this->_app->userHandler()->setFlash('Echec suppression du compte, contacter l\'administrateur');

            // attribution au membre "anonyme" de tous les billets et commentaires associés à cet utilisateur
            if(!$tableauResultat=$this->_app->userHandler()->managerUser()->recherchePseudo('anonyme')){
                // TODO changer adresse mail
            	if(!is_file($nomRepertoireAvatar .$nomAvatarParDefaut))
            				$avatar = $this->creerImageAvatar();
            	else $avatar = $nomAvatarParDefaut;

                $anonyme = array ('anonyme','anonyme',NULL,NULL,'toto.fdr@wanadoo.fr',NULL,'anonyme',$avatar,$statutMembre,0,
                        0,0,'127.0.0.1',NULL,NULL);
                //TODO a traiter si erreur ...normalement traité par MyExceptionPDO
                $idAnonyme = $this->_app->userHandler()->managerUser()->ajouterUser($anonyme);
            } else $idAnonyme = $tableauResultat['id'];

            $this->_managerBillet->anonymiserBillets($idAnonyme,$idUser);
            $this->_managerCommentaire->anonymiserCommentaires($idAnonyme,$idUser);

            $this->_app->userHandler()->detruireSession();
          //  $session= new \Framework\Entites\Session();
        } else $this->notify(array('code'=>\Framework\SecuriteHandler::NOT_CONNECTED));
        $this->_app->routeur()->routerInterne($this->_app,array('controleur'=>'Accueil','action'=>'','id'=>''));
    }

    /**
     *
     * Méthode inscrire
     *
     * Correspond à l'action "inscrire" permettant de confirmer les éléments d'inscription de l'utilisateur
     * pour le forum et de les stocker en BDD
     * Elle ne doit être exécutée que si les données insérées dans le formulaire sont valides
     * la vérification de l'existence d'un même utilisateur (pseudo et mail ) en BDD est laissée au manager en utilisant les index UNIQUE de MySQL
     *
     */
    public function inscrire()
    {
        $options = array();
        // si la methode est bien POST
        if (($this->_requete->getMethode() =='POST')){
            // récupération des données de la requête
            $pseudo = $this->_requete->getParametre("pseudo");
            $mail = $this->_requete->getParametre("mail");

            //TODO controler si pas un user banni $pseudo + mail
            //spécification du type de formulaire pour  le formbuilder
            $type = \Framework\Formulaire\FormBuilder::COURT;

            // tableau des valeurs à prendre en compte pour le formulaire
            $tableauValeur = array ('pseudo'=> $pseudo,
                                    'mail'=>$mail,
                                    'methode'=>'post',
                                    'action'=>'inscription/inscrire',
                                    'type'=>$type);

            // création du formulaire d'inscription en l'hydratant avec les valeurs de la requête
            $form=$this->initForm('User',$tableauValeur);

            // le formulaire est valide
            if ($form->isValid()){
                $statut = \Framework\Configuration::get('visiteur');
                $nbreCommentairesBlog = 0;

               // préparation du tableau de paramètre pour la requête
                $param = array ($statut, $pseudo,$mail,$nbreCommentairesBlog);

                // inscrire le nouvel utilisateur en BDD
                $idUser = $this->_app->userHandler()->managerUser()->ajouterUser($param);

                // s'il n'y a pas de doublon
                if(ctype_digit($idUser)){
                    // hydratation de l'instance User créée par le UserHandler avec l'ensemble des donnees
                    $donneesUser = $this->_app->userHandler()->managerUser()->getUser($idUser);
                    //TODO pluto initUser
                    $this->_app->userHandler()->user()->hydrate($donneesUser);

                    //l'utilisateur est autorisé
                    $this->_app->userHandler()->setUserAutorised();

                    // remplissage de la variable $_SESSION
                    $this->_app->userHandler()->peuplerSuperGlobaleSession(array('user'=>array('pseudo'=>$user->pseudo(),
                                                                                 'id'=>$user->id(),
                                                                                 'statut'=>$user->statut())));

                    $this->_app->userHandler()->setFlash('Vous êtes inscrit');
                    $this->_app->routeur()->routerInterne($this->_app,array('controleur'=>'Article','action'=>'','id'=>''));
                }
                // s'il y a un doublon ( existe déjà en BDD)
                else {
                	// préparer le pré remplissage du formulaire avec les champs valides
                	$options=$form->validField();
                	$options = array_merge($options,$this->traiteDoublon($idUser,$pseudo,$mail));
                    $this->executerAction("index",$options);
                }
             }
             // le formulaire est invalide
             else {
                //il s'agit  d'executer l'action par d�faut permettant d'afficher à nouveau le formulaire d'inscription
                // pré rempli avec les champs valides
                 $options=$form->validField();

                 //envoyer un flash de tentative d'inscription invalide à l'utilisateur
                 $this->_app->userHandler()->setFlash("L'inscription a échoué, certains champs sont invalides");
// TODO ICI
                 $this->executerAction("index",$options);
             }
         }
         // la requete ne contenait pas de méthode POST
         else {
             $this->_app->userHandler()->setFlash("La requête d'inscription a échoué");
// TODO ICI
             //il s'agit  d'executer l'action par d�faut permettant d'afficher à nouveau le formulaire d'inscription
             $this->executerAction("index",$options);
         }
    }

    /**
     *
     * Méthode traiteDoublon
     *
     * traite le doublon en BDD
     *
     * @param array $erreur retourné par la BDD MySQL
     * @param string $pseudo
     * @param string $mail
     * @return array $options tableau des paramètre remis à zéro car déjà utilisé en BDD
     */
    private function traiteDoublon($erreur,$pseudo=NULL,$mail=NULL)
    {
    	// TODO peut etre à déplacer dans la classe abstraite ???
        $options = array();
        // On vérifie que l'erreur concerne bien un doublon - Le code d'erreur 23000 signifie "doublon" dans le standard ANSI SQL
        if (23000 == $erreur[0]){
            // expression rationnelle appliquée sur le message d'erreur SQL pour rechercher la valeur à l'origine du doublon
            preg_match("`Duplicate entry '(.+)' for key`", $erreur[2], $valeur_probleme);

            $valeur_probleme = $valeur_probleme[1];

            if (!empty($pseudo) && $pseudo == $valeur_probleme){
                $this->_app->userHandler()->setFlash("Ce pseudo est déjà utilisé.");
                $options['pseudo'] ='';
            } else if (!empty($mail) && $mail == $valeur_probleme){
                $this->_app->userHandler()->setFlash("Cette adresse e-mail est déjà utilisée.");
                $options['mail'] ='';
            } else {
                $this->_app->userHandler()->setFlash("Doublon non identifié dans la base de données, reéssayez.");
                $options['pseudo'] ='';
                $options['mail'] ='';
            }
        } else  $this->_app->userHandler()->setFlash("L'inscription a échoué, merci de recommencer.");
        return $options;
    }
}