<?php $this->_titre = "Inscription AtelierBlancNordOuest"; ?>
<div class="col-sm-12">
    <div class="row">
    <?php if(!empty($formulaire1) && (!empty($formulaire2))):?>
        <div class="col-sm-6">
    		<div class="col-sm-4"><img src="Contenu/Images/Avatar/<?= $this->nettoyer($avatar)?>"  alt="avatar" id="photo"/></div>
            <div class="col-sm-offset-1 col-sm-10 well"><?= $this->nettoyer($formulaire0)?></div>
        </div>
         <div class="col-sm-6">
            <div class="col-sm-offset-1 col-sm-10 well"><?= $this->nettoyer($formulaire1)?></div>
            <div class="col-sm-offset-1 col-sm-10 well"><?= $this->nettoyer($formulaire2)?></div>
        </div>
    <?php else:?>
        <div class="col-sm-10">
            <div class="col-sm-offset-1 col-sm-10 well"><?= $this->nettoyer($formulaire0)?></div>
        </div>
    <?php endif;?>
    </div>
</div>
