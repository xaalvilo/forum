<?php
namespace Applications\Frontend\Modules\Membres;
require_once './Framework/autoload.php';
/**
 *
 * @author Frédéric Tarreau
 *
 * 29 mars 2015 - ControleurMembres.class.php
 *
 * la classe ControleurMembres hérite de la classe abstraite Controleur du framework.
 * elle utilise également la methode genererVue pour générer la vue associée à l'action
 *
 */

class ControleurMembres extends \Framework\Controleur
{
    /* @var manager de user */
    protected $_managerUser;

    /**
    * le constructeur instancie les classes "modèles" requises
    */
    public function __construct(\Framework\Application $app)
    {
    	parent::__construct($app);
        $this->_managerUser = \Framework\FactoryManager::createManager('User');
    }

    /**
    *
    * Méthode index
    *
    * cette méthode est l'action par défaut affichant la page de gestion des utilisateurs
    *
    * @param array $donnees tableau de données éventuellement passé en paramètre, permettant d'afficher dans le formulaire les champs valides saisis lors d'une
    * requête précédente
    *
    */
    public function index(array $donnees = array())
    {
    	$users = $this->_managerUser->getUsers();
    	$banni = \Framework\Configuration::get('banni',0);
    	$membre = \Framework\Configuration::get('membre',3);

    	$vue = $this->genererVue(array('users'=>$users,'banni'=>$banni,'membre'=>$membre));
    	$this->envoyerReponse(array('vue'=>$vue));
    }
}