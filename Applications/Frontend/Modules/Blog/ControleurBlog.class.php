<?php
/**
 *
 * @author Frédéric Tarreau
 * @version 1.1
 *
 * 25 oct. 2014 - ControleurBlog.class.php
 *
 * la classe ControleurBlog hérite de la classe abstraite Controleur du framework.
 * elle utilise également la methode genererVue pour générer la vue associée à l'action
 *
 */
namespace Applications\Frontend\Modules\Blog;
require_once './Framework/autoload.php';

class ControleurBlog extends \Framework\Controleur
{
    /* @var manager de article */
    private $_managerArticle;

    /* @var gestionnaire de téléchargement de fichier */
    protected $_uploadFileHandler;

    /**
    * le constructeur instancie les classes "modèles" requises
    */
    public function __construct(\Framework\Application $app)
    {
    	parent::__construct($app);
         $this->_managerArticle= \Framework\FactoryManager::createManager('Article');
    }

    /**
    *
    * Méthode index
    *
    * cette méthode est l'action par défaut consistant à :
    * - afficher l'article le plus récent du blog, puis la liste des articles regroupés par page
    * - créer le formulaire d'ajout de commentaire par la méthode initForm si l'utilisateur est administrateur
    * - afficher l'ensemble des articles avec le même libellé et permettre la navigation au sein de ce groupe
    *
    * elle utilise notamment les méthodes privées :
    * - calculerPagination : en fonction du nombre d'article
    * - selectionnerArticles : avec un filtre
    *
    * et exploite la superGlobale de session pour savoir si la navigation au sein des articles est limitée à un libellé ou non
    *
    * @param array $donnees tableau de données éventuellement passé en paramètre, permettant d'afficher dans le formulaire les champs valides saisis lors d'une
    * requête précédente ou de restreindre la navigation dans un libelle d'article pré selectionné
    *
    */
    public function index(array $donnees = array())
    {
var_dump($donnees);
        if((!$this->_requete->existeParametre('id')) && (!array_key_exists('choixLibelle',$donnees))) {
            // supprimer la variable de session relative aux libellés
            if(!empty($_SESSION['choixLibelle']))
                 $this->_app->userHandler()->removeAttribute('choixLibelle');
        }

        if(empty($_SESSION['choixLibelle'])){
            $curseur =1;// les articles étant affichées 5 / page, il faut définir un curseur pour afficher la bonne page
            if($this->_requete->existeParametre('id'))
                $curseur =  $this->_requete->getParametre('id');

            // suppression des doublons en cas de renvoie de formulaire
            $this->_managerArticle->supprimerDoublons('article','art_id',array('art_titre','art_libelle','art_contenu','art_image'));

            if(array_key_exists('curseur',$donnees)) //cas ou routage interne du controleurArticle vers controleurBlog
            	$curseur = $donnees['curseur'];

            $donneesVue = $this->selectionnerArticles(array('curseur'=>$curseur));

            // affichage du formulaire d'ajout d'article si l'utilisateur est administrateur et qu'il s'agit de l'affichage du dernier article
            $administrateur = \Framework\Configuration::get('admin');
            if($curseur==1){
            	if($this->_app->securiteHandler()->verifConnexion()){
            		if($this->_app->securiteHandler()->verifPermission('admin')){
            			if((array_key_exists('idArticle',$donnees)) && (!empty($donnees['idArticle']))){
            				unset($donnees['action']); // nécessaire si reroutage interne pour modification
            				$type = \Framework\Formulaire\FormBuilder::LONG;// spécification du type de formulaire pour le formbuilder
            				$tableauValeur = array('methode'=>'post','action'=>'blog/modifier','enctype'=>'multipart/form-data','type'=>$type,'value'=>$donnees['idArticle']);
            			} else {
            				$type = \Framework\Formulaire\FormBuilder::MOYEN;// spécification du type de formulaire pour le formbuilder
            				$tableauValeur = array('methode'=>'post','action'=>'blog/editer','enctype'=>'multipart/form-data','type'=>$type);
            			}

            			if(!empty($donnees))
            				$tableauValeur=array_merge($tableauValeur,$donnees);

            			$form=$this->initForm('article',$tableauValeur); // création du formulaire d'ajout d'un article
            			$donneesVue['formulaire']=$form->createView();
            		}
            	}
            }
        }
        // il y a des données relatives au libelle en session
        else {
            if((!empty($donnees)) && (array_key_exists('choixLibelle',$donnees))){
                $curseur = 1;
                $choixLibelle = $donnees['choixLibelle'];
            }
            // il s'agit d'une navigation au sein du groupe d'article correspondant au Libelle
            else {
                if($this->_requete->existeParametre('id'))
                    $curseur =  $this->_requete->getParametre('id');
                $choixLibelle = $this->_app->userHandler()->getAttribute('choixLibelle');
            }
            $donneesVue = $this->selectionnerArticles(array('curseur'=>$curseur,'choixLibelle'=>$choixLibelle));
        }
        $vue = $this->genererVue($donneesVue);
        $this->envoyerReponse(array('vue'=>$vue));
    }

    /**
    *
    * Méthode calculerPagination
    *
    * calcule le nombre de pages à prévoir pour la barre de navigation en fonction du nombre d'articles par page et
    * du nombre total d'articles
    *
    * @param string $filtre
    * @return array $listeArticles, référence les id d'articles pour chaque page
    */
    private function calculerPagination($filtre=NULL)
    {
        // récupération de données sur le nombre d'articles et pagination (suivant config)
        $nombreArticlesParPage=\Framework\Configuration::get('nombreArticleParPage',5);
        $nombreTotalArticles = $this->_managerArticle->getNombreArticles($filtre);

        // calcul du nombre de liens en bas de page vers les articles suivants
        if(($nombreTotalArticles%$nombreArticlesParPage)===0)
            $nombrePage = $nombreTotalArticles/$nombreArticlesParPage;
        else $nombrePage = floor($nombreTotalArticles/$nombreArticlesParPage) + 1;

        $listeArticles[0]= 1;
        $i=0;
        while($i<$nombrePage-1){
            $listeArticles[$i+1]=$listeArticles[$i]+$nombreArticlesParPage;
            $i++;
        }
        return $listeArticles;
    }

    /**
     *
     * Méthode selectionnerArticles
     *
     * en fonction du filtre elle sélectionne les articles à afficher et récupère l'ensemble des libellés pour alimenter la barre latérale (aside)
     *
     * @param array $donneesFiltre, paramètres du filtre de la BDD suivant les articles à sélectionner
     */
    private function selectionnerArticles($donneesFiltre=array())
    {
        // récupération de données sur le nombre d'articles et pagination (suivant config)
        $nombreArticlesParPage=\Framework\Configuration::get('nombreArticleParPage',5);

        $curseur = $donneesFiltre['curseur'];
        $choixLibelle = '';
        if(array_key_exists('choixLibelle', $donneesFiltre))
            $choixLibelle = $donneesFiltre['choixLibelle'];

        $listeArticles = $this->calculerPagination($choixLibelle);

        //récupération des articles
        if(($curseur==1)&& (empty($choixLibelle))){
            $donneesVue['dernierArticle'] = $this->_managerArticle->getDernierArticle();
            $donneesVue['articles'] = $this->_managerArticle->getArticles($curseur,$nombreArticlesParPage-1);
        } else {
            $donneesVue['dernierArticle'] = NULL;
            $donneesVue['articles'] = $this->_managerArticle->getArticles($curseur-1,$nombreArticlesParPage,$choixLibelle);
        }

        $listeLibelles = $this->_managerArticle->getLibelles();

        $donneesVue['listeArticles']= $listeArticles;
        $donneesVue['curseur']= $curseur;
        $donneesVue['listeLibelles']=$listeLibelles;

        $donneesVue['formulaire']='';
        return $donneesVue;
    }

    /**
     *
     * Méthode selectionnerLibelles
     *
     * lorsqu'un lien de la barre de navigation latérale (aside) permet de restreindre la navigation sur le libelle choisi
     *
     */
    public function selectionnerLibelles()
    {
        // supprimer l'ancien en variable de session (repésentée par un tableau : libellé et curseur)
        if(!empty($_SESSION['choixLibelle']))
           $this->_app->userHandler()->removeAttribute('choixLibelle');

        $choixLibelle=$this->_requete->getParametre("id");

        // créer la variable de session
        $this->_app->userHandler()->setAttribute('choixLibelle',$choixLibelle);
        $this->executerAction('index',array('choixLibelle'=>$choixLibelle));
    }

    /**
     *
     * Méthode editer
     *
     * cette méthode correspond à l'action "editer" permettant d'ajouter un article du blog
     * Elle ne doit être exécutée que si les données insérées dans le formulaire sont valides
     *
     */
    public function editer()
    {
    	if($this->_app->securiteHandler()->verifConnexion()){
    		if($this->_app->securiteHandler()->verifPermission('admin')){

    			$type = \Framework\Formulaire\FormBuilder::MOYEN;// spécification du type de formulaire pour le formbuilder

    			$allNoms = array('titre','libelle','contenu','image');
    			$allParams = $this->_requete->getAllParametres($allNoms);

    			$this->_uploadFileHandler = new \Framework\UploadFileHandler($this->_app,'image');
    			if($this->_uploadFileHandler->isValid())
    				$allParams['image'] = $this->_uploadFileHandler->getFilename();
    			else $allParams['image'] ='';

    			$date = new \DateTime();

    			$allValeurForm = array('methode'=>'post','action'=>'blog/editer','date'=>$date,'type'=>$type);
    			$allValeurForm = array_merge($allValeurForm, $allParams);

    			$form=$this->initForm('article',$allValeurForm);

    			$options=array();

    			if(($this->_requete->getMethode() ==='POST')){// si la methode est bien POST
    				//le formulaire est valide, insertion des données en BDD
    				if ($form->isValid()){
    					if($this->_uploadFileHandler->isValid()){
    						$uploadFile = $_SERVER['DOCUMENT_ROOT']
    									.substr_replace(\Framework\Configuration::get('racineWeb'),'',0,1)
    									.\Framework\Configuration::get('repertoireUploads')
    									.$allParams['image'];
    								$this->_uploadFileHandler->moveTo($uploadFile);
    						$this->creerImageArticle($allParams['image']);
    					}
    					extract($allParams);
    					$this->_managerArticle->ajouterArticle($titre,$libelle,$contenu,$image,0);
    				}
    				else $options=$form->validField();
    			}
    			//il s'agit sinon ou ensuite d'executer l'action par d�faut permettant d'afficher la liste des articles
    			$this->executerAction("index",$options);
    		} else $this->notify(array('code'=>\Framework\SecuriteHandler::NOT_AUTHORIZED));
    	} else $this->notify(array('code'=>\Framework\SecuriteHandler::NOT_CONNECTED));
    }

    /**
     *
     * Méthode modifier
     *
     * cette méthode correspond à l'action "modifier" permettant de modifier un article du blog
     * Elle ne doit être exécutée que si les données insérées dans le formulaire sont valides
     *
     */
    public function modifier()
    {
    	if($this->_app->securiteHandler()->verifConnexion()){
    		if($this->_app->securiteHandler()->verifPermission('admin')){
    			$type = \Framework\Formulaire\FormBuilder::LONG;// spécification du type de formulaire pour le formbuilder

    			$allNoms = array('titre','libelle','contenu','image','idArticle');

    			$this->_uploadFileHandler = new \Framework\UploadFileHandler($this->_app,'image');
    			if($this->_uploadFileHandler->isValid())
    				$allParams['image'] = $this->_uploadFileHandler->getFilename();

    			$dateModif = new \DateTime();
    			$idArticle = $allParams['idArticle'];
    			unset($allParams['idArticle']);
    			$allParams['dateModif']=$dateModif;

    			$allValeurForm = array('methode'=>'post','action'=>'blog/modifier','dateModif'=>$dateModif,'type'=>$type,'value'=>$idArticle);
    			$allValeurForm = array_merge($allValeurForm, $allParams);

    			$form=$this->initForm('article',$allValeurForm);

    			$options=array();

    			if (($this->_requete->getMethode() ==='POST')){ // si la methode est bien POST
    				//le formulaire est valide, insertion des données en BDD
    				if ($form->isValid()){
    					if($this->_uploadFileHandler->isValid()){
    						$uploadFile = $_SERVER['DOCUMENT_ROOT']
    									.substr_replace(\Framework\Configuration::get('racineWeb'),'',0,1)
    									.\Framework\Configuration::get('repertoireUploads')
    									.$allParams['image'];
    									$this->_uploadFileHandler->moveTo($uploadFile);
    									$this->creerImageArticle($allParams['image']);
    					}
    					$this->_managerArticle->actualiserArticle($idArticle,$allParams);
    				} else $options=$form->validField();
    			}
    			$this->executerAction("index",$options);
    		} else $this->notify(array('code'=>\Framework\SecuriteHandler::NOT_AUTHORIZED));
    	} else $this->notify(array('code'=>\Framework\SecuriteHandler::NOT_CONNECTED));
    }

    /**
     *
     * Méthode creerImageArticle
     *
     * créé une image redimensionnée à partir d'un fichier téléchargé et détruit le fichier original
     *
     * @param string $filename
     * @throws \InvalidArgumentException
     */
    private function creerImageArticle($filename)
    {
    	if(!empty($filename) && is_string($filename)){
    		if($this->_uploadFileHandler->isValid()){

    			$newFilenameWithPath = \Framework\Configuration::get('repertoireImagesArticle').$filename;
    			$uploadFile = \Framework\Configuration::get('repertoireUploads').$filename;

    			$image = new \Framework\Entites\Image(array('filename'=>$uploadFile));

    			$largeurMax = \Framework\Configuration::get('largeurMaxArticle');
    			$hauteurMax = \Framework\Configuration::get('hauteurMaxArticle');

    			$image->resize($largeurMax,$hauteurMax);
    			$image->saveImageAs($newFilenameWithPath);

    			$this->_uploadFileHandler->deleteFile($uploadFile);
    			} else throw new \Framework\Exceptions\ControleurException('incoherence sur chargement image');
    	} else throw new \InvalidArgumentException('les paramètres doivent être de type string');
    }
}