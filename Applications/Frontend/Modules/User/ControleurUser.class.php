<?php
namespace Applications\Frontend\Modules\User;
use Framework\Entite;
require_once './Framework/autoload.php';
/**
 *
 * @author Frédéric Tarreau
 *
 * 29 mars 2015 - ControleurUser.class.php
 *
 * la classe ControleurUser hérite de la classe abstraite Controleur du framework.
 * elle utilise également la methode genererVue pour générer la vue associée à l'action
 *
 */

class ControleurUser extends \Framework\Controleur
{
    /* @var Gestionnaire de mail */
    protected $_mailHandler;

    /**
    * le constructeur instancie les classes "modèles" requises
    */
    public function __construct(\Framework\Application $app)
    {
    	parent::__construct($app);
        $this->_mailHandler = new \Framework\MailHandler($app);
    }

    /**
    *
    * Méthode index
    *
    * cette méthode est l'action par défaut affichant la page de gestion d'un utilisateur
    *
    * @param array $donnees tableau de données éventuellement passé en paramètre, permettant d'afficher dans le formulaire les champs valides saisis lors d'une
    * requête précédente ou de servir de flag
    *
    */
    public function index(array $donnees = array())
    {
        if($this->_requete->existeParametre('id')) {
            $idUser=$this->_requete->getParametre('id');
            $user=$this->_app->userHandler()->managerUser()->getUser($idUser);
        } else {
            //TODO gestion d'erreur a faire
        }

        $params = array('user'=>$user);

        if(array_key_exists('contact',$donnees)){

            // tableau des valeurs à prendre en compte pour le formulaire
            $tableauVal = array('methode'=>'post','action'=>'user/envoyer','value'=>$idUser,
                                'destinataire'=>$user['_mail']);

            if(!empty ($donnees))
                $tableauVal=array_merge($tableauVal,$donnees);

            $form = $this->initForm('Mail',$tableauVal);
            $params['formulaire']=$form->createView();

        } elseif (array_key_exists('statut',$donnees)) {
        	$type = \Framework\Formulaire\FormBuilder::LISTE;

        	// préparation du tableau de valeur passée au formulaire
        	$value = \Framework\Configuration::get(null,null,'statuts_utilisateurs');
        	$value['idUser']=$idUser;
        	$value['statut']=$user['_statut'];

        	$tableauVal = array('value'=>$value,'methode'=>'post','action'=>'user/changerStatut','type'=>$type);

        	if(!empty ($donnees))
        		$tableauVal=array_merge($tableauVal,$donnees);

        	$form = $this->initForm('User',$tableauVal);

        	$params['formulaire']=$form->createView();
        }
    	$vue = $this->genererVue($params);
    	$this->envoyerReponse(array('vue'=>$vue));
    }

    /**
     *
     * Méthode changerStatut
     *
     * cette action permet de changer le statut d'un membre. Notamment si celui se retrouve avec un statut
     * à "banni". Dans la mesure où il est conservé en BDD, il est possible de lui interdire une reconnexion
     *
     */
    public function changerStatut()
    {
    	if($this->_app->securiteHandler()->verifConnexion()){
    		if($this->_app->securiteHandler()->verifPermission('admin')){
    			$idUser = $this->_requete->getParametre('id');
    			$newStatut = $this->_requete->getParametre('statut');

    			$user = $this->_app->userHandler()->managerUser()->getUser($idUser);

    			$value = \Framework\Configuration::get(null,null,'statuts_utilisateurs');
    			$objet= \Framework\Configuration::get('objetForum','Atelier blanc nord ouest');
    			$adminMail = \Framework\Configuration::get('adminMail','fred.tarreau@wanadoo.fr');

    			switch ($newStatut){
    				case $value['banni']:

    					if($this->_app->userHandler()->managerUser()->actualiserUser($idUser,array('_statut'=>$newStatut))){
    						$this->_app->userHandler()->setFlash("Membre banni de l'espace privé");
    						$this->_mailHandler->myMail($user['_mail'],$objet, "vous êtes banni du forum et ne pouvez plus y accéder");
    					}
    					//TODO doit on attribuer le pseudo anonyme a un membre banni ?
    					break;

    				case $value['admin']:

    					if($this->_app->userHandler()->managerUser()->actualiserUser($idUser,array('_statut'=>$newStatut))){
    						$this->_app->userHandler()->setFlash("Membre devenu administrateur");
    						$this->_mailHandler->myMail($adminMail,$objet, 'vous venez de donner le statut administrateur à'.$user['_pseudo']);
    					}
    					break;

    				default:

    					if($this->_app->userHandler()->managerUser()->actualiserUser($idUser,array('_statut'=>$newStatut)))
    						$this->_app->userHandler()->setFlash("Modification de statut prise en compte");
    					else $this->_app->userHandler()->setFlash("Echec modification du statut du membre");
    			}
    			$this->executerAction('index');
    		} else $this->notify(array('code'=>\Framework\SecuriteHandler::NOT_AUTHORIZED));
    	} else $this->notify(array('code'=>\Framework\SecuriteHandler::NOT_CONNECTED));
    }

    /**
     *
     * Méthode changer
     *
     * cette action permet de générer une liste de choix pour changer le statut
     *
     * @param array $options
     */
    public function changer($options = array())
    {
    	$options['statut']=TRUE;
    	$this->executerAction("index",$options);
    }

    /**
     *
     * Méthode contacter
     *
     * cette action permet de générer un formulaire de contact
     *
     * @param array $options
     */
    public function contacter($options = array())
    {
        $options['contact']=TRUE;
        $this->executerAction("index",$options);
    }

    /**
     *
     * Méthode envoyer
     *
     * cette action permet d'envoyer le mail de contact au membre
     *
     */
    public function envoyer()
    {
    	if($this->_app->securiteHandler()->verifConnexion()){
    		if($this->_app->securiteHandler()->verifPermission('admin')){
    			$userMail = $this->_requete->getParametre('destinataire');
    			$objet = $this->_requete->getParametre('objet');
    			$contenu = $this->_requete->getParametre('contenu');

    			$form=$this->initForm('Mail',array('destinataire'=>$userMail,
                                                'objet'=>$objet,
                                                'methode'=>'post',
                                                'contenu'=>$contenu,
                                                'action'=>'user/envoyer'));

    			$options = array();

    			// si la methode est bien POST et que le formulaire est valide,
    			if (($this->_requete->getMethode() =='POST')){
    				// le formulaire est valide
    				if ($form->isValid()){
    					if($this->_mailHandler->myMail($userMail,$objet,$contenu))
    						$this->_app->userHandler()->setFlash('Email envoyé');
    					else $this->_app->userHandler()->setFlash('Echec envoi d\'email');
    					$this->executerAction("index");
    				}
    				// le formulaire est invalide
    				else {
    					$options=$form->validField();
    					$this->_app->userHandler()->setFlash('Format des données saisies invalide');
    					$this->contacter($options);
    				}
    			}
    			// pas de méthode POST
    			else {
    				$this->_app->userHandler()->setFlash('Données de votre email non récupérées');
    				$this->contacter($options);
    			}
    		}else $this->notify(array('code'=>\Framework\SecuriteHandler::NOT_AUTHORIZED));
    	}else $this->notify(array('code'=>\Framework\SecuriteHandler::NOT_CONNECTED));
    }
}