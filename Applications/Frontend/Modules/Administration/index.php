<!-- Portion de Vue spécifique à l'affichage de la page d'administration -->

<?php $this->_titre = "Administration AtelierBlancNordOuest"; ?>
<?php if ($isAdmin):?>
<section class="col-sm-8">
<div class="row">
<?php $categorie='';
	  $articleHTML=0;
	foreach ($topics as $topic):?>
	<?php if($topic['categorie']!=$categorie):?>
	<?php if($articleHTML):?>
	 	</div>
	</article>
	<?php $articleHTML=0;
		  endif;?>
	<article class="col-sm-12">
		<div class="row">
		<?php $articleHTML = 1;?>
			<div class="col-sm-12">
				<div class="row">
					<div class="col-xs-4"><p><?= $this->nettoyer($topic['categorie'])?></p></div>
					<div class="col-xs-8">
						<div class='row'>
							<div class="col-xs-6"><p><?= $this->nettoyer($topic['titre'])?></p></div>
							<?php if(array_key_exists('id',$topic)):?>
							<div class="col-xs-3"><a href="<?= "administration/supprimer" . $this->nettoyer($topic['id']) ?>">Supprimer</a></div>
                           	<div class="col-xs-3"><a href="<?= "administration/modifier" . $this->nettoyer($topic['id']) ?>">Modifier</a></div>
                           	<?php endif;?>
                        </div>
                    </div>
                </div>
            </div>

	<?php $categorie=$topic['categorie'];?>
	<?php else:?>
                  <div class="col-sm-offset-4 col-sm-8">
                      <div class="row">
                          <div class="col-xs-6"><p><?= $this->nettoyer($topic['titre'])?></p></div>
                          <?php if(array_key_exists('id',$topic)):?>
                          <div class="col-xs-3"><a href="<?= "administration/supprimer" . $this->nettoyer($topic['id']) ?>">Supprimer</a></div>
                          <div class="col-xs-3"><a href="<?= "administration/modifier" . $this->nettoyer($topic['id']) ?>">Modifier</a></div>
                          <?php endif;?>
                      </div>
                  </div>
    <?php endif;?>
<?php endforeach; ?>

</div>
</section>
<section class="col-sm-4">
    <div class="row">
        <!-- Portion de Vue spécifique à l'affichage des formulaires de creation de topic et de categorie-->
        <div class="col-sm-12"><?= $this->nettoyer($formulaireTopic)?></div>
        <div class="col-sm-12"><?= $this->nettoyer($formulaireCategorie)?></div>
    </div>
</section>
<?php endif;?>
