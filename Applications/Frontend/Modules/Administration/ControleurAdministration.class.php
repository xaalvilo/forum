<?php
/**
 *
 * @author Frédéric Tarreau
 *
 * 13 juin 2015 - ControleurAdministration.class.php
 *
 * la classe ControleurAdministration hérite de la classe abstraite Controleur du framework.
 * elle utilise également la methode genererVue pour générer la vue associée à l'action
 *
 */
namespace Applications\Frontend\Modules\Administration;
require_once './Framework/autoload.php';

class ControleurAdministration extends \Framework\Controleur
{
    /* @var manager de Topic */
    private $_managerTopic;

    /* @var manager de Categorie */
    private $_managerCategorie;

    /**
    * le constructeur instancie les classes "mod�les" requises
    */
    public function __construct(\Framework\Application $app)
    {
        parent::__construct($app);
        $this->_managerTopic = \Framework\FactoryManager::createManager('Topic');
        $this->_managerCategorie = \Framework\FactoryManager::createManager('Categorie');
    }

    /**
    *
    * Méthode index
    *
    * action par défaut consistant à afficher la page d'accueil du site
    *
    * @param array $donnees éventuellement passé en paramètre pour afficher dans le formulaire les champs valides saisis lors d'une
    * requête précédente
    *
    */
    public function index(array $donnees = array())
    {
    	// suppression des doublons en cas de renvoie de formulaire
    	$this->_managerTopic->supprimerDoublons('forum_topic','topic_id',array('topic_titre'));

    	$topics = $this->_managerTopic->getAllTopics();

    	if(!empty($donnees) && array_key_exists('idCat',$donnees)){
    		$categorie = $this->_managerCategorie->getCategorie($donnees['idCat']);
    		$topics[]=array('titre'=>'','idCat'=>$donnees['idCat'],'categorie'=>$categorie->name());
    	}

    	// préparation des tableaux de valeur passées aux formulaires (données de la liste de choix)
    	foreach ($topics as $topic){
    		$value[$topic['idCat']]=array('categorie'=>$topic['categorie']);
    	}
    	$type = \Framework\Formulaire\FormBuilder::COURT;
    	$tableauFormTopic = array('value'=>$value,'methode'=>'post','action'=>'administration/creer','type'=>$type);
    	$tableauFormCat = array('methode'=>'post','action'=>'administration/creer');


    	$isAdmin = $this->_app->securiteHandler()->verifPermission('admin');

    	if(!empty($donnees)){
    		$tableauFormTopic=array_merge($tableauFormTopic,$donnees);
    		$tableauFormCat=array_merge($tableauFormCat,$donnees);
    	}

        $formTopic=$this->initForm('Topic',$tableauFormTopic);// création du formulaire d'ajout de Topic
        $formCat=$this->initForm('Categorie',$tableauFormCat);// création du formulaire d'ajout de Topic

        // génération de la vue avec les données : liste des billets et formulaire d'ajout de billet
        $vue = $this->genererVue(array('isAdmin'=>$isAdmin,'topics'=>$topics,
        						'formulaireTopic'=>$formTopic->createView(),'formulaireCategorie'=>$formCat->createView()));
        $this->envoyerReponse(array('vue'=>$vue));
    }

    /**
     *
     * Méthode creer
     *
     * cette méthode correspond à l'action "creer" permettant d'ajouter un topic ou une catégorie au forum
     * Elle ne doit être exécutée que si les données insérées dans le formulaire sont valides
     *
     */
    public function creer()
    {
    	if($this->_app->securiteHandler()->verifConnexion()){
    		if($this->_app->securiteHandler()->verifPermission('admin')){

    			$allNoms = array('id','titre','name');
    			$valeurs = $this->_requete->getAllParametres($allNoms);

    			$valeurs['type'] = \Framework\Formulaire\FormBuilder::COURT;
    			if(!empty($valeurs['titre']))
    				$form = $this->initForm('Topic',$valeurs);
    			elseif (!empty($valeurs['name']))
    				$form = $this->initForm('Categorie',$valeurs);

    			$options = array();

    			if($this->_requete->getMethode()=='POST'){
    				if($form->isValid()){

    					if(!empty($valeurs['titre']))
    						$id=$this->_managerTopic->ajouterTopic($valeurs['id'],$valeurs['titre']);
    					elseif (!empty($valeurs['name']))
    						$id=$this->_managerCategorie->ajouterCategorie($valeurs['name']);

    					if(ctype_digit($id)){
    						$this->_app->userHandler()->setFlash('Creation réussie');
    						if(!empty($valeurs['name']))
    							$options['idCat']=$id;
    					} else {
    						$options = $form->validField();
    						$this->_app->userHandler()->setFlash('Echec de l\'insertion ');
    					}
    				} else {
    					$options = $form->validField();
    					$this->_app->userHandler()->setFlash('Nom de catégorie non valide');
    				}
    			}
    			$this->executerAction('index',$options);
    		} else $this->notify(array('code'=>\Framework\SecuriteHandler::NOT_AUTHORIZED));
    	} else $this->notify(array('code'=>\Framework\SecuriteHandler::NOT_CONNECTED));
    }

    /**
     *
     * Méthode supprimer
     *
     * cette méthode correspond à l'action "supprimer" appelée par le contrôleur lorsque l'utilisateur demande la suppression d'un
     * Topic ou d'une catégorie
     */
    public function supprimer()
    {
    	if($this->_app->securiteHandler()->verifConnexion()){
    		if($this->_app->securiteHandler()->verifPermission('admin')){

    			//TODO verifier qu'il n'y a pas de billet
    			$idTopic = $this->_requete->getParametre('id');
    			$this->_managerTopic->supprimerTopic($idTopic);
    			$this->executerAction('index');
    		}else $this->notify(array('code'=>\Framework\SecuriteHandler::NOT_AUTHORIZED));
    	}else $this->notify(array('code'=>\Framework\SecuriteHandler::NOT_CONNECTED));
    }

    /**
     *
     * Méthode modifier
     *
     * cette méthode correspond à l'action "modifier" appelée par le contrôleur lorsque l'administrateur demande la modification d'un
     * Topic ou d'une Catégorie
     *
     */
    public function modifier()
    {
    	if($this->_app->securiteHandler()->verifConnexion()){
    		if($this->_app->securiteHandler()->verifPermission('admin')){

    			$allNoms = array('id','titre','idCat');
    			$valeurs = $this->_requete->getAllParametres($allNoms);
    			$form = $this->initForm('Topic',$valeurs);

    			$options = array();

    			if($this->_requete->getMethode() == 'POST'){
    				if($form->isValid()){
    					$this->_managerTopic->actualiserTopic($valeurs['id'],array('titre'=>$valeurs['titre']));
    					$this->executerAction('index',$options);
    				} else {
    					$options = $form->validField();
    					$this->_app->userHandler()->setFlash('Echec modification du Topic');
    				}
    				$this->executerAction('index',$options);
    			}
    		} else $this->notify(array('code'=>\Framework\SecuriteHandler::NOT_AUTHORIZED));
    	}else $this->notify(array('code'=>\Framework\SecuriteHandler::NOT_CONNECTED));
    }
}