<?php
/**
 *
 * @author Frédéric Tarreau
 *
 * 3 oct. 2014 - ControleurBillet.class.php
 *
 *  La classe ControleurBillet h�rite de la classe abstraite Controleur du framework.
 *
 *  Il s'agit du contrôleur des actions liées aux billets
 *  Elle utilise les services de la classe Requete pour accéder aux parametres de la requete.
 *  elle utilise également la methode executerAction de sa superclasse afin d'actualiser les détails sur le billet, et genererVue pour
 *  générer la vue associée à l'action
 *
 *  Elle permet les actions suivantes :
 *  - afficher un billet et ses commentaires
 *  - répondre à un billet avec une citation
 *  - supprimer un commentaire
 *  - modifier un commentaire
 *
 */
namespace  Applications\Frontend\Modules\Billet;
use Modele\CommentaireFormBuilder;
use Modele\Commentaire;
require_once './Framework/autoload.php';

class ControleurBillet extends \Framework\Controleur
{
    /* @var \Framework\Manager manager de billet */
    private $_managerBillet;

    /* @var \Framework\Manager manager de commentaire */
    protected $_managerCommentaire;

    /**
    * le constructeur instancie les classes "modèles" requises
    */
     public function __construct(\Framework\Application $app)
     {
     	 parent::__construct($app);
     	 $this->_managerBillet= \Framework\FactoryManager::createManager('Billet');
         $this->_managerCommentaire= \Framework\FactoryManager::createManager('Commentaire');
     }

     /**
      *  getters
      */
     public function managerCommentaire()
     {
     	return $this->_managerCommentaire;
     }

    /**
    *
    * Méthode Index
    *
    * cette m�thode est l'action par d�faut consistant � afficher la liste des commentaires associ�s � un billet
    * elle utilise les m�thodes getter des mod�les instanci�s pour r�cup�rer les donn�es n�cessaires aux vues
    * elle créée le formulaire d'ajout de commentaire par la méthode initForm
    *
    * @param array $donnees tableau de données optionnel, permettant d'afficher dans le formulaire les données de champs valides saisis lors d'une
    * requête précédente
    *
    */
    public function index(array $donnees=array())
    {
        // spécification de la table concernée dans la BDD
        $table=\Framework\Modeles\ManagerCommentaire::TABLE_COMMENTAIRES_BILLET;

        $idBillet=$this->_requete->getParametre("id");
        $this->_managerBillet->actualiserBillet($idBillet,array('nbVu'=>1));
        $billet=$this->_managerBillet->getBillet($idBillet);

        // recupération de données récupérées par la requête avec jointure ci-dessus
        $topic = $billet->topic;
        $nbUserComents = $billet->nbUserComents;
        $nbUserBillets = $billet->nbUserBillets;

        // suppression des doublons en cas de renvoie de commentaire
        $this->_managerBillet->supprimerDoublons('commentaire','com_id',array('com_contenu','user_id'));

        $commentaires=$this->_managerCommentaire->getListeCommentaires($table,$idBillet);

        $tableauValeur=array('idParent'=>$idBillet,'methode'=>'post','action'=>'billet/valider');

        // il faut préremplir le champ avec le pseudo fourni
        $donnees['auteur']=$this->_app->userHandler()->user()->pseudo();
        $pseudo = $donnees['auteur'];

        $isModerateur = $this->_app->securiteHandler()->verifPermission('moderateur');

        if(!$billet->verrou()){
            if(!empty($donnees))
                $tableauValeur=array_merge($tableauValeur,$donnees);
            $form=$this->initForm('Commentaire',$tableauValeur);// création du formulaire d'ajout de commentaire
            $vue = $this->genererVue(array('pseudo'=>$pseudo,'nbUserComents'=>$nbUserComents,'nbUserBillets'=>$nbUserBillets,
                                'billet'=>$billet,'topic'=>$topic,'isModerateur'=>$isModerateur,
                                'commentaires'=>$commentaires,'formulaire'=>$form->createView()));
        }
        else  $vue = $this->genererVue(array('pseudo'=>$pseudo,'nbUserComents'=>$nbUserComents,'nbUserBillets'=>$nbUserBillets,
                                'billet'=>$billet,'topic'=>$topic,'isModerateur'=>$isModerateur,
                                'commentaires'=>$commentaires,'formulaire'=>''));
        $this->envoyerReponse(array('vue'=>$vue));
    }

    /**
     *
     * Méthode valider
     *
     * Cette m�thode est l'action "valider" permettant d'ajouter un commentaire sur un billet ou de modifier un billet
     * existant.
     *
     */
    public function valider()
    {
    	// l'utilisateur est déjà connecté
    	if($this->_app->securiteHandler()->verifConnexion()){

    		$table = \Framework\Modeles\ManagerCommentaire::TABLE_COMMENTAIRES_BILLET;

    		$idBillet = $this->_requete->getParametre("id");
    		$auteur = $this->_app->userHandler()->user()->pseudo();
    		$contenu = $this->_requete->getParametre("contenu");

    		// création du formulaire d'ajout de commentaire en l'hydratant avec les valeurs de la requête
    		$form=$this->initForm('Commentaire',array('idParent'=>$idBillet,'auteur'=>$auteur,'contenu'=>$contenu,'methode'=>'post','action'=>'billet/valider'));

    		$options = array();

    		// si la methode est bien POST
    		if (($this->_requete->getMethode()==='POST')){
    			//le formulaire est valide
    			if ($form->isValid()){
    				$idAuteur=$this->_app->userHandler()->user()->id();

    				if(!$this->_app->securiteHandler()->antiflood('commentaire',$idAuteur))
    					$this->notify(array('alert'=>'antiflood','id'=>$idBillet));
    				else {
    					// si le commentaire est nouveau
                    	if (empty($_SESSION['modifCommentaire'])){

                    		$this->_managerCommentaire->ajouterCommentaire($table,$idBillet,$idAuteur,$contenu);
                    		$this->_managerBillet->actualiserBillet($idBillet,array('nbComents'=>1));

                    		// actualiser l'utilisateur en BDD
                    		$nbreCommentairesForum = $this->_app->userHandler()->user()->nbreCommentairesForum();
                    		$nbreCommentairesForum++;
                    		$this->_app->userHandler()->managerUser()->actualiserUser($idAuteur,array('_nbreCommentairesForum'=>$nbreCommentairesForum));
                    	}
                    	// il s'agit d'une actualisation de commentaire
                    	else {
                    		$dateModif = new \DateTime();
                    		$this->_managerCommentaire->actualiserCommentaire($table, $_SESSION['modifCommentaire'], array('contenu'=>$contenu,'dateModif'=>$dateModif));
                    		$this->_app->userHandler()->removeAttribute('modifCommentaire');
                    	}
                    	$this->executerAction("index",$options);
                	}
    			}
    			// formulaire non valide
    			else {
    				$options=$form->validField();
    				$this->executerAction("index",$options); // executer l'action par d�faut permettant d'afficher la liste des commentaires
    			}
    		}
    		// pas methode POST
    		else $this->executerAction("index",$options);
        }
        // l'utilisateur n'est pas connecté - fonctionnement anormal
        else $this->notify(array('code'=>\Framework\SecuriteHandler::NOT_CONNECTED));
    }

    /**
     *
     * Méthode supprimer
     *
     * cette méthode correspond à l'action "supprimer" appelée par le contrôleur lorsque l'utilisateur demande la suppression d'un
     * commentaire qu'il a rédigé précédemment, elle renvoie ensuite vers la page par défaut
     *
     */
    public function supprimer()
    {
        // l'utilisateur est déjà connecté
        if($this->_app->securiteHandler()->verifConnexion())
        {
            // spécification de la table concernée dans la BDD
            $table = \Framework\Modeles\ManagerCommentaire::TABLE_COMMENTAIRES_BILLET;

            // récupération des paramètres de la requête
            $idCommentaire=$this->_requete->getParametre("id");

            // mise en tampon de l'idBillet associée au commentaire
            $tableauBillet = $this->_managerCommentaire->getIdParent($table, $idCommentaire);

            $idBillet = $tableauBillet["idParent"];
            $idUser  = $tableauBillet["idUser"];

            // verification des permissions
            if($this->_app->securiteHandler()->verifPermission('moderateur')
                    || $this->_app->userHandler()->user()->id()==$idUser){
                $resultat = $this->_managerCommentaire->supprimerCommentaire($table, $idCommentaire);

                if($resultat){
                    $this->_app->userHandler()->setFlash('commentaire supprimé');

                    // actualisation du nbre de commentaires du billet //
                    $this->_managerBillet->actualiserBillet($idBillet,array('nbComents'=>-1));

                    // actualiser l'utilisateur en BDD
                    $nbreCommentairesForum = $this->_app->userHandler()->user()->nbreCommentairesForum();
                    $nbreCommentairesForum--;
                    $idAuteur=$this->_app->userHandler()->user()->id();
                    $this->_app->userHandler()->managerUser()->actualiserUser($idAuteur,array('_nbreCommentairesForum'=>$nbreCommentairesForum));
                    }
                    else
                        $this->_app->userHandler()->setFlash('échec de la tentative de suppression de commentaire');

                //il s'agit sinon ou ensuite d'executer l'action par d�faut permettant d'afficher le billet et la liste des commentaires
                // il faut changer l'id du commentaire par l'id du billet pour afficher correctement l'index
                $this->_requete->setParametre("id",$idBillet);
                $this->executerAction("index",array());
            }
            else {
                $this->_app->userHandler()->setFlash("Vous n'êtes pas autorisé à réaliser cette action");
                //TODO log securité à l'admin
                $this->_app->routeur()->routerInterne($this->_app,array('controleur'=>'Connexion',
                                                                        'action'=>'',
                                                                        'id'=>''));
            }
        }
        // l'utilisateur n'est pas connecté - fonctionnement anormal
        else $this->notify(array('code'=>\Framework\SecuriteHandler::NOT_CONNECTED));
    }

    /**
     *
     * Méthode modifier
     *
     * cette méthode correspond à l'action "modifier" appelée par le contrôleur lorsque l'utilisateur demande la modification d'un
     * commentaire qu'il a rédigé précédemment. Elle est également appelée lorsqu'un utilisateur souhaite faire une citation
     * Elle permet de pré remplir le formulaire de commentaire dans la page par défaut (index) avec les bonnes données
     * Elle utilise la variable superglobale S_SESSION
     *
     * @param boolean $citation, vrai s'il s'agit d'inclure une citation
     *
     */
    public function modifier($citation=NULL)
    {
        // utilisateur connecté
        if($this->_app->securiteHandler()->verifConnexion()){
            $table = \Framework\Modeles\ManagerCommentaire::TABLE_COMMENTAIRES_BILLET;
            $idCommentaire=$this->_requete->getParametre("id");

            // récupération du commentaire à modifier et auteur associé
            $commentaire = $this->_managerCommentaire->getCommentaire($table,$idCommentaire);
            $pseudo = $commentaire->auteur();

            // verification des permissions
            if($this->_app->securiteHandler()->verifPermission('moderateur')
                        || $this->_app->userHandler()->user()->pseudo()==$pseudo){

                $idBillet=$commentaire->idParent();// mise en tampon de l'idBillet associée au commentaire

                // il s'agit d'une citation, on inclut le message dans la fenêtre d'édition d'un commentaire
                if($citation)
                    $commentaire->setContenu('<blockquote>'.$tableauCommentaire['auteur'].' a écrit :"'.$tableauCommentaire['contenu'].'"</blockquote>');
                // il faut tracer cette modification jusqu'à l'actualisation du commentaire
                else
                    $this->_app->userHandler()->setAttribute("modifCommentaire",$idCommentaire);

                $tableauCommentaire= $commentaire->EntiteToArray(); // transformation de l'objet en tableau

                $this->_requete->setParametre("id",$idBillet); // Echanger l'id du commentaire par l'id du billet pour afficher correctement l'index
                $this->executerAction("index",$tableauCommentaire); //action par d�faut permettant d'afficher le billet et la liste des commentaires avec le formulaire pré rempli avec le commentaire à modifier
            }
            // l'utilisateur n'est pas autorisé - fonctionnement anormal
            else {
                $this->_app->userHandler()->setFlash("Vous n'êtes pas autorisé à réaliser cette action");
                //TODO log securité à l'admin
                $this->_app->routeur()->routerInterne($this->_app,array('controleur'=>'Connexion',
                                                                        'action'=>'',
                                                                        'id'=>''));
            }
        }
        // l'utilisateur n'est pas connecté - fonctionnement anormal
        else $this->notify(array('code'=>\Framework\SecuriteHandler::NOT_CONNECTED));
    }

    /**
     *
     * Méthode citer
     *
     * Elle permet d'inclure un autre commentaire dans un commentaire, ceci revient à exécuter l'action
     * "modifier"
     *
     */
    public function citer()
    {
       $citation=TRUE;
       $this->modifier($citation);
    }

    /**
     *
     * Méthode verrouiller
     *
     * action de verrouillage d'une conversation autorisée pour un modérateur
     *
     */
    public function verrouiller()
    {
        if($this->_app->securiteHandler()->verifConnexion()){
        	if($this->_app->securiteHandler()->verifPermission('moderateur')){

        		$idBillet=$this->_requete->getParametre("id");
        		$this->_managerBillet->actualiserBillet($idBillet,array('verrou'=>TRUE));
        		$this->executerAction("index");

        	}else $this->notify(array('code'=>\Framework\SecuriteHandler::NOT_AUTHORIZED));
        }else $this->notify(array('code'=>\Framework\SecuriteHandler::NOT_CONNECTED));
    }

    /**
     *
     * Méthode deverrouiller
     *
     * action de déverrouillage d'une conversation, autorisée pour un modérateur
     *
     */
    public function deverrouiller()
    {
        if($this->_app->securiteHandler()->verifConnexion()){
        	if($this->_app->securiteHandler()->verifPermission('moderateur')){

        		$idBillet=$this->_requete->getParametre("id");
        		$this->_managerBillet->actualiserBillet($idBillet,array('verrou'=>FALSE));
        		$this->executerAction("index");
        	}else $this->notify(array('code'=>\Framework\SecuriteHandler::NOT_AUTHORIZED));
        }else $this->notify(array('code'=>\Framework\SecuriteHandler::NOT_CONNECTED));
    }
}