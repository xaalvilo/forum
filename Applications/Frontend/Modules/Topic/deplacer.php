<!-- Portion de Vue spécifique à l'affichage de la page d'administration -->

<?php $this->_titre = "Administration AtelierBlancNordOuest"; ?>
<section class="col-sm-12">
    <div class="row">
        <!-- Portion de Vue spécifique à l'affichage du formulaire de gestion des topics -->
        <div class="col-sm-6"><?= $this->nettoyer($formulaire)?></div>
    </div>
</section>