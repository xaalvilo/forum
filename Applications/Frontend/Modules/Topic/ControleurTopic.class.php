<?php
/**
 *
 * @author Frédéric Tarreau
 * @version 1.1
 *
 * 6 mars 2015 - ControleurTopic.class.php
 *
 * la classe ControleurTopic hérite de la classe abstraite Controleur du framework.
 * elle utilise également la methode genererVue pour générer la vue associée à l'action
 * Elle permet les actions suivantes :
 * - afficher la liste des billets de ce topic (action par défaut)
 * - editer un billet
 * - modifier un billet
 * - supprimer un billet
 *
 */
namespace Applications\Frontend\Modules\Topic;
use Framework\SecuriteAntiFlood;
require_once './Framework/autoload.php';

class ControleurTopic extends \Framework\Controleur
{
    /* @var manager de Billet */
    protected $_managerBillet;

    /* @var manager de Topic */
    private $_managerTopic;

    /* @var manager de commentaire */
    protected $_managerCommentaire;

    /**
    * le constructeur instancie les classes "mod�les" requises
    */
    public function __construct(\Framework\Application $app)
    {
        parent::__construct($app);
        $this->_managerBillet= \Framework\FactoryManager::createManager('Billet');
        $this->_managerTopic = \Framework\FactoryManager::createManager('Topic');
        $this->_managerCommentaire = \Framework\FactoryManager::createManager('Commentaire');
    }

    /**
     *  getters
     */
    public function managerBillet()
    {
    	return $this->_managerBillet;
    }

    public function managerCommentaire()
    {
    	return $this->_managerCommentaire;
    }

    /**
    *
    * Méthode index
    *
    * cette m�thode est l'action par défaut consistant à afficher la liste de tous les posts du forum
    * pour un topic donné. Elle créée le formulaire d'ajout de commentaire par la méthode initForm
    *
    * @param array $donnees éventuellement passé en paramètre pour afficher dans le formulaire les champs valides saisis lors d'une
    * requête précédente
    *
    */
    public function index(array $donnees = array())
    {
        $idTopic=$this->_requete->getParametre("id");

        // suppression des doublons en cas de renvoie de formulaire
        $this->_managerBillet->supprimerDoublons('billet','bil_id',array('bil_contenu','user_id','bil_titre'));

        $billets = $this->_managerBillet->getBillets(array('idTopic'=>$idTopic));
        $this->_managerTopic->actualiserTopic($idTopic,array('vu'=>1));
        $topic=$this->_managerTopic->getTopic($idTopic);

        // récupération des données sur les topics de la catégorie pour le menu latéral
        $topics=$this->_managerTopic->getTopics($topic['idCat']);

        // tableau des valeurs à prendre en compte pour le formulaire
        $tableauValeur = array('idTopic'=>$idTopic,'methode'=>'post','action'=>'topic/editer');

        $pseudo=$this->_app->userHandler()->user()->pseudo();
        $isModerateur = $this->_app->securiteHandler()->verifPermission('moderateur');

        if(!empty ($donnees))
            $tableauValeur=array_merge($tableauValeur,$donnees);

        $form=$this->initForm('Billet',$tableauValeur);// création du formulaire d'ajout de Billet

        // génération de la vue avec les données : liste des billets et formulaire d'ajout de billet
        $vue = $this->genererVue(array('pseudo'=>$pseudo,'titreTopic'=>$topic['titre'],'isModerateur'=>$isModerateur,
                                'billets'=>$billets,'formulaire'=>$form->createView(),'topics'=>$topics));
        $this->envoyerReponse(array('vue'=>$vue));
    }

    /**
     *
     * Méthode editer
     *
     * cette méthode correspond à l'action "editer" permettant d'ajouter un billet au forum
     * Elle ne doit être exécutée que si les données insérées dans le formulaire sont valides
     *
     */
    public function editer()
    {
    	// l'utilisateur est déjà connecté
    	if($this->_app->securiteHandler()->verifConnexion()){
    		$idTopic = $this->_requete->getParametre("id");
        	$titre = $this->_requete->getParametre("titre");
        	$auteur = $this->_app->userHandler()->user()->pseudo();
        	$contenu = $this->_requete->getParametre("contenu");

        	// prise en compte de la date courante
        	$date = new \DateTime();

        	// création du formulaire d'ajout d'un billet en l'hydratant avec les valeurs de la requête
        	$form=$this->initForm('Billet',array('id'=>$idTopic,
                                              'titre'=>$titre,
                                              'auteur'=>$auteur,
                                              'contenu'=>$contenu,
                                              'date'=>$date,
                                              'methode'=>'post',
                                              'action'=>'topic/editer'));
         	$options=array();

         	// si la methode est bien POST et que le formulaire est valide, insertion des données en BDD
         	if ($this->_requete->getMethode()==='POST'){
        		if ($form->isValid()){
        			$idAuteur = $this->_app->userHandler()->user()->id();

        			if(!$this->_app->securiteHandler()->antiflood('billet',$idAuteur))
        				$this->notify(array('alert'=>'antiflood','id'=>$idTopic));
        			else {
        				// nouveau billet
        				if (empty($_SESSION['modifBillet'])){
        					// enregistrer billet en BDD
        					$idBillet=$this->_managerBillet->ajouterBillet($idTopic,$titre,$idAuteur,$contenu);

        					if(ctype_digit($idBillet)){
        						// actualiser utilisateur en BDD
        						$nbreBilletsForum = $this->_app->userHandler()->user()->nbreBilletsForum();
        						$nbreBilletsForum++;
        						$this->_app->userHandler()->managerUser()->actualiserUser($idAuteur,array('_nbreBilletsForum'=>$nbreBilletsForum));

        						// il faut actualiser les données du Topic (incrementation nbre de post et dernierPost)
        						$donnees['nbPost']=1;
        						$donnees['lastPost']=$idBillet;
        						$this->_managerTopic->actualiserTopic($idTopic,$donnees);
        					} else {
        						$this->_app->userHandler()->setFlash('Echec insertion du billet');
        						$options=$form->validField();
        					}
        				}
        				// actualisation d'un billet
        				else {
        					$dateModif = new \DateTime();
        					$this->_managerBillet->actualiserBillet($_SESSION['modifBillet'],array('titre'=>$titre,'contenu'=>$contenu,'dateModif'=>$dateModif));
        					$this->_app->userHandler()->removeAttribute('modifBillet');
        				}
        				$this->executerAction("index",$options); // executer l'action par d�faut permettant d'afficher la liste des billets
        			}
        		}
        		// formulaire non valide
        		else {
        			$options=$form->validField();
        			$this->executerAction("index",$options);
        		}
         	}
         	// pas methode POST
         	else $this->executerAction("index",$options);
         }
         // l'utilisateur n'est pas connecté - fonctionnement anormal
         else $this->notify(array('code'=>\Framework\SecuriteHandler::NOT_CONNECTED));
    }

    /**
     *
     * Méthode supprimer
     *
     * cette méthode correspond à l'action "supprimer" appelée par le contrôleur lorsque l'utilisateur demande la suppression d'un
     * billet qu'il a rédigé précédemment, elle renvoie ensuite vers la page par défaut
     * WARNING : pour que les commentaires associés à un billet soient supprimés en même temps que le billet la contrainte SQL sur clé étrangère doit
     * être ON DELETE CASCADE
     *
     */
    public function supprimer()
    {
        if($this->_app->securiteHandler()->verifConnexion()){

            $idBillet=$this->_requete->getParametre("id");

            // mise en tampon de l'idTopic associée au billet
            $tableauTopic = $this->_managerTopic->getParent($idBillet);
            $idTopic = $tableauTopic["idParent"];
            $lastPost = $tableauTopic["lastPost"];
            $idUser = $tableauTopic["idUser"];

            if($this->_app->securiteHandler()->verifPermission('moderateur')
                    || $this->_app->userHandler()->user()->id()==$idUser){
                if ($this->_managerBillet->supprimerBillet($idBillet)){
                	$this->_app->userHandler()->setFlash('Billet supprimé');

                	$donnees['nbPost']='-1'; // actualisation du topic

                    // le billet supprimé était le dernier du topic
                    if ($idBillet == $lastPost){
                        $tableauResultat = $this->_managerBillet->getLastBillet($idTopic);
                        $donnees['lastPost']=$tableauResultat['id'];
                    }
                    $this->_managerTopic->actualiserTopic($idTopic,$donnees);
                } else $this->_app->userHandler()->setFlash('échec suppression du Billet');

                $this->_requete->setParametre("id", $idTopic); // Echanger l'id du Billet par l'id du Topic pour afficher correctement l'index
                $this->executerAction("index", array()); // action par d�faut permettant d'afficher la liste des billets

            } // l'utilisateur n'est pas autorisé - fonctionnement anormal
    		else $this->notify(array('code'=>\Framework\SecuriteHandler::NOT_AUTHORIZED));
    	}// l'utilisateur n'est pas connecté - fonctionnement anormal
    	else $this->notify(array('code'=>\Framework\SecuriteHandler::NOT_CONNECTED));
    }

    /**
     *
     * Méthode modifier
     *
     * cette méthode correspond à l'action "modifier" appelée par le contrôleur lorsque l'utilisateur demande la modification d'un
     * billet qu'il a rédigé précédemment.
     * Elle permet de pré remplir le formulaire de billet dans la page par défaut (index) avec les bonnes données
     * Elle utilise la variable superglobale $_SESSION
     *
     */
    public function modifier()
    {
        if($this->_app->securiteHandler()->verifConnexion()){

            $idBillet=$this->_requete->getParametre("id");
            $billet = $this->_managerBillet->getBillet($idBillet);
            $pseudo = $billet->auteur();

            // verification des permissions
            if($this->_app->securiteHandler()->verifPermission('moderateur')
                    || $this->_app->userHandler()->user()->pseudo()==$pseudo){
                // récupération de l'idTopic associée au billet
                $tableauTopic = $this->_managerTopic->getParent($idBillet);
                $idTopic = $tableauTopic["idParent"];

                // création d'un tableau avec les attributs souhaités de l'objet $billet
                $tableauBillet['titre']=$billet->offsetget('titre');
                $tableauBillet['contenu']=$billet->offsetget('contenu');

                $this->_app->userHandler()->setAttribute("modifBillet",$idBillet); // il faut tracer cette modification jusqu'à l'édition du billet (action "editer")

                $this->_requete->setParametre("id",$idTopic);// Echanger l'id du billet par l'id du topic pour afficher correctement l'index
                $this->executerAction("index",$tableauBillet); // action par d�faut permettant d'afficher la liste des billets en affichant le formulaire pré rempli avec le billet à modifier
            } // l'utilisateur n'est pas autorisé - fonctionnement anormal
    		else $this->notify(array('code'=>\Framework\SecuriteHandler::NOT_AUTHORIZED));
    	}// l'utilisateur n'est pas connecté - fonctionnement anormal
    	else $this->notify(array('code'=>\Framework\SecuriteHandler::NOT_CONNECTED));
    }

    /**
     *
     * Méthode deplacer
     *
     * correspond à l'action "deplacer" autorisée pour le modérateur, permettant d'afficher un formulaire
     * pour déplacer un billet d'un Topic vers un autre
     *
     */
    public function deplacer()
    {
    	if($this->_app->securiteHandler()->verifConnexion()){
    		if($this->_app->securiteHandler()->verifPermission('moderateur')){

    			$idBillet = $this->_requete->getParametre("id");

    			$topicBillet = $this->_managerTopic->getParent($idBillet); // Topic actuel du Billet
    			$topics=$this->_managerTopic->getAllTopics();

    			// préparation du tableau de valeur passée au formulaire
    			foreach($topics as $topic){
    				$value[$topic['id']]=array('titre'=>$topic['titre'],'categorie'=>$topic['categorie']);
    			}
    			$value['idBillet']=$idBillet;

    			// creation du formulaire
    			$type = \Framework\Formulaire\FormBuilder::LISTE;
    			$tableauValeur = array('id'=>$topicBillet['idParent'],'value'=>$value,'methode'=>'post','action'=>'topic/modifierTopic','type'=>$type);
    			$form = $this->initForm('Topic',$tableauValeur);
    			$vue = $this->genererVue(array('formulaire'=>$form->createView()));
    			$this->envoyerReponse(array('vue'=>$vue));
    		} else $this->notify(array('code'=>\Framework\SecuriteHandler::NOT_AUTHORIZED));
    	}else $this->notify(array('code'=>\Framework\SecuriteHandler::NOT_CONNECTED));
    }

    /**
     *
     * Méthode modifierTopic
     *
     * correspond à l'action "valider" autorisée pour le modérateur, permettant de confirmer via formulaire,
     * le déplacement d'un billet vers un autre topic (cf. méthode "déplacer")
     *
     */
    public function modifierTopic()
    {
    	if($this->_app->securiteHandler()->verifConnexion()){
    		if($this->_app->securiteHandler()->verifPermission('moderateur')){
    			$idBillet = $this->_requete->getParametre("idBillet");
    			$newIdTopic = $this->_requete->getParametre("id");

    			$tableauTopic = $this->_managerTopic->getParent($idBillet);// récupération des données sur l'ancien Topic associé au billet
    			$this->_managerBillet->actualiserBillet($idBillet,array('topic_id'=>$newIdTopic));

    			// le billet déplacé était le dernier de l'ancien topic de rattachement
    			if ($idBillet == $tableauTopic["lastPost"]){
    				$tableauResultat = $this->_managerBillet->getLastBillet($tableauTopic["idParent"]);
    				$this->_managerTopic->actualiserTopic($tableauTopic["idParent"],array('lastPost'=>$tableauResultat['id'],'nbPost'=>-1));
    			} else $this->_managerTopic->actualiserTopic($tableauTopic["idParent"],array('nbPost'=>-1));

    			// vérifier si le billet déplacé est le dernier du nouveau topic
    			$newTableauResultat  = $this->_managerBillet->getLastBillet($newIdTopic);
    			if($newTableauResultat['id']== $idBillet)
    				$this->_managerTopic->actualiserTopic($newIdTopic,array('lastPost'=>$idBillet,'nbPost'=>1));
    			else $this->_managerTopic->actualiserTopic($newIdTopic,array('nbPost'=>1));

    			$this->executerAction("index");

    		} else $this->notify(array('code'=>\Framework\SecuriteHandler::NOT_AUTHORIZED));
    	} else $this->notify(array('code'=>\Framework\SecuriteHandler::NOT_CONNECTED));
    }
}