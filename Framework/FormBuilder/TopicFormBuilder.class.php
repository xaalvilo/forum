<?php
namespace Framework\FormBuilder;
require_once './Framework/autoload.php';
/**
 *
 * @author Frédéric Tarreau
 *
 * 10 juin 2015 - TopicFormBuilder.class.php
 *
 * Classe fille de FormBuilder dont le r�le est de cr�er le formulaire associé à un Topic
 *
 */
 class TopicFormBuilder extends \Framework\Formulaire\FormBuilder
 {
 	/**
 	 * Méthode build
 	 *
 	 * Méthode permettant de construire le formulaire de choix d'un Topic (associé à un billet)
 	 *
 	 * @see \Framework\FormBuilder::build()
 	 *
 	 */
 	public function build($type = NULL)
 	{
 		switch ($type){
 			case 3 : //COURT
 				$fieldSet1 = new \Framework\Formulaire\FieldSet(array('legend'=>'Votre nouveau thème'));

 				$select = new \Framework\Formulaire\Select(array('name'=>'id','label'=>'Catégorie'));

 				if(!empty($value=$this->form->value())){
 					foreach($value as $cle=>$valeur){
 						$select->addOption(new \Framework\Formulaire\Option(array('value'=>$cle,
 																				'contenu'=>$valeur['categorie'])));
 					}
 				}

 				$fieldSet1->addSelect($select)->addField(new \Framework\Formulaire\InputField (array('type'=>'text',
 																				'label'=>'Thème',
 																				'name'=>'titre',
 																				'maxLength'=>15,
 																				'id'=>'titre',
 																				'size'=>15,
 																				'required'=>true,
 																				'autofocus'=>true,
 																				'placeholder'=> 'thème du forum',
 																				'validators'=>array(
 																						new \Framework\Formulaire\NotNullValidator('Merci de donner un titre de forum'),
 																						new \Framework\Formulaire\MaxLengthValidator('le nombre maximal de caract�re est fix� � 15', 15)
 						))));
 				$this->form->addFieldSet($fieldSet1)->addButton(new \Framework\Formulaire\Button('submit','Valider'));
 				break;

 			case 4 : //PETIT
 				$fieldSet = new \Framework\Formulaire\FieldSet();
 				$fieldSet->addField(new \Framework\Formulaire\InputField (array('type'=>'text',
 																				'name'=>'titre',
 																				'maxLength'=>15,
 																				'id'=>'titre',
 																				'size'=>15,
 																				'required'=>true,
 																				'autofocus'=>true,
 																				'placeholder'=>'thème du forum',
 																				'validators'=>array(
 																						new \Framework\Formulaire\NotNullValidator('Merci de donner un titre de forum'),
 																						new \Framework\Formulaire\MaxLengthValidator('le nombre maximal de caract�re est fix� � 15', 15)
 						))));
 				$this->form->addFieldSet($fieldSet)->addButton(new \Framework\Formulaire\Button('submit','Valider'));
 				break;

 			case 6 : //LISTE

 				$fieldSet = new \Framework\Formulaire\FieldSet(array('legend'=>'Thème associé'));

 				$select = new \Framework\Formulaire\Select(array('name'=>'id'));

 				if(!empty($value=$this->form->value())){
 					$hiddenValue=$value['idBillet'];// prise en compte de la valeur cachée à transmettre
 					unset($value['idBillet']);

 					foreach($value as $cle=>$valeur){
 						$select->addOption(new \Framework\Formulaire\Option(array('value'=>$cle,
 																	  'contenu'=>$valeur['titre'],
 																	  'optgrouplabel'=>$valeur['categorie'])));
 					}
 				}
 				$fieldSet->addSelect($select);
 				$this->form->addFieldSet($fieldSet)->addButton(new \Framework\Formulaire\Button('submit','Déplacer',NULL,'idBillet',$hiddenValue));
 				break;

 				default: throw new \Exception("Type de formulaire non reconnu");
 		}
 	}
 }
