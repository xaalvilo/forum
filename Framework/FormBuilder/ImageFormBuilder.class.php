<?php
namespace Framework\FormBuilder;
require_once './Framework/autoload.php';
/**
 *
 * @author Frédéric Tarreau
 *
 * 24 juillet 2015 	ImageFormBuilder.class.php
 *
 * Classe fille de FormBuilder dont le r�le est de cr�er le formulaire associ� à une image
 *
 */
 class ImageFormBuilder extends \Framework\Formulaire\FormBuilder
 {
 	/**
 	 * Méthode build
 	 *
 	 * Méthode permettant de construire le formulaire d'upload d'un fichier Image
 	 *
 	 * @see \Framework\FormBuilder::build()
 	 *
 	 */
 	public function build($type = NULL)
 	{
 		$fieldSet = new \Framework\Formulaire\FieldSet(array('legend'=>'Votre Image'));

 		$fieldSet->addField(new \Framework\Formulaire\InputField (array(
 		 			                                        'type'=>'file',
 		 													'label'=>'Image  ',
 		 													'name'=>'filename',
 															'accept'=>'image/jpeg,image/png,image/gif',
 		 													'maxLength'=>50,
 		 													'id'=>'image',
 		 													'size'=>50,
 		 													'required'=>true,
 		                                                    'autofocus'=>true,
 		 													'placeholder'=> 'chemin du fichier',
 		 													'validators'=>array(
 		 																		new \Framework\Formulaire\NotNullValidator('Merci de donner le chemin du fichier image'),
 		 																		new \Framework\Formulaire\MaxLengthValidator('le nombre maximal de caract�re est fixé à 50', 50)
 		 													))));

 		 $this->form->addFieldSet($fieldSet);

 		$hiddenValue=\Framework\Configuration::get('MAX_FILE_SIZE',5000);// recommandation php.net

 		 // ajout du bouton de validation du formulaire permettant l'upload de fichier
 		 $this->form->addButton(new \Framework\Formulaire\Button('submit','Télécharger',NULL,'MAX_FILE_SIZE',$hiddenvalue));
 	}
 }
