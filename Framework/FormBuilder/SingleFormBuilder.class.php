<?php
namespace Framework\FormBuilder;
require_once './Framework/autoload.php';
/**
 *
 * @author Frédéric Tarreau
 *
 * 04 mai 2015 - SingleFormBuilder.class.php
 *
 * Classe fille de FormBuilder dont le r�le est de cr�er un formulaire générique
 * se limitant à un bouton
 *
 */
 class SingleFormBuilder extends \Framework\Formulaire\FormBuilder
 {
 	/**
 	 * Méthode build
 	 *
 	 * Méthode permettant de construire un bouton de suppression du compte d'un membre
 	 *
 	 * @see \Framework\Formulaire\FormBuilder::build()
 	 */
 	public function build($type = NULL)
 	{
 		 // ajout du bouton  permettant de supprimer un compte
 		 $this->form->addButton(new \Framework\Formulaire\Button('submit','Supprimer le compte'));
 	}
}
