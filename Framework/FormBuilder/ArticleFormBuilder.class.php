<?php
namespace Framework\FormBuilder;
require_once './Framework/autoload.php';

/**
 *
 * @author Fr�d�ric Tarreau
 * @version 1.1
 * @name ArticleFormBuilder.class.php
 *
 *  25 oct 2014 -
 *
 * Classe fille de FormBuilder dont le r�le est de cr�er le formulaire associ� aux articles du blog
 *
 */
 class ArticleFormBuilder extends \Framework\Formulaire\FormBuilder
 {
 	/**
 	 * Méthode build
 	 *
 	 * @see \Framework\Formulaire\FormBuilder::build()
 	 * @throws \Exception
 	 */
 	public function build($type = NULL)
 	{
 		$fieldSet = new \Framework\Formulaire\FieldSet(array('legend'=>'Votre article'));
 		$fieldSet->addField(new \Framework\Formulaire\InputField(array(
 		                                                    'type'=>'text',
 		 													'label'=>'Titre  ',
 		 													'name'=>'titre',
 		 													'maxLength'=>100,
 		 													'id'=>'titre',
 		 													'size'=>100,
 		 													'required'=>true,
 		                                                    'autofocus'=>true,
 		 													'placeholder'=> 'titre de l\'article',
 		 													'validators'=>array(
 		 																		new \Framework\Formulaire\NotNullValidator('Merci de donner un titre à cet article'),
 		 																		new \Framework\Formulaire\MaxLengthValidator('le nombre maximal de caract�re est fix� � 100', 100),
 		 			        ))))
 		 		->addField(new \Framework\Formulaire\InputField(array(
 		 			                                         'type'=>'text',
 		 			                                         'label'=>'Libelle   ',
 		 			                                         'name'=>'libelle',
 		 			                                         'maxLength'=>20,
 		 			                                         'id'=>'libelle',
 		 			                                         'size'=>20,
 		 			                                         'required'=>true,
 		 			                                         'placeholder'=> 'libellé de l\'article',
 		 			                                         'validators'=>array(
 		 			                                                         new \Framework\Formulaire\NotNullValidator('Merci de donner un libellé à cet article'),
 		 			                                                         new \Framework\Formulaire\MaxLengthValidator('le nombre maximal de caract�re est fix� � 20', 20),
 		 			          ))))
 		 		->addField(new \Framework\Formulaire\TextField(array(
 		 							                     'label'=>'Article',
 		 			                                     'name'=>'contenu',
 		 			                                     'id'=>'contenu',
 		 			              						 'cols'=>20,'rows'=>3,
 		 			                                     'required'=>true,
 		 							                     'placeholder'=>'votre article',
 		 			                                     'validators'=>array(new \Framework\Formulaire\NotNullValidator('Merci d\'ecrire le texte de l\'article')
 		 					))))
 		 		->addField(new \Framework\Formulaire\InputField (array(
 		 												'type'=>'file',
 		 												'label'=>'Image  ',
 		 												'name'=>'image',
 		 												'accept'=>'image/jpeg,image/png,image/gif',
 		 												'maxLength'=>50,
 		 												'id'=>'image',
 		 												'size'=>50,
 		 												'required'=>false,
 		 												'autofocus'=>true,
 		 												'placeholder'=> 'chemin du fichier',
 		 												'validators'=>array(new \Framework\Formulaire\IsUploadedFileValidator('le fichier fourni n\'a pas été téléchargé','image')
 		 					))));

 		 	$this->form->addFieldSet($fieldSet);

 		 // ajout du bouton de validation ou de modification du formulaire
 		switch ($type){
 		 	case 1 : //LONG
 		 		if(!empty($hiddenValue = $this->form->value()))
 		 				$this->form->addButton(new \Framework\Formulaire\Button('submit','Modifier',NULL,'idArticle',$hiddenValue));
 		 		else throw new \Exception("valeur cachée du bouton de formulaire inconnue");
 		 		break;
 		 	case 2 : //MOYEN
 		 		$this->form->addButton(new \Framework\Formulaire\Button('submit','Editer'));
 		 		break;
 		 	default:throw new \Exception("Type de formulaire non reconnu");
 		}
 	}
 }
