<?php
namespace Framework\FormBuilder;
require_once './Framework/autoload.php';
/**
 *
 * @author Frédéric Tarreau
 *
 * 17 juin 2015 - CategorieFormBuilder.class.php
 *
 * Classe fille de FormBuilder dont le r�le est de cr�er le formulaire associé à une catégorie du forum
 *
 */
 class CategorieFormBuilder extends \Framework\Formulaire\FormBuilder
 {
 	/**
 	 * Méthode build
 	 *
 	 * Méthode permettant de construire le formulaire de creation de categorie
 	 *
 	 * @see \Framework\FormBuilder::build()
 	 *
 	 */
 	public function build($type = NULL)
 	{
 		$fieldSet = new \Framework\Formulaire\FieldSet(array('legend'=>'Votre nouvelle catégorie'));

 		$fieldSet->addField(new \Framework\Formulaire\InputField (array(
 																		'label'=>'Categorie',
 																		'type'=>'text',
 																		'name'=>'name',
 																		'maxLength'=>15,
 																		'id'=>'name',
 																		'size'=>15,
 																		'required'=>true,
 											 							'placeholder'=> 'catégorie du forum',
 																		'validators'=>array(
 																		new \Framework\Formulaire\NotNullValidator('Merci de donner un nom de categorie'),
 																		new \Framework\Formulaire\MaxLengthValidator('le nombre maximal de caract�re est fix� � 15', 15)
 																		))));
 		$this->form->addFieldSet($fieldSet)->addButton(new \Framework\Formulaire\Button('submit','Valider'));
 	}
 }
