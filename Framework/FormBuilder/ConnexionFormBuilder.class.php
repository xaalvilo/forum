<?php
namespace Framework\FormBuilder;
use Framework\Form;
require_once './Framework/autoload.php';
 /**
  *
  * @author Frédéric Tarreau
  *
  * 30 nov. 2014 - ConnexionFormBuilder.class.php
  *
  * Classe fille de FormBuilder dont le r�le est de cr�er le formulaire de connexion
  *
  */
 class ConnexionFormBuilder extends \Framework\Formulaire\FormBuilder
 {
 	/**
 	*
 	* Méthode build
 	*
 	* cette méthode permet de construire le formulaire de connexion
 	*
 	* @see \Framework\FormBuilder::build()
 	*
 	*/
 	public function build($type = NULL)
 	{
 	    // récupération de la taille minimale et maximale d'un mot de passe en configuration
 	    $minLength = (int)\Framework\Configuration::get('longMinMdp');
 	    $maxLength = (int)\Framework\Configuration::get('longMaxMdp');

 	    $fieldSet = new \Framework\Formulaire\FieldSet(array('legend'=>'Vos identifiants'));

 	    // ajout du champ de pseudo, attention, il faut bien reprendre le nom de l'attribut "pseudo" de l'objet connexion
 	    $fieldSet->addField(new \Framework\Formulaire\InputField(array(
 	                                                        'type'=>'text',
 		 													'label'=>'Identifiant  ',
 		 													'name'=>'pseudo',
 		 													'maxLength'=>15,
 		 													'id'=>'pseudo',
 		 													'size'=>20,
 		 													'required'=>true,
 	                                                        'autofocus'=>true,
 		 													'placeholder'=> 'votre pseudo',
 		 													'validators'=>array(
 		 																		new \Framework\Formulaire\NotNullValidator('Merci de sp�cifier votre pseudo'),
 		 																		new \Framework\Formulaire\MaxLengthValidator('le nombre maximal de caract�re est fixe a 15', 15),
 		 													                    new \Framework\Formulaire\StringValidator('Merci d\'entrer une chaine de caractere alphanumerique')
 		 			       ))))
 		 			// ajout du champ de password, attention, il faut bien reprendre le nom de l'attribut "mdp" de l'objet Connexion
 	                  ->addField(new \Framework\Formulaire\InputField(array(
 	                                                         'type'=>'password',
 	                                                         'label'=>'Password  ',
 	                                                         'name'=>'mdp',
 	                                                         'pattern'=>'{'.$minLength.','.$maxLength.'}',
 	                                                         'id'=>'mdp',
 	                                                         'size'=>20,
 	                                                         'required'=>true,
 	                                                         'placeholder'=> 'votre mot de passe',
 	                                                         'validators'=>array(
 	                                                                             new \Framework\Formulaire\NotNullValidator('Merci de donner un mot de passe'),
 	                                                                             new \Framework\Formulaire\MinLengthValidator('le nombre minimal de caractère est fixé à'.$minLength, $minLength),
 	                                                                             new \Framework\Formulaire\MaxLengthValidator('le nombre maximal de caractère est fixé à'.$maxLength, $maxLength),
 	                                                                             new \Framework\Formulaire\PasswordValidator('le mot de passe doit comporter xxx')
 		 					))));

 	     $this->form->addFieldSet($fieldSet);

 	     // prise en compte de la valeur cachée à transmettre
 	    $hiddenValue=$this->form->value();

 		 // ajout du bouton de validation du formulaire
 		 $this->form->addButton(new \Framework\Formulaire\Button('submit','Connecter',NULL,'id',$hiddenValue));
 	}
 }
