<?php
namespace Framework\Entites;
require_once './Framework/autoload.php';
/**
 *
 * @author Frédéric Tarreau
 *
 * 10 juin 2015 - Topic.class.php
 *
 *  cette classe représente l'entité Topic (thème) du Forum
 *
 */
class Topic extends \Framework\Entite
{
	/* @var string titre du Topic */
	protected $_titre;

	/* @var int id du dernier billet du Topic */
	protected $_lastPost;

	/* @var int nbre de billets du Topic */
	protected $_nbPost;

	/* @var int nombre d'affichage du Topic */
	protected $_nbVu;

	/* @var int catégorie du Topic */
	protected $_idCat;

	const TITRE_INVALIDE=1;

    /**
    * méthodes "setters" des attributs privés
    *
    * @throws \InvalidArgumentException
    */
    public function setTitre($titre)
    {
    	if (!is_string($titre) || empty($titre))
        	throw new \InvalidArgumentException('Erreur de format du titre de topic',self::TITRE_INVALIDE);
        else
            $this->_titre=$titre;
    }

    /**
     * @param int $idCat
     */
    public function setIdCat($idCat)
    {
        $this->_idCat=$idCat;
    }

    /**
     * @param int $nbVu
     */
    public function setNbVu($nbVu)
    {
        $this->_nbVu=$nbVu;
    }

    /**
     * @param int $lastPost
     */
    public function setLastPost($lastPost)
    {
        $this->_lastPost=$lastPost;
    }

    /**
     * @param int $nbPost
     */
    public function setNbPost($nbPost)
    {
    	$this->_nbPost=$nbPost;
    }

    /**
     * méthodes "getters" des attributs privés
     *
     * @return int
     */
    public function idCat()
    {
        return $this->_idCat;
    }

    /**
     * @return int
     */
    public function nbVu()
    {
        return $this->_nbVu;
    }

    /**
     * @return string
     */
    public function titre()
    {
    	return $this->_titre;
    }

    /**
     * @return int
     */
    public function nbPost()
    {
        return $this->_nbPost;
    }

    /**
     * @return int
     */
    public function lastPost()
    {
        return $this->_lastPost;
    }
}








