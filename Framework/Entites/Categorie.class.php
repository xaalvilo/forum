<?php
namespace Framework\Entites;
require_once './Framework/autoload.php';
/**
 *
 * @author Frédéric Tarreau
 *
 * 16 juin 2015 - Categorie.class.php
 *
 *  cette classe représente l'entité Categorie du Forum
 *
 */
class Categorie extends \Framework\Entite
{
	/* @var string nom de la catégorie */
	protected $_name;

	/* @var int ordre de la catégorie */
	protected $_ordre;

	const NOM_INVALIDE=1;

    /**
    * méthodes "setters" des attributs privés
    *
    * @throws \InvalidArgumentException
    */
    public function setName($name)
    {
    	if (!is_string($name) || empty($name))
        	throw new \InvalidArgumentException('Erreur de format de nom de catégorie', self::NOM_INVALIDE);
        else
            $this->_name=$name;
    }

    /**
     * @param int $ordre
     */
    public function setNbVu($ordre)
    {
        $this->_ordre=$ordre;
    }

    /**
     * méthodes "getters" des attributs privés
     *
     * @return int
     */
    public function ordre()
    {
        return $this->_ordre;
    }

    /**
     * @return string
     */
    public function name()
    {
    	return $this->_name;
    }
}








