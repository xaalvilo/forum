<?php
/**
 *
 * @author Frédéric Tarreau
 *
 * 22 juillet 2015 - Image.class.php
 *
 * Cette classe hérite de la classe Entités et représente l'image de l'avatar ou d'un Blog
 *
 */
namespace Framework\Entites;
require_once './Framework/autoload.php';

class Image extends \Framework\Entite
{
    /* @var array informatins sur l'image */
    protected $_info;

    /* @var string  extension correspondant au type de fichier */
    protected $_extension;

    /* @var string nom du fichier */
    protected $_filename;

    /* @resource image */
    protected $_resource;

    const EXTENSION_INVALIDE=110;
    const FILENAME_INVALIDE=111;
    const IMAGE_OPERATION_FAILED = 112;
    const DIMENSION_IMAGE_INVALIDE = 113;
    const FILE_TYPE_INVALIDE = 200;

    public function __construct($donnees)
    {
    	parent::__construct($donnees);
    	if (!isset($this->_filename) || empty($this->_filename))
    		throw new \BadMethodCallException('Erreur lors de l\'hydratation de l\'objet image', self::IMAGE_OPERATION_FAILED);
    	else $this->setInfo();
    }

    /**
     *
     * Méthode getExtension
     *
     * getter de extension (sans le point '.')
     *
     * @return string
     * @throws \InvalidArgumentException
     * @throws \Framework\Exceptions\FileException
     */
    protected function getExtension()
    {
    	if(empty($this->_extension)){
    		if($imagetype = exif_imagetype($this->_filename)){
    			$extension = image_type_to_extension($imagetype, false);
    		} else throw new \Framework\Exceptions\FileException('impossible de déterminer le type de fichier image',self::FILE_TYPE_INVALIDE);

    		$authorisedExtension = \Framework\Configuration::get(null,null,'extension_image');
    		if (array_key_exists($extension,$authorisedExtension)){
    			// TODO est ce bien utile puisqu'on se sert de l'extension uniquement pour les fonctions php et non l'écriture du fichier
    			//$remplacements = array ('jpeg'=>'jpg','tiff'=>'tif'); // pour bon format de l'extension
    			//$this->_extension = str_replace(array_keys($remplacements),array_values($remplacements),$extension);
    			$this->_extension=$extension;
    		} else throw new \InvalidArgumentException('type de fichier image incompatible', self::EXTENSION_INVALIDE);
    	}
    	return $this->_extension;
    }

    /**
     *
     * Méthode setFilename
     *
     * Setter de filename
     *
     * @param string $extension
     * @throws \InvalidArgumentException
     */
    public function setFilename($filename)
    {
    	if (!empty($filename) && is_file($filename))
    		 $this->_filename=$filename;
    	else throw new \InvalidArgumentException('fichier image inconnu ou absent', self::FILENAME_INVALIDE);
    }

    /**
     *
     * Méthode getFilename
     *
     * getter de filename
     *
     * @return string
     */
    public function getFilename()
    {
    	return $this->_filename;
    }

    /**
     *
     * Méthode setInfo
     *
     * spécifie les informations de l'image
     *
     * @param array $infos
     * @throws \InvalidArgumentException
     */
    protected function setInfo($infos = NULL)
    {
    	if (empty($infos))
    		$this->_info = getimagesize($this->_filename);
    	elseif (is_array($infos)) {
    		$this->_info[0]=$infos[0];
    		$this->_info[1]=$infos[1];
    	}else throw new \InvalidArgumentException('le type de l\'argument ne correspond pas', self::DIMENSION_IMAGE_INVALIDE);
    }

    /**
     *
     * Méthode getInfo
     *
     * getter de info
     *
     * @return array
     */
    public function getInfo()
    {
    	return $this->_info;
    }

    /**
     *
     * Méthode setResource
     *
     * attribue une ressource à l'objet
     *
     * @param resource $resource
     * @throws \InvalidArgumentException
     */
    protected function setResource($resource)
    {
    	if(!empty($resource) && is_resource($resource))
    		$this->_resource=$resource;
    	else throw new \InvalidArgumentException('l\'argument de la fonction doit être une ressource image',self::IMAGE_OPERATION_FAILED);
    }

    /**
     *
     * Méthode getResource
     *
     * récupération de l'image existante
     *
     * @return resource
     * @throws \BadFunctionCallException
     */
    protected function getResource()
    {
    	if(empty($this->_resource)){
    		$fonction = 'imagecreatefrom'.$this->getExtension();
    		if (is_callable($fonction,false,$callable_name))
    			$this->_resource=$fonction($this->_filename); // ex imagecreatefrompng()
    		else throw new \BadFunctionCallException('nom de fonction de creation d\'image invalide',self::IMAGE_OPERATION_FAILED);
    	}
    	return $this->_resource;
    }

    /**
     *
     * Méthode saveImage
     *
     * enregistrement de l'image au même format que celui fourni
     *
     * @param string $filename fichier de destination
     * @return \Framework\Entites\Image
     * @throws \BadFunctionCallException
     */
    public function saveImageAs($filename)
    {
    	if(!empty($filename) && is_string($filename)){
    		$fonction = 'image'.$this->getExtension(); //envoi de l'image vers un fichier
    		if (is_callable($fonction,false,$callable_name))
    			$fonction($this->getResource(),$filename); // ex imagepng()
    		else throw new \BadFunctionCallException('nom de fonction de sauvegarde d\'image invalide',self::IMAGE_OPERATION_FAILED);
    	}
    	$this->_filename = $filename;
    	return $this;
    }

    /**
     *
     * Méthode resize
     *
     * commande modifiant la taille d'une image
     *
     * @param int $largeurMax
     * @param int $hauteurMax
     * @return \Framework\Entites\Image
     * @throws \RuntimeException
     */
    public function resize($largeurMax,$hauteurMax)
    {
    	if ($this->_info[0] <= $largeurMax && $this->_info[1]<= $hauteurMax) {
    		//inutile de redimensionner
    		$newWidth = $this->_info[0];
    		$newHeight = $this->_info[1];
    	} else {
    		//respect des proportions
    		if($largeurMax/$this->_info[0] > $hauteurMax/$this->_info[1]) {
    			$newWidth = (int)round($this->_info[0]*($hauteurMax/$this->_info[1]));
    			$newHeight = $hauteurMax;
    		} else {
    			$newHeight = (int)round($this->_info[1]*($largeurMax/$this->_info[0]));
    			$newWidth = $largeurMax;
    		}
    	}

    	$newImage = imagecreatetruecolor($newWidth, $newHeight);// créer la zone blanche de travail

    	// gestion de la palette de couleur pour certains types
    	if($this->_info[2] == IMAGETYPE_PNG || $this->_info[2] == IMAGETYPE_GIF){
    		imagealphablending($newImage, false);
    		imagesavealpha($newImage, true);
    		$transparent=imagecolorallocatealpha($newImage, 255, 255, 255, 127);
    		imagefilledrectangle($newImage, 0, 0, $newWidth, $newHeight, $transparent);
    	}

    	//copie, redimensionnement et rééchantillonnage de l'image en ressource
    	if(!imagecopyresampled($newImage, $this->getResource(), 0, 0, 0, 0, $newWidth, $newHeight, $this->_info[0], $this->_info[1]))
    		throw new \RuntimeException('redimensionnement de l\'image',self::IMAGE_OPERATION_FAILED);

    	imagedestroy($this->_resource); // libération de mémoire

    	$this->setResource($newImage);
    	$this->setInfo(array($newWidth,$newHeight));
    	return $this;
    }
}














