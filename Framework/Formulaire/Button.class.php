<?php
/**
 * @author Frédéric Tarreau
 *
 * 28 sept. 2014 - Button.class.php
 *
 * Classe dont le rôle est de représenter un bouton de validation d'un formulaire. Elle prend en compte les champs cachés des données
 * "hidden" à envoyer sans saisie de l'utilisateur.
 *
 */
namespace Framework\Formulaire;

class Button
{
	// @var string Nom de l'action associée
	protected $_action;

	// @var string type de bouton : submit, reset, image, button
	protected $_type;

	// @var string nom du champ caché "hidden"
	protected $_hiddenName;

	// @var mixed valeur associée au champ, hidden
	protected $_hiddenValue;

	//@var string nom facultatif associé au bouton
	protected $_name;

	const FORME_TYPE_INVALIDE = 1;

	/**
	* Le constructeur demande la liste des attributs avec leur valeur, avec une valeur par défaut pour les champs cachés
	*
	*/
	public function __construct($type,$action,$name='',$hiddenName='',$hiddenValue='')
	{
	   $this->setType($type);
	   $this->setAction($action);
	   if(!empty($name))
	       $this->setName($name);

	   if((!empty($hiddenName))&&(!empty($hiddenValue))){
		  $this->setHiddenName($hiddenName);
	      $this->setHiddenValue($hiddenValue);
	   }
	}

	/**
	 *
	 * Méthode buildWidget
	 *
	 * Cette méthode permet de construire le bouton en HTML5
	 *
	 * @return string $widget chaîne de caratère représentant le code HTML 5
	 *
	 */
	public function buildWidget()
	{
	  $widget='';

      // s'il faut transférer des données cachés, créer le champ de type "hidden"
	  if(isset($this->_hiddenName,$this->_hiddenValue))
	      $widget.='<input type="hidden" name="'.$this->_hiddenName.'" value="'.$this->_hiddenValue.'"/>';

	  $widget.='<input type="'.$this->_type.'"';
	  if(isset($this->_name))
	      $widget.=' name="'.$this->_name.'"';
	  $widget .=' value="'.$this->_action.'"/>';
	  return $widget;
	}

	/**
	* Getter de "action"
	*/
	public function action()
	{
	    return $this->_action;
	}

	/**
	 * getter de hiddenName
	 */
	public function hiddenName()
	{
	    return $this->_hiddenName;
	}

	/**
	 * getter de name
	 */
	public function name()
	{
	    return $this->_name;
	}

	/**
	* getter de hiddenValue
	*/
	public function hiddenValue()
	{
		return $this->_hiddenValue;
	}

	/**
	* getter de type
	*/
	public function type()
	{
		return $this->_type;
	}

	/**
	* setter de "type"
	*/
	public function setType($type)
	{
		if(is_string($type))
	       $this->_type=$type;
		else throw new \InvalidArgumentException('Erreur format du type de bouton',self::FORME_TYPE_INVALIDE);
	}

	/**
	 * setter de hiddenName
	 *
	 * @throws \InvalidArgumentException
	 */
	public function setHiddenName($hiddenName)
	{
	    if(is_string($hiddenName))
	        $this->_hiddenName=$hiddenName;
	    else throw new \InvalidArgumentException('Erreur format du nom du champ caché du bouton',self::FORME_TYPE_INVALIDE);
	}

	/**
	 * setter de name
	 *
	 * @throws \InvalidArgumentException
	 */
	public function setName($name)
	{
	    if(is_string($name))
	        $this->_name=$name;
	    else throw new \InvalidArgumentException('Erreur format du nom du bouton',self::FORME_TYPE_INVALIDE);
	}

	/**
	* setter de hiddenValue
	*
	* @throws \InvalidArgumentException
	*/
	public function setHiddenValue($hiddenValue)
	{
	    $this->_hiddenValue=$hiddenValue;
	}

	/**
	* setter de "action"
	*
	* @throws \InvalidArgumentException
	*/
	public function setAction($action)
	{
		if(is_string($action))
	       $this->_action=$action;
		else throw new \InvalidArgumentException('Erreur format du nom de l\'action associée au bouton',self::FORME_TYPE_INVALIDE);
	}
}