<?php
/**
 *
 * @author Frédéric Tarreau
 *
 * 30 juil. 2015 - IsUploadedFileValidator.class.php
 *
 * classe fille de Validator dont le rôle est de vérifier que le nom de fichier correspond bien à un fichier téléchargé
 *
 */
namespace Framework\Formulaire;

class IsUploadedFileValidator extends Validator
{
	/*@var string */
	protected $_name;

	public function __construct($errorMessage,$name)
	{
		parent::__construct($errorMessage);
		$this->setName($name);
	}


	public function isValid($value=NULL)
	{
	    return (is_uploaded_file($_FILES[$this->_name]['tmp_name'])) ;
	}

	/**
	 *
	 * Méthode setName
	 *
	 * @param string $name
	 * @throws \InvalidArgumentException
	 */
	public function setName($name)
	{
		if(is_string($name))
			$this->_name=$name;
		else throw new \InvalidArgumentException('l\' attribut du validateur doit être de type string');
	}
}