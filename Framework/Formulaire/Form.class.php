<?php
/**
 *
 * @author Frédéric Tarreau
 *
 * 27 févr. 2015 - Form.class.php
 *
 * cette classe a pour rôle de représenter un formulaire composé d'une liste de champs
 *
 */
namespace Framework\Formulaire;

class Form
{
	/* objet instance d'une classe fille de Entité, auquel sera rattaché le formulaire  */
	protected $entite;

	/* @var string methode associée au formulaire */
	protected $methode;

	/* @var string action associée au formulaire */
	protected $action;

	/* @var array liste des fieldset du formulaire */
	protected $_fieldSets;

	/* @var array liste des boutons sous forme de tableau */
	protected $buttons;

	/* @var array liste des couples nom/valeur des champs valides sous forme de tableau associatif */
	protected $_validField;

	/* @var mixed valeur optionnelle à passer dans le formulaire */
	protected $value;

	/* @var string  correspondant à l'attribut enctype pour l'envoi de données binaires */
	protected $_enctype;

	const ATTRIBUT_FORMULAIRE_INVALIDE = 301;

	public function __construct(\Framework\Entite $entite = NULL,$donnees)
	{
		if(isset($entite)&&(!empty($entite)))
		    $this->setEntite($entite);
		$this->hydrate($donnees);
	}

	/**
	 * Méthode hydrate
	 *
	 * cette méthode permet d'hydrater le champ avec les valeurs passées en paramètre au constructeur
	 *
	 * @param array $options
	 */
	public function hydrate($options)
	{
	    foreach($options as $type=>$value){
	        $method='set'.ucfirst($type);
	        if (is_callable(array($this,$method)))
	            $this->$method($value);
	    }
	}

	/**
	*
	* Méthode addFieldSet
	*
	* Méthode permettant d'ajouter des fieldsets au formulaire
	*
	* @param FieldSet à ajouter
	* @return Form nouveau formulaire
	*/
	public function addFieldSet(FieldSet $fieldSet)
	{
		if(!empty($fieldSet->fields())){
			foreach ($fieldSet->fields() as $field){
				$attr=$field->name();// récupération du nom du champ
				if(method_exists($this->entite,$attr))
					$field->setValue($this->entite->$attr());// assignation de la valeur correspondante au champ
			}
		}

	    if(!empty($fieldSet->selects())){
	    	foreach ($fieldSet->selects() as $select){
	    		$attr=$select->name();
	    		if(!empty($select->options())){
	    			foreach ($select->options() as $option){
	    				if($option->value()==$this->entite->$attr())
	    					$option->setSelected(TRUE);
	    			}
	    		}
	    	}
	    }

	    // ajout du fieldset à la liste des fieldsets
		$this->_fieldSets[]=$fieldSet;
		return $this;
	}

	/**
	 *
	 * Méthode addButton
	 *
	 * Cette méthode permet d'ajouter un bouton au formulaire
	 *
	 * @param Button $button
	 * @return \Framework\Formulaire\Form
	 */
	public function addButton(Button $button)
	{
	    $this->buttons[]=$button;// ajout du bouton passé en argument à la liste des boutons
	    return $this;
	}

	/**
	 *
	 * Méthode baliseForm
	 *
	 * Cette méthode permet de créer les balises HTML 5 d'ouverture et de cloture d'un formulaire
	 *
	 * Remarque : l'utilisation des classes bootstrap CSS ici
	 *
	 * @param string $methode méthode associée au formulaire
	 * @param string $action action associée au formulaire
	 */
	public function baliseForm()
	{
		if(isset($this->methode,$this->action)){
			if(empty($this->_enctype))
				$balise='<form class="form-horizontal" method="'.$this->methode.'" action="'.$this->action.'"><div class="form-group">';
			else $balise='<form class="form-horizontal" method="'.$this->methode.'" action="'.$this->action
						.'" enctype="' .$this->_enctype .'">'
						.'<div class="form-group">';
	   }
	   else $balise='<form class="form-horizontal"><div class="form-group">';
	   return $balise;
	}

	/**
	* Méthode createView
	*
	* permet de générer l'affichage de tous les champs associés au formulaire
	*
	* @return string View, portion de vue correspondant au formulaire c.a.d HTML
	*/
	public function createView()
	{
	    $view = $this->baliseForm();

	    if(!empty($this->_fieldSets)) {
	        foreach ($this->_fieldSets as $fieldSet) {
	            // un fieldset avec une entrée est considéré comme un champ simple
	            if(count($this->_fieldSets)>0)
	                $view.=$fieldSet->buildWidget();

	            // g�n�ration pas � pas de chaque fieldset
	            if (!empty($fieldSet->selects())){
	            	foreach($fieldSet->selects() as $select){
	            		$view.=$select->buildWidget();
	            	}
	            }

	            if (!empty($fieldSet->fields())){
	                foreach($fieldSet->fields() as $field){
	                    $view.=$field->buildWidget();
	                }
	            }

	            if (!empty($fieldSet->buttons())) {
	                foreach($fieldSet->buttons() as $button){
	                    $view.=$button->buildWidget();
	                }
	            }
	            if(count($this->_fieldSets)>0)
	                $view.='</fieldset>';
	        }
	    }

	    // insertion des éventuels boutons du formulaire
	    if (!empty($this->buttons)){
	        foreach($this->buttons as $button) {
	            $view.=$button->buildWidget();
	        }
	    }
		$view.='</div></form>';//balise de cloture du formulaire
		return $view;
	}

	/**
	* Méthode isValid
	*
	* Cette méthode permet d'acter que tous les champs du formulaire sont valides
	*
	* @return Boolean Vrai si le formulaire est valide, Faux sinon
	*/
	public function isValid()
	{
		$valid=true;
		//vérification pas à pas que les champs sont valides
		foreach($this->_fieldSets as $fieldSet){
			if(!empty($fieldSet->fields())){
				foreach($fieldSet->fields() as $field){
					if(!$field->isValid())
						$valid=false;
					else $this->_validField[$field->name()]=$field->value();
				}
			}
		}
		return $valid;
	}

	/**
	* setter de entite
	*
	* @param \Framework\Entite
	*/
	public function setEntite(\Framework\Entite $entite)
	{
		$this->entite=$entite;
	}

	/**
	* getter de entite
	*/
	public function entite()
	{
		return $this->entite;
	}

	/**
	 * setter de methode
	 *
	 * @param string
	 * @throws \InvalidArgumentException
	 */
	public function setMethode($methode)
	{
	    if(is_string($methode))
	         $this->methode=$methode;
	     else throw new \InvalidArgumentException('Erreur format de methode du formulaire');
	}

	/**
	 *
	 * getter de methode
	 *
	 * @return string
	 */
	public function methode()
	{
	    return $this->methode;
	}

	/**
	 * setter de action
	 *
	 * @param string
	 * @throws \InvalidArgumentException
	 */
	public function setAction($action)
	{
	    if(is_string($action))
	       $this->action=$action;
	    else throw new \InvalidArgumentException('Erreur format de l\'action associée au champ');
	}

	/**
	 * getter de action
	 *
	 * @return string
	 */
	public function action()
	{
	    return $this->action;
	}

	/**
	 * setter de value
	 *
	 * @param mixed
	 */
	public function setValue($value)
	{
	    $this->value=$value;
	}

	/**
	 *
	 * getter de value
	 *
	 * @return mixed
	 */
	public function value()
	{
	    return $this->value;
	}

	/**
	 * setter de validField
	 *
	 * @param array
	 * @throws \InvalidArgumentException
	 */
	public function setValidField($validField)
	{
		if(is_array($validField))
			$this->_validField=$validField;
		else throw new \InvalidArgumentException('Erreur de type validField du formulaire');
    }

	/**
	 * getter de validField
	 */
	public function validField()
	{
		return (empty($this->_validField) ?  array() : $this->_validField);
	}

	/**
	 * setter de enctype
	 *
	 * @param string
	 * @throws \InvalidArgumentException
	 * @throws \DomainException
	 */
	public function setEnctype($enctype)
	{
		$valeurAutoriseeHTML5 = array ('application/x-www-form-urlencoded', 'multipart/form-data', 'text/plain');
		if(is_string($enctype)){
			if(in_array($enctype, $valeurAutoriseeHTML5))
				$this->_enctype=$enctype;
			else throw new \DomainException('valeur enctype non valide',self::ATTRIBUT_FORMULAIRE_INVALIDE);
		}
		else throw new \InvalidArgumentException('Erreur de type validField du formulaire');
	}

	/**
	 * getter de enctype
	 */
	public function enctype()
	{
		return $this->_enctype;
	}
}
