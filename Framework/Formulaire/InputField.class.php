<?php
/**
 *
 * @author Frédéric Tarreau
 *
 * 5 mai 2015 - InputField.class.php
 *
 * classe héritée de Field représentant un champ de formulaire sous forme de chaîne de caractère
 *
 */
namespace Framework\Formulaire;

class InputField extends Field
{
	/* @var longueur maximale du champ */
	protected $maxLength;

	/* @var taille du champ */
	protected $size;

	/* @var type du champ input */
	protected $_type;

	/* @var pattern du champ */
	protected $_pattern;

	/* @var string correspondant à la liste de types MIME pour les "input" de type "file" */
	protected $_accept;

	const TYPE_INPUT_INVALIDE = 302;
	const ATTRIBUT_INPUT_INVALIDE = 302;

	/**
	 * Méthode isValid
	 *
	 * surcharge de IsValid en spécifiant un contournement s'il s'agit d'un champ de type hidden
	 *
	 *@return boolean Vrai si le champ est valide

	public function isValid()
	{
		if($this->_type ==='hidden')
			return true;
		else parent::isValid();
	}*/

	/**
	 * Méthode permettant de générer le code HTML pour un champ input
	 * @see \Framework\Formulaire\Field::buildWidget()
	 */
	public function buildWidget()
	{
		$widget='';

		// affichage du message d'erreur lié au champ
		if (!empty($this->errorMessage))
			$widget.=$this->errorMessage.'<br />';

		//début du code HTML du champ avec le label, le type et le nom
		$widget.='<div class="row">
		          <div class="form-group">
		          <label for="'.$this->id.'" class="col-xs-4 control-label">'.$this->label.'</label>
		          <div class="col-xs-8">
		          <input type="'.$this->_type.'" class="form-control" name="'.$this->name.'" id="'.$this->id.'"';

		// s'il y a une valeur, échapper les caractères HTML
		if(!empty($this->value))
			$widget.=' value="'.htmlspecialchars($this->value).'"';

		//prise en compte de la chaîne de caractère d'indication pour l'utilisateur, si elle existe
		if(!empty($this->placeholder))
			$widget.=' placeholder="'.htmlspecialchars($this->placeholder).'"';

		//prise en compte de la taille du champ, si elle existe
		if(!empty($this->size))
			$widget.='size="'.htmlspecialchars($this->size).'"';

		//prise en compte de la longueur maximale, si elle existe
		if(!empty($this->maxlength))
			$widget.='maxlength="'.htmlspecialchars($this->maxLength).'"';

		//prise en compte du pattern si il existe
		if(!empty($this->_pattern))
		    $widget.='pattern="'.$this->_pattern.'"';

		if (!empty($this->_accept))
			$widget.='accept="'.$this->_accept.'"';

		if($this->required===true)
			$widget.=' required';
		if ($this->autofocus===true)
			$widget.=' autofocus';
		if ($this->disabled===true)
		    $widget.=' disabled';

		$widget.=' /></div></div></div>';
		return $widget;
	}

	/**
	* setter de maxLength
	*
	* @param int
	* @throws \InvalidArgumentException
	*/
	public function setMaxLength($maxLength)
	{
		if(is_int($maxLength) && $maxLength >0)
			$this->maxLength=$maxLength;
		else throw new \InvalidArgumentException('la longueur maximale du champ doit être un entier positif');
	}

	/**
	* setter de size
	*
	* @param int
	* @throws \InvalidArgumentException
	*/
	public function setSize($size)
	{
		if(is_int($size) && $size >0)
			$this->size=$size;
		else throw new \InvalidArgumentException('la longueur du champ doit être un entier positif');
	}

    /**
     *
     * Méthode setType
     *
     * Il s'agit du setter de l'attribut type. Il doit vérifier que le type correspond à un des types
     * de l'élément <input> de HTML5
     *
     * @param string $type
     * @throws \DomainException
     */
	public function setType($type)
	{
	    $valeurAutoriseeHTML5 = array ('text', 'password', 'hidden', 'radio', 'checkbox', 'reset', 'submit', 'image', 'file', 'tel',
	                                   'url', 'email','search','date','time', 'datetime', 'datetime-local', 'month','week',
	                                   'number','range','color');

	    if(is_string($type)){
	    	if (in_array($type,$valeurAutoriseeHTML5,TRUE))
	    		$this->_type=$type;
	    	else throw new \DomainException('le type de champ spécifié n\'existe pas dans HTML5',self::TYPE_INPUT_INVALIDE);
	    } else throw new \InvalidArgumentException('attribut type doit être une chaîne de caractère');
	}

	/**
	 *
	 * Méthode type
	 *
	 * il s'agit du getter de type
	 *
	 * @return string $_type
	 *
	 */
	public function type()
	{
	    return $this->_type;
	}

	/**
	 *
	 * Méthode setPattern
	 *
	 * il s'agit du setter de l'attribut pattern
	 *
	 * @param string $pattern
	 * @throws \InvalidArgumentException
	 */
	public function setPattern($pattern)
	{
	    if(is_string($pattern))
	        $this->_pattern=$pattern;
	    else throw new \InvalidArgumentException('le pattern du champ n\'est pas valide');
	}

	/**
	 *
	 * Méthode pattern
	 *
	 * il s'agit du getter de l'attribut pattern
	 *
	 * @return string $_pattern , attribut de la classe
	 */
	public function pattern()
	{
	    return $this->_pattern;
	}

	/**
	 * setter de accept
	 *
	 * @param string
	 * @throws \InvalidArgumentException
	 * @throws \DomainException
	 */
	public function setAccept($accept)
	{
		$typeImageAuthorises = \Framework\Configuration::get(NULL,NULL,'extension_image');
		if(is_string($accept)){
			if(strpos($accept,'image')!=FALSE){
				// prise en compte des type d'image autorisé dans le fichier de configuration
				$extensions = explode(',',str_replace('image/','', $extension));
				$authorised = array();
				foreach ($extensions as $extension){
					if(array_key_exists($extension,$typeImageAuthorises))
						$authorised[]=$extension .',';
				}
				if(empty($authorised))
					throw new \DomainException('le type d\'image du champ est invalide ou non autorisé en fichier de configuration',self::ATTRIBUT_INPUT_INVALIDE);
				else $accept=implode('image/',$authorised);
			}
			$this->_accept=$accept;
		}else throw new \InvalidArgumentException('Erreur de type "accept" du champ');
	}

	/**
	 * getter de accept
	 */
	public function accept()
	{
		return $this->_accept;
	}
}