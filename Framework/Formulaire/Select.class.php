<?php
/**
 *
 * @author Frédéric Tarreau
 *
 * 07 juin 2015 - Select.class.php
 *
 * classe abstraite dont le rôle est de représenter une liste d'option HTML
 */
namespace Framework\Formulaire;

class Select
{
	/* @var int nombre d'options à présenter à l'utilisateur HTML5 */
    protected $_size;

    /* @var string nom clé de la paire clé/valeur à soumettre au formulaire */
    protected $_name;

    /* @var booléen permettant de sélectionner plusieurs options simultanément */
    protected $_multiple;

    /* @var array liste des "options" */
    protected $_options;

    /* @var string label associé */
    protected $_label;

    /**
     * Le constructeur demande la liste des attributs avec leur valeur afin d'hydrater l'objet
     *
     * @param array options correspondant à la liste des attributs avec leur valeur
     */
    public function __construct(array $params=array())
    {
        if(!empty($params))
            $this->hydrate($params);
    }

    /**
     * Méthode hydrate
     *
     * cette méthode permet d'hydrater la liste avec les valeurs passées en paramètre au constructeur
     *
     * @param array $params
     */
    public function hydrate($params)
    {
        foreach($params as $type=>$value){
            $method='set'.ucfirst($type);
            if (is_callable(array($this,$method)))
                $this->$method($value);
        }
    }

	/**
	 *
	 * Méthode addOption
	 *
	 * Méthode permettant d'ajouter des options à la liste de choix
	 *
	 * @param option à ajouter
	 * @return Select
	 */
	public function addOption(Option $option)
	{
	    $this->_options[]=$option;
	    return $this;
	}

	/**
	 *
	 * Méthode buildWidget
	 *
	 * Permet de générer les balises HTML correspondant à une liste "select"
	 *
	 *@return string chaîne de code HTML 5
	 */
	public function buildWidget()
	{
		$widget='<p>';

		if(!empty($this->_label))
			$widget.='<label for="' .$this->_name .'">' .$this->_label .'&nbsp</label>';

		$widget.='<select name="' .$this->_name .'" ';

	   	if(!empty($this->_size))
	   		$widget.='size="' .$this->_size .'" ';
	   	if(!empty($this->_multiple))
	   		$widget.= 'multiple ';
	   	$widget.= '>';

	   	foreach ($this->options() as $option){
	   		if(!empty($option->optgroupLabel()))
	   			$groups[$option->optgroupLabel()][]=$option->buildwidget();
	  		else $widget.=$option->buildwidget();
	   	}

	   	if(!empty($groups)){
	   		foreach($groups as $cle=>$group) {
	   			$widget.= '<optgroup label="' .$cle .'">';
	   			foreach ($group as $choix){
	   				$widget.= $choix;
	   			}
	   			$widget.= '</optgroup>';
	   		}
	   	}

	  	$widget.= '</select></p>';
	    return $widget;
	}

	/**
	 *
	 * getter de l'attribut protégé options
	 *
	 * @return array options liste des éléments "option" de la liste
	 *
	 */
	public function options()
	{
	    return $this->_options;
	}

   /**
	*
	* getter de l'attribut protégé name
	*
	* @return string $_name
	*/
	public function name()
	{
		return $this->_name;
	}

	/**
	 *
	 * setter de l'attribut protégé name
	 *
	 * @param string $name
	 * @throws \InvaliArgumentException
	 */
	public function setName($name)
	{
		if(is_string($name))
			$this->_name=$name;
		else throw new \InvalidArgumentException('Erreur de type du nom de la liste d\'option');
	}

	/**
	 *
	 * getter de l'attribut protégé label
	 *
	 * @return string $_label
	 */
	public function label()
	{
		return $this->_label;
	}

	/**
	 *
	 * setter de l'attribut protégé label
	 *
	 * @param string $label
	 * @throws \InvaliArgumentException
	 */
	public function setLabel($label)
	{
		if(is_string($label))
			$this->_label=$label;
		else throw new \InvalidArgumentException('Erreur de type du label de la liste d\'option');
	}

	/**
	 *
	 * getter de l'attribut protégé size
	 *
	 * @return int $_size
	 */
	public function size()
	{
		return $this->_size;
	}

	/**
	 *
	 * setter de l'attribut protégé size
	 *
	 * @param int $size
	 * @throws \InvaliArgumentException
	 */
	public function setSize($size)
	{
		if(is_int($size) && $size>0)
			$this->_size = $size;
		else throw new \InvalidArgumentException('Erreur de type sur la taille de la liste d\'option');
	}

	/**
	 *
	 * getter de l'attribut protégé multiple
	 *
	 * @return boolean $_multiple
	 */
	public function multiple()
	{
		return $this->_multiple;
	}

	/**
	 *
	 * setter de l'attribut protégé setMultiple
	 *
	 * @param boolean $multiple
	 * @throws \InvaliArgumentException
	 */
	public function setMultiple($multiple)
	{
		if(is_bool($multiple))
			$this->_multiple=$multiple;
		else throw new \InvalidArgumentException('Erreur de type de l\'attribut multiple de la liste d\'option');
	}
}