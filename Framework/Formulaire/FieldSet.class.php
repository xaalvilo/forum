<?php
/**
 *
 * @author Frédéric Tarreau
 *
 * 27 févr. 2015 - FieldSet.class.php
 *
 * classe abstraite dont le rôle est de représenter un Fieldset regroupant des champs de formulaire
 */
namespace Framework\Formulaire;

class FieldSet
{
	/* @var string legende du fieldset HTML5 */
    protected $_legend;

    /* @var array liste des champs du fieldset */
    protected $_fields;

    /* @var array listes de choix */
    protected $_selects;

    /* @var array liste des boutons du fieldset */
    protected $_buttons;

    /**
     * Le constructeur  demande la liste des attributs avec leur valeur afin d'hydrater l'objet
     *
     * @param array options correspondant à la liste des attributs avec leur valeur
     */
    public function __construct(array $options=array())
    {
        if(!empty($options))
            $this->hydrate($options);
    }

    /**
     * Méthode hydrate
     *
     * cette méthode permet d'hydrater le champ avec les valeurs passées en paramètre au constructeur
     *
     * @param array $options
     */
    public function hydrate($options)
    {
        foreach($options as $type=>$value){
            $method='set'.ucfirst($type);
            if (is_callable(array($this,$method)))
                $this->$method($value);
        }
    }

	/**
	 *
	 * Méthode addField
	 *
	 * Méthode permettant d'ajouter des champs au fieldset
	 *
	 * @param Field à ajouter
	 * @return FieldSet
	 */
	public function addField(Field $field)
	{
	    $this->_fields[]=$field;
	    return $this;
	}

	/**
	 *
	 * Méthode addSelect
	 *
	 * Méthode permettant d'ajouter des listes de choix au fieldset
	 *
	 * @param Select à ajouter
	 * @return FieldSet
	 */
	public function addSelect(Select $select)
	{
		$this->_selects[]=$select;
		return $this;
	}

	/**
	 *
	 * Méthode buildWidget
	 *
	 * Permet de générer les balises HTML correspondant à un fieldset
	 *
	 *@return string chaîne de code HTML
	 */
	public function buildWidget()
	{
	    $widget='<fieldset>';

	    // une legende est spécifiée
	    if(!empty($this->_legend))
	        $widget.='<legend>'.$this->_legend.'</legend>';
	    return $widget;
	}

	/**
	 *
	 * Méthode addButton
	 *
	 * Cette méthode permet d'ajouter un bouton au formulaire
	 *
	 * @param Button $button
	 * @return FieldSet
	 */
	public function addButton(Button $button)
	{
	    // ajout du bouton passé en argument à la liste des boutons
	    $this->_buttons[]=$button;
	    return $this;
	}

	/**
	 *
	 * Méthode fields
	 *
	 * getter de l'attribut protégé fields
	 *
	 * @return array fields liste de champs de formulaire
	 *
	 */
	public function fields()
	{
	    return $this->_fields;
	}

	/**
	 *
	 * Méthode selects
	 *
	 * getter de l'attribut protégé selects
	 *
	 * @return array selects listes de choix de formulaire
	 *
	 */
	public function selects()
	{
		return $this->_selects;
	}

   /**
	*
	* Méthode buttons
	*
	* getter de l'attribut protégé buttons
	*
	* @return array buttons liste de boutons du formulaire
	*
	*/
	public function buttons()
	{
		return $this->_buttons;
	}

	/**
	 *
	 * Méthode legend
	 *
	 * getter de l'attribut protégé legend
	 *
	 * @return string
	 */
	public function legend()
	{
		return $this->_legend;
	}

	/**
	 *
	 * Méthode setLegend
	 *
	 * @param string $legend
	 * @throws \InvalidArgumentException
	 */
	public function setLegend($legend)
	{
		if(is_string($legend))
			$this->_legend=$legend;
		else throw new \InvalidArgumentException('Erreur format de legende du fieldset');
	}
}