<?php
/**
* classe abstraite dont le rôle est de représenter un champ de formulaire
*
*/
namespace Framework\Formulaire;

abstract class Field
{
	// label HTML du champ
	protected $label;

	// Nom HTML du champ. Il est rappelé que ce nom apparaît dans les superglobales sous forme de tableau
	// $GET, $POST, $REQUEST, il est donc important de ne pas choisir ce nom au hasard par rapport aux objets à hydrater
	// avec les valeurs des INPUT
	protected $name;

	// valeur du champ
	protected $value;

	// id du champ, permettant en HTML de li� le champ à son nom
	protected $id;

	// message d'erreur associ� au champ
	protected $errorMessage;

	//tableau des validateurs
	protected $validators = array();

	// indication pour remplir le champ
	protected $placeholder;

	// indication HTML de champ obligatoire
	protected $required;

	// indication d'autofocus au chargement de la page
	protected $autofocus;

	// blocage de la saisie d'un champ
	protected $disabled;


	/**
	* Le constructeur  demande la liste des attributs avec leur valeur afin d'hydrater l'objet
	*
	* @param array options correspondant à la liste des attributs avec leur valeur
	*/
	public function __construct(array $options=array())
	{
		if(!empty($options))
			$this->hydrate($options);
	}

	/**
	* Méthode abstraite à implémenter par les classes filles permettant de construire le code HTML de chaque champ
	*/
	abstract public function buildWidget() ;

	/**
	* Méthode hydrate
	*
	* cette méthode permet d'hydrater le champ avec les valeurs passées en paramètre au constructeur
	*
	* @param array $options
	*/
	public function hydrate($options)
	{
		foreach($options as $type=>$value){
			$method='set'.ucfirst($type);
			if (is_callable(array($this,$method)))
				$this->$method($value);
		}
	}

	/**
	* Méthode isValid
	*
	* permettant d'acter que la valeur d'un champ est valide ou qu'il est considéré comme tel car
	* sans valeur et non requis
	*
	*@return boolean Vrai si le champ est valide
	*/
	public function isValid()
	{
		if ((isset($this->required) && !empty($this->required))
			||(isset($this->value) && !empty($this->value))){
			foreach ($this->validators as $validator){
				if (!$validator->isValid($this->value)){
					$this->errorMessage = $validator->errorMessage();
					return FALSE;
				}
			}
			return TRUE;
		} else return TRUE;
	}

	/**
	* getter de label
	*/
	public function label()
	{
		return $this->label;
	}

	/**
	* getter de name
	*/
	public function name()
	{
		return $this->name;
	}

	/**
	* getter de value
	*/
	public function value()
	{
		return $this->value;
	}

	/**
	* getter de l'id
	*/
	public function id()
	{
		return $this->id;
	}

	/**
	 * getter de validators
	 */
	public function validators()
	{
		return $this->validators;
	}

	/**
	 * getter de placeholder
	 */
	public function placeholder()
	{
		return $this->placeholder;
	}

	/**
	 * getter de required
	 */
	public function required()
	{
		return $this->required;
	}

	/**
	 * getter de autofocus
	 */
	public function autofocus()
	{
	    return $this->autofocus;
	}

	/**
	 * getter de disabled
	 */
	public function disabled()
	{
	    return $this->disabled;
	}

	/**
	 * setter de validators
	 *
	 * @param array $validators
	 * @throws \Exception
	 */
	public function setValidators(array $validators)
	{
		foreach ($validators as $validator){
			if($validator instanceof Validator && !in_array($validator,$this->validators))
				$this->validators[] = $validator;
			else throw new \InvalidArgumentException('Erreur dans le tableau de validateur du champ');
		}
	}

	/**
	* setter de label
	*/
	public function setLabel($label)
	{
		$this->label=$label;
	}

	/**
	* setter de name
	*
	* @param string name
	* @throws \InvalidArgumentException
	*/
	public function setName($name)
	{
		if(is_string($name))
			$this->name=$name;
		else throw new \InvalidArgumentException('Erreur format du nom de champ');
	}

	/**
	* setter de value
	*/
	public function setValue($value)
	{
		$this->value=$value;
	}

	/**
	* setter de l'id
	*/
	public function setId($id)
	{
		$this->id=$id;
	}

	/**
	 * setter de placeholder
	 */
	public function setPlaceholder($placeholder)
	{
		$this->placeholder=$placeholder;
	}

	/**
	 *
	 * Setter de required
	 *
	 * @param boolean $required
	 * @throws \InvalidArgumentException
	 */
	public function setRequired($required)
	{
		if(is_bool($required))
			$this->required = $required;
		else throw new \InvalidArgumentException('Erreur format de l\'attribut required du champ');
	}

	/**
	 *
	 * Setter de autofocus
	 *
	 * @param boolean $autofocus
	 * @throws \InvalidArgumentException
	 */
	public function setAutofocus($autofocus)
	{
		if(is_bool($autofocus))
			$this->autofocus = $autofocus;
		else throw new \InvalidArgumentException('Erreur format de l\'attribut autofocus du champ');
	}

	/**
	 *
	 * Setter de disabled
	 *
	 * @param boolean $disabled
	 * @throws \InvalidArgumentException
	 */
	public function setDisabled($disabled)
	{
		if(is_bool($disabled))
			$this->disabled = $disabled;
		else throw new \InvalidArgumentException('Erreur format de l\'attribut disabled du champ');
	}
}