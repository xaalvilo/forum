<?php
/**
 *
 * @author Frédéric Tarreau
 *
 * 18 sept. 2014 - FormBuilder.class.php
 *
 * Classe abstraire dont le r�le est la construction d'un formulaire
 *
 */
namespace Framework\Formulaire;

abstract class FormBuilder
{
    /* constante type de formulaire */
    CONST LONG=1;
    CONST MOYEN=2;
    CONST COURT=3;
    CONST PETIT=4;
    CONST BOUTON=5;
    CONST LISTE=6;

	/* @var formulaire � cr�er */
	protected $form;

	/**
	 * Constructeur
	 *
	 * @param Entite $entity
	 */
	public function __construct(\Framework\Entite $entite = NULL, array $donnees)
	{
	    if(!empty($entite))
	        $this->setForm(new Form($entite,$donnees));
	    else
	        $this->setForm(new Form(NULL,$donnees));
	}

	/**
	 *
	 * Méthode abstraite build
	 *
	 * permet de construire le formulaire , ses champs et ses boutons
	 *
	 * @param int $type précisant un type de formulaire à construire (ex : long, court...)
	 *
	 */
	abstract public function build($type = NULL);

	/**
	 *
	 * Setter
	 *
	 * return_type
	 *
	 * @param \Framework\Formulaire\Form $form
	 */
	public function setForm($form)
	{
		$this->form = $form;
	}

	/**
	 *
	 * Getter
	 *
	 * @return \Framework\Formulaire\Form Form
	 */
	public function form()
	{
		return $this->form;
	}
}
