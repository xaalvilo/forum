<?php
/**
* classe fille de Validator dont le rôle est de vérifer qu'une donnée entrée dans un champ de formulaire
* n'est pas nulle
*/

namespace Framework\Formulaire;

class MaxLengthValidator extends Validator
{
	// longueur maximale du champ
	protected $maxLength;

	public function __construct($errorMessage,$maxLength)
	{
		parent::__construct($errorMessage);
		$this->setMaxLength($maxLength);
	}

	public function isValid($value)
	{
		// calculer la taille de la chaîne et vérifier qu'elle est inférieure à la taille de chaîne maximale
		return strlen($value) <= $this->maxLength;
	}

	/**
	* setter de maxLength
	*
	* @param int
	* @throws \InvalidArgumentException
	*/
	public function setMaxLength($maxLength)
	{
		if(is_int($maxLength) && $maxLength >0)
			$this->maxLength=$maxLength;
		else throw new \InvalidArgumentException('l\' attribut longueur maximale du validateur doit être un entier positif');
	}
}