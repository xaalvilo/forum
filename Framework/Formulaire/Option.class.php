<?php
/**
 *
 * @author Frédéric Tarreau
 *
 * 8 juin 2015 - Option.class.php
 *
 * classe représentant un élément "option" HTML5
 *
 */
namespace Framework\Formulaire;

class Option
{
	/* @var mixed valeur envoyée par le formulaire */
	protected $_value;

	/* @var booléan sélection ou non d'une option par défaut */
	protected $_selected;

	/* @var booléan disabled, état désactivé ou pas */
	protected $_disabled;

	/* @var string label */
	protected $_label;

	/* @var string contenu, texte qui s'affiche */
	protected $_contenu;

	/* @var string label du groupe d'option */
	protected $_optgroupLabel;

	/**
	* Le constructeur  demande la liste des attributs avec leur valeur afin d'hydrater l'objet
	*
	* @param array options correspondant à la liste des attributs avec leur valeur
	*/
	public function __construct(array $params=array())
	{
		if(!empty($params))
			$this->hydrate($params);
	}

	/**
	* Méthode buildWidget
	*
	* permet de générer le code HTML5 d'une balise "option"
	*/
	public function buildWidget()
	{
		$widget = '<option ';

		if(!empty($this->_label))
			$widget.= 'label="' .$this->_label .'" ';

		$widget.= 'value="' .$this->_value .'" ';

		if(!empty($this->_selected) && $this->_selected)
			$widget.= 'selected';
		else if(!empty($this->_disabled) && $this->_disabled)
			$widget.= 'disabled';

		$widget.= '>' .$this->_contenu .'</option>';
		return $widget;
	}

	/**
	* Méthode hydrate
	*
	* cette méthode permet d'hydrater le champ avec les valeurs passées en paramètre au constructeur
	*
	* @param array $params
	*/
	public function hydrate($params)
	{
		foreach($params as $type=>$value){
			$method='set'.ucfirst($type);
			if (is_callable(array($this,$method)))
				$this->$method($value);
		}
	}

	/**
	* getter de value
	*/
	public function value()
	{
		return $this->_value;
	}

	/**
	 * getter de label
	 */
	public function label()
	{
		return $this->_label;
	}

	/**
	 * getter de optgroupLabel
	 */
	public function optgroupLabel()
	{
		return $this->_optgroupLabel;
	}

	/**
	 * getter de contenu
	 */
	public function contenu()
	{
		return $this->_contenu;
	}

	/**
	 * getter de selected
	 */
	public function selected()
	{
		return $this->_selected;
	}

	/**
	 * getter de disabled
	 */
	public function disabled()
	{
		return $this->_disabled;
	}

	/**
	* setter de value
	*
	* @param mixed $value
	*/
	public function setValue($value)
	{
		$this->_value=$value;
	}

	/**
	 * setter de label
	 *
	 * @param string $label
	 * @throws \InvalidArgumentException
	 */
	public function setLabel($label)
	{
		if (is_string($label))
			$this->_label=$label;
		else throw new \InvalidArgumentException('Erreur de type du label de l\'option');
	}

	/**
	 * setter de optgroupLabel
	 *
	 * @param string $optgroupLabel
	 * @throws \InvalidArgumentException
	 */
	public function setOptgroupLabel($optgroupLabel)
	{
		if (is_string($optgroupLabel))
			$this->_optgroupLabel=$optgroupLabel;
		else throw new \InvalidArgumentException('Erreur de type du groupe de l\'option');
	}

	/**
	 * setter de contenu
	 *
	 * @param string $contenu
	 * @throws \InvalidArgumentException
	 */
	public function setContenu($contenu)
	{
		if (is_string($contenu))
			$this->_contenu=$contenu;
		else throw new \InvalidArgumentException('Erreur de type du contenu de l\'option');
	}


	/**
	 *
	 * Setter de selected
	 *
	 * @param boolean $selected
	 * @throws \InvalidArgumentException
	 */
	public function setSelected($selected)
	{
		if(is_bool($selected))
			$this->_selected = $selected;
		else throw new \InvalidArgumentException('Erreur de type du l\'attribut selected de l\'option');
	}

	/**
	 *
	 * Setter de disabled
	 *
	 * @param boolean $disabled
	 * @throws \InvalidArgumentException
	 */
	public function setDisabled($disabled)
	{
		if(is_bool($disabled))
			$this->_disabled = $disabled;
		else throw new \InvalidArgumentException('Erreur de type de l\'attribut disabled de l\'option');
	}
}