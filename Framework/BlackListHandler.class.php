<?php
namespace Framework;
require_once './Framework/autoload.php';
/**
 *
 * @author Frédéric Tarreau
 *
 * 24 juin 2015 - BlackListHandler.class.php
 *
 * Cette classe gère la BlackList des utilisateurs identifiés par le couplie @IP et version Browser
 *
 */
class BlackListHandler
{
    /* @var objet SplFileInfo lié au fichier liste noire */
    private $_BlackListFileInfo;

    /* @var objet MyCsvFileHandler */
    private $_myCsvFileHandler;

    /**
     *
     * Constructeur
     *
     * @throws \Framework\Exceptions\SecuriteException
     */
    public function __construct()
    {
        $this->_myCsvFileHandler= new MyCsvFileHandler();
        try {
        	$this->createBlackList();
        }catch (\Framework\Exceptions\FileException $e){
        	throw new \Framework\Exceptions\SecuriteException($e,\Framework\SecuriteHandler::BLACKLIST_CREATION_FAILED);
        }
    }

    /**
     *
     * Méthode createBlackList
     *
     * création du fichier de liste noire au format CSV
     *
     * @throws \Framework\Exceptions\FileException en cas d'échec de création du fichier
     */
    private function createBlackList()
    {
       $filename = \Framework\Configuration::get('blacklist');
       if(is_string($filename)){
           if(!file_exists($filename)){
               touch($filename);
               chmod($filename,0600);
           }
           $this->_BlackListFileInfo = new \SplFileInfo($filename);
       } else throw new \Framework\Exceptions\FileException('echec à la création du fichier liste noire');
    }

    /**
     *
     * Méthode ckeckBlackList
     *
     * regarde si un couple de données est en liste noire
     *
     * @param string $ip
     * @param string $browser
     * @return mixed FALSE si recherche infructueuse ou numero de ligne sinon
     * @throws \Framework\Exceptions\FileException
     */
    public function checkBlackList($ip,$browser)
    {
        if ($this->_BlackListFileInfo instanceof \SplFileInfo
                && $this->_BlackListFileInfo->isFile()
                && $this->_BlackListFileInfo->isReadable()){
            if (($fileObject = $this->_myCsvFileHandler->csvOpenFile($this->_BlackListFileInfo,'rb')) instanceof \SplFileObject){
                if($fileObject->flock(LOCK_SH)){ //verrou partagé
                    $nb = $this->_myCsvFileHandler->searchInCSV($fileObject, $ip, $browser);
                    $fileObject->flock(LOCK_UN); //libere le verrou
                    return $nb;
                } else throw new \Framework\Exceptions\FileException('impossible de récupérer le verrou sur fichier de liste noire');
            } else throw new \Framework\Exceptions\FileException('fichier de liste noire inaccessible à l\'ouverture');
        } else throw new \Framework\Exceptions\FileException('fichier de liste noire inaccessible pour vérification');
    }

    /**
     *
     * Méthode addToBlackList
     *
     * inscrit le couple @IP et version de navigateur d'un utilisateur banni sur le fichier liste noire
     *
     * @param string $ip adresse ip
     * @param string $browser version du navigateur
     * @return mixed FALSE si échec de l'ajout, TRUE si délà présent, $nb taille de la chaîne ajoutée
     */
    public function addToBlackList($ip,$browser)
    {
        $nb = FALSE ;
        $fields = array($ip,$browser);

       if($this->_BlackListFileInfo->isFile()
          && $this->_BlackListFileInfo->isWritable()
          && $this->_BlackListFileInfo->isReadable()){
           if (($fileObject = $this->_myCsvFileHandler->csvOpenFile($this->_BlackListFileInfo,'ab+')) instanceof \SplFileObject){
               if($fileObject->flock(LOCK_EX)){ //verrou exclusif
                   (is_bool($this->_myCsvFileHandler->searchInCSV($fileObject, $ip, $browser))) ? // eviter un doublon
                       $nb = $fileObject->fputcsv($fields) : $nb = TRUE; // ecrire la ligne
                   $fileObject->flock(LOCK_UN); //libere le verrou
                   $fileObject->fflush(); // forcer transfert tampon vers fichier
               }
           }
       }
       return $nb;
    }

    /**
     *
     * Méthode removeFromBlackList
     *
     * supprime le couple @IP et version de navigateur d'un utilisateur banni du fichier liste noire
     *
     * @param string $ip adresse ip
     * @param string $browser version du navigateur
     * @return mixed longueur de la chaîne si succès, FALSE sinon
     */
    public function removeFromBlackList($ip,$browser)
    {
       $nb = FALSE;

       if($this->_BlackListFileInfo->isFile()
          && $this->_BlackListFileInfo->isWritable()
          && $this->_BlackListFileInfo->isReadable()){
           if(($fileObject=$this->_myCsvFileHandler->csvOpenFile('rb+')) instanceof \SplFileObject){

               if($fileObject->flock(LOCK_EX)){//verrou exclusif
                   $line = $this->_myCsvFileHandler->searchInCSV($fileObject, $ip, $browser);
                   $nb = $this->_myCsvFileHandler->replaceInCSV($fileObject,'0','0', $line);
                   $fileObject->flock(LOCK_UN); //libere le verrou
                   $fileObject->fflush(); // force transfert tampon vers fichier
               }
           }
        }
        return $nb;
    }
}