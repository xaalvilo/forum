<?php
namespace Framework;
require_once './Framework/autoload.php';
/**
 *
 * @author Frédéric Tarreau
 *
 * 31 août 2015 - UploadFileHandler.class.php
 *
 * Cette classe gère un fichier téléchargé avant qu'il ne quitte le répertoire temporaire
 *
 */
class UploadFileHandler extends ApplicationComponent
{
	/* @var string name - attribut name du formulaire */
	protected $_name;

	/* @var boolean - flag */
	protected $_isValid;

    /* @var code erreur pour les exceptions */
    CONST NO_UPLOADED_FILE_IN_TMP = 401;
    CONST INVALID_FILENAME = 402;
    CONST DELETE_UPLOADED_FILE_FAILED = 403;
    CONST MOVE_UPLOADED_FILE_FAILED = 404;
    CONST ERROR_FROM_SUPERGLOBALE_FILES = 405;

    /**
     *
     * Constructeur
     *
     * @param string $name - attribut name du formulaire
     * @throws \Framework\Exceptions\UploadFileException
     * @throws \InvalidArgument
     */
    public function __construct(Application $app,$name)
    {
    	parent::__construct($app);
    	if(isset($name) && !empty($name) && is_string($name)){
    		$this->_name = $name;
    		$code = $this->getError();
    		$this->_isValid = TRUE;
    		if ($code != 0) {
    			$this->_isValid = FALSE;
    			if($code != 4)
    				throw new \Framework\Exceptions\UploadFileException($code);
    		}
    	} else throw new \InvalidArgumentException('le type de nom de fichier est invalide', self::INVALID_FILENAME);
    }

    /**
     *
     * Méthode getName
     *
     * getter de l'attribut name
     *
     * @return string
     */
    public function name()
    {
    	return $this->_name;
    }

    /**
     *
     * Méthode isValid
     *
     * getter de l'attribut isValid
     *
     * @return boolean
     */
    public function isValid()
    {
    	return $this->_isValid;
    }

    /**
     *
     * Méthode deleteFile
     *
     * détruit le fichier téléchargé dans le répertoire temporaire
     *
     * @param string $filename
     * @throws \Framework\Exceptions\FileException
     */
    public function deleteFile($filename = NULL)
    {
 		if($data = $this->getUploadFileData()){
 			if (empty($filename))
 				$filename = $data['tmp_name'];
 			if(!unlink($filename))
 				throw new \Framework\Exceptions\FileException('Echec lors de la tentative de destruction du fichier téléchargé',self::DELETE_UPLOADED_FILE_FAILED);
 		} else throw new \Framework\Exceptions\FileException('superglobale $_files vide',self::ERROR_FROM_SUPERGLOBALE_FILES);
    }

    /**
     *
     * Méthode getFileExtension
     *
     * extrait l'extension du nom du fichier
     *
     * @return string correspondant à l'extension (commençant par un point)
     * @throws \Framework\Exceptions\FileException
     */
    public function getFileExtension()
    {
    	if($data = $this->getUploadFileData()){
    		$extension = substr_replace($data['type'],'',0,-4);
    		return $extension;
    	} else throw new \Framework\Exceptions\FileException('superglobale $_files vide',self::ERROR_FROM_SUPERGLOBALE_FILES);
    }

    /**
     *
     * Méthode getFilename
     *
     * retourne le nom du fichier
     *
     * @throws \Framework\Exceptions\FileException
     * @return string
     */
    public function getFilename()
    {
    	if($data = $this->getUploadFileData())
    		return $data['name'];
    	else throw new \Framework\Exceptions\FileException('superglobale $_files vide',self::ERROR_FROM_SUPERGLOBALE_FILES);

    }

    /**
     *
     * Méthode getFileSize
     *
     * retourne la taille du fichier
     *
     * @return int taille du fichier
     * @throws \Framework\Exceptions\FileException
     */
    public function getFileSize()
    {
    	if($data = $this->getUploadFileData())
    		return $data['size'];
    	else throw new \Framework\Exceptions\FileException('superglobale $_files vide',self::ERROR_FROM_SUPERGLOBALE_FILES);
    }

    /**
     *
     * Méthode getError
     *
     * retourne le code erreur
     *
     * @return int code erreur prédéfini
     * @throws \Framework\Exceptions\FileException
     */
    public function getError()
    {
    	if($data = $this->getUploadFileData())
    		return $data['error'];
    	else throw new \Framework\Exceptions\FileException('superglobale $_files vide',self::ERROR_FROM_SUPERGLOBALE_FILES);
    }

    /**
     *
     * Méthode getUploadFileData
     *
     * permet d'obtenir sur le fichier téléchargé : name, type, size, tmp_name, error
     *
     * @return array données ou boolean
     * @throws \Framework\Exceptions\FileException
     */
    private function getUploadFileData()
    {
    	if(isset($_FILES) && is_array($_FILES) && count($_FILES)>0)
    		return $_FILES[$this->_name];
    	else return FALSE;
    }

    /**
     *
     * Méthode moveTo
     *
     * @param string $destination
     * @throws \Framework\Exceptions\FileException
     */
    public function moveTo($destination)
    {
    	if($data = $this->getUploadFileData()){
    		if (!move_uploaded_file($data['tmp_name'],$destination))
    			throw new \Framework\Exceptions\FileException('Erreur au déplacement du fichier téléchargé',self::MOVE_UPLOADED_FILE_FAILED);
    	} else throw new \Framework\Exceptions\FileException('superglobale $_files vide',self::ERROR_FROM_SUPERGLOBALE_FILES);
    }
}