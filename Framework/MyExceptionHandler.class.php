<?php
namespace Framework;
require_once './Framework/autoload.php';
/**
 *
 * @author Frédéric Tarreau
 *
 * 6 juil. 2015 - MyExceptionHandler.class.php
 *
 */

class MyExceptionHandler extends ApplicationComponent
{
	/**
	 *
	 * Méthode catchExceptionByDefault
	 *
	 * traite les exceptions non attrapées
	 *
	 * @param Exception $exception
	 */
	public static function catchExceptionByDefault($exception)
	{
		echo' exception n';
		echo "Exception non attrapée : " , $exception->getMessage(), "\n";
	}

	/**
	 *
	 * Méthode errorToException
	 *
	 * change tous les messages d'erreur en errorException
	 *
	 * @param int $severity
	 * @param string $message
	 * @param string $file
	 * @param int $line
	 * @throws \ErrorException
	 */
	public static function errorToException($severity,$message,$file,$line)
	{
		// le code erreur ne doit pas être remonté (cf .ini)
		if(!error_reporting() & $severity) return;
		throw new \ErrorException($message, 0, $severity, $file, $line);
	}

	/**
	 *
	 * Méthode handleFatalError
	 *
	 * permet de prendre en compte les erreurs fatales non prises en compte nativement
	 * par la directive set_error_handler
	 *
	 */
	public static function handleFatalError()
	{
		$lastError=error_get_last();
		if($lastError['type'] === E_ERROR){
			self::errorToException(E_ERROR, $lastError['message'],$lastError['file'], $lastError['line']);
		}
	}

	/**
	 *
	 * Méthode start
	 *
	 * active la redirection d'erreur en exception (y compris erreur fatale)
	 * ou/et le traitement des exceptions hors try/catch
	 *
	 * @param bool $error
	 * @param bool $exception
	 */
	public static function start($error=NULL,$exception=NULL)
	{
		if($exception)
			set_exception_handler(array('\Framework\MyExceptionHandler','catchExceptionByDefault'));
		if($error){
			register_shutdown_function(array('\Framework\MyExceptionHandler','handleFatalError'));
			set_error_handler(array('\Framework\MyExceptionHandler','errorToException'));
		//	ini_set( "display_errors", "off" );
		//	error_reporting( E_ALL );
		}
	}

	/**
	 *
	 * Méthode stop
	 *
	 * désactive la redirection d'erreur en exception ou/et le traitement des exceptions hors try/catch
	 *
	 * @param bool $error
	 * @param bool $exception
	 */
	public static function stop($error=NULL,$exception=NULL)
	{
		if($error)
			restore_error_handler();
		if($exception)
			restore_exception_handler();
	}
}

