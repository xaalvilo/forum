<?php
namespace Framework;
require_once './Framework/autoload.php';
/**
 *
 * @author Frédéric Tarreau
 *
 * 24 juin 2015 - MyCsvFileHandler.class.php
 *
 * Cette classe gère les accès à un fichier CSV
 *
 */
class MyCsvFileHandler
{
    /**
     *
     * Méthode csvOpenFile
     *
     * créé l'objet SplFileObject et ouvre le fichier avec les options prévues par cette classe Php
     *
     * @param \SplFileInfo $fileInterface
     * @param string $rw options d'ouverture de fichier (cf. fopen() de php)
     * @return \SplFileObject $fileObject
     * @throws \OutOfRangeException
     * @throws Framework\Exception\FileException
     */
    public function csvOpenFile(\SplFileInfo $fileInterface,$rw)
    {
        $options = array("ab+","ab","rb+","rb");
        if (is_string($rw) && in_array($rw, $options)){
        	if( ($fileInterface instanceof \SplFileInfo)){
        		$fileObject = $fileInterface->openFile($rw);
        		$fileObject->setFlags(\SplFileObject::READ_CSV | \SplFileObject::SKIP_EMPTY
                                | \SplFileObject::DROP_NEW_LINE | \SplFileObject::READ_AHEAD);
        		return $fileObject;
        	} else throw new Framework\Exception\FileException( 'l\'interface du fichier'.$fileInterface->getFilename().'ou le mode RW est incorrect');
        } else throw new \OutOfRangeException('le paramètre fixant les droits de lecture/ecriture est invalide');
    }

    /**
     *
     * Méthode searchInCSV
     *
     * recherche un couple de chaîne de caractère dans un fichier CSV
     *
     * @param \SplFileObject $fileObject
     * @param string $a, $b
     * @return mixed $line ligne correspondant à la recherche ou FALSE
     * @throws Framework\Exception\FileException si le fichier n'est pas au format CSV
     */
    public function searchInCSV(\SplFileObject $fileObject,$a,$b)
    {
       $line = FALSE;
       if($fileObject->getFlags() & \SplFileObject::READ_CSV){ // & operateur sur les bits
         while(!$fileObject->eof() && !$line){
               list($valueA,$valueB)=$fileObject->fgetcsv();
               if($valueA==$a && $valueB==$b)
                   $line = $fileObject->key();
           }
         return $line;
       } else throw new Framework\Exception\FileException('le fichier'.$fileObject->getFilename().'n\'est pas au format CSV');
    }

    /**
     *
     * Méthode replaceInCSV
     *
     * remplace un couple de chaîne de caractere dans un fichier CSV
     *
     * @param \SplFileObject $fileObject
     * @param string $a, $b nouvelles valeurs
     * @param int $line ligne du fichier
     * @return int longueur de la chaîne
     * @throws \InvalidArgumentException
     */
    public function replaceInCSV(\SplFileObject $fileObject,$a,$b,$line)
    {
        if(isset($line) && is_int($line)){
            $fileObject->seek($line-1);
            $nb = $fileObject->fputcsv(array($a,$b));
            return $nb;
        } else throw new \InvalidArgumentException('mauvais argument de la méthode replaceInCSV');
    }
}