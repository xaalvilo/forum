<?php
/**
 *
 * @author Fr�d�ric Tarreau
 *
 * 7 sept. 2014 - ManagerCommentaire.class.php
 *
 * classe h�rit�e de la classe abstraite Mod�le dont le r�le est la gestion des acc�s aux donn�es Commentaires
 */
namespace Framework\Modeles;
require_once './Framework/autoload.php';

class ManagerCommentaire extends \Framework\Manager
{
    /* constantes de différenciation entre la table des commentaires de billet et la table de commentaires
    * d'article
    */
    const TABLE_COMMENTAIRES_BILLET = 1;
    const TABLE_COMMENTAIRES_ARTICLE = 2;

    /**
    *
    * Méthode getListeCommentaires
    *
    * méthode renvoyant la liste de l'ensemble des commentaires d'un Billet ou d'un article
    *
    * @param int $table identifiant de la table concernée dans la BDD
    * @param int $idParent identifiant du billet ou de l'article
    * @return PDOStatement la liste des commentaires
    * @throws \Framework\Exceptions\BDDException
    */
    public function getListeCommentaires($table,$idParent)
    {
        switch ($table)
        {
            case 1 :
                    $sql ='select C.COM_ID as id, C.COM_DATE as date, C.COM_DATEMODIF as dateModif,'
                    . ' U.USER_PSEUDO as auteur, C.COM_CONTENU as contenu,'
                    .  'U.USER_NBRECOMMENTAIRESFORUM as nbUserComents, U.USER_NBREBILLETSFORUM as nbUserBillets'
                    . ' from T_COMMENTAIRE C'
                    . ' join T_USER U on U.USER_ID = C.USER_ID'
                    . ' where C.BIL_ID=?'
                    . ' order by C.COM_DATE asc';
                    break;
            case 2 :
                    $sql = 'select C.COM_ART_ID as id, C.COM_ART_DATE as date, C.COM_ART_DATEMODIF as dateModif,'
                    . ' U.USER_PSEUDO as auteur, COM_ART_CONTENU as contenu'
                    . ' from T_COMMENTAIRE_ARTICLE C'
                    . ' join T_USER U on U.USER_ID = C.USER_ID'
                    . ' where C.ART_ID=?'
                    . ' order by C.COM_ART_DATE asc';
                    break;
            default:
                throw new \Framework\Exceptions\BDDException("mauvaise selection de la table de BDD");
        }

        //instanciation d'objets "Modele\Commentaire" dont les attributs publics prennent pour valeur les donn�es de la BDD
        $requete = $this->executerRequete($sql,array($idParent),'\Framework\Entites\Commentaire');
        $commentaires = $requete->fetchAll();
        $requete->closeCursor();

        // spécification de la langue utilisée pour l'affichage de la date et heure
        // grâce au trait Affichage utilisé par la classe mère
    	$this->setHeureDateLocale();

        foreach ($commentaires as $commentaire)
        {
            // il faut transformer l'attribut Date en objet DateTime
            $commentaire->setDate(new \DateTime($commentaire->date()));
            $commentaire->setDateModif(new \DateTime($commentaire->dateModif()));
        }
        return $commentaires;
    }

    /**
     *
     * Méthode getCommentaire
     *
     * méthode renvoyant la liste de l'ensemble des commentaires d'un Billet ou d'un article
     *
     * @param int $table identifiant de la table concernée dans la BDD
     * @param int $id identifiant du commentaire de billet ou d'article
     * @return PDOStatement le commentaire sous forme d'objet
     * @throws \Framework\Exceptions\BDDException en cas d'erreur
     */
    public function getCommentaire($table,$id)
    {
    	switch ($table)
    	{
    		case 1 :
    		$sql = 'select C.BIL_ID as idParent, U.USER_PSEUDO as auteur, C.COM_CONTENU as contenu,'
    		     . ' C.COM_DATE as date, C.COM_DATEMODIF as dateModif, C.COM_ID as id from T_COMMENTAIRE C'
    		     . ' join T_USER U on U.USER_ID=C.USER_ID'
    		     . ' where C.COM_ID=?';
                    break;
            case 2 :
    		$sql = 'select C.ART_ID as idParent, U.USER_PSEUDO as auteur, C.COM_ART_CONTENU as contenu, C.COM_ART_DATE as date, C.COM_ART_DATEMODIF as dateModif '
    		     . ' from T_COMMENTAIRE_ARTICLE C'
    		     . ' join T_USER U on U.USER_ID=C.USER_ID'
    		     . ' where C.COM_ART_ID=?';
                    break;
            default:
                throw new \Framework\Exceptions\BDDException("mauvaise selection de la table de BDD");
    	}

    	//instanciation d'objets "Modele\Commentaire" dont les attributs publics et protégés prennent pour valeur les donn�es de la BDD
    	$resultat = $this->executerRequete($sql,array($id),'\Framework\Entites\Commentaire');

    	// TODO plutot fetchcolumn() car rowcount non supporté systématiquement pour requete select
    	if ($resultat->rowcount()==1){
    		$commentaire = $resultat->fetch();
    		$resultat->closeCursor();

    		// spécification de la langue utilisée pour l'affichage de la date et heure
    		// cela permet d'utliser la fonction strftime() au moment d'afficher l'heure
    		$this->setHeureDateLocale();

    		// il faut transformer l'attribut Date et DateModif en objet DateTime
    		$commentaire->setDate(new \DateTime($commentaire->date()));
    		$commentaire->setDateModif(new \DateTime($commentaire->dateModif()));
    		return $commentaire;
    	}
    	else throw new \Framework\Exceptions\BDDException("Aucun commentaire ne correspond à l'identifiant '$id'");
    }

    /**
     *
     * Méthode getNewCommentaires
     *
     * permet de récupérer tous les nouveaux commentaires postérieurs à une date de dernière connexion d'un utilisateur
     *
     * @param string $date
     * @return \PDOStatement liste des commentaires
     */
    public function getNewCommentaires($date)
    {
       $sql = 'select C.BIL_ID as idParent, U.USER_PSEUDO as auteur, C.COM_CONTENU as contenu,'
    		   . ' C.COM_DATE as date, C.COM_DATEMODIF as dateModif, C.COM_ID as id,'
    		   . ' B.BIL_TITRE as titre, B.TOPIC_ID as idTopic, T.TOPIC_TITRE as titreTopic from T_COMMENTAIRE C'
    		   . ' join T_USER U on U.USER_ID=C.USER_ID'
    		   . ' join T_BILLET B on B.BIL_ID=C.BIL_ID'
    		   . ' join T_FORUM_TOPIC T on T.TOPIC_ID = B.TOPIC_ID'
    		   . ' where C.COM_DATE > ?';

       //instanciation d'objets "Modele\Commentaire" dont les attributs publics prennent pour valeur les donn�es de la BDD
       $resultat = $this->executerRequete($sql,array($date),'\Framework\Entites\Commentaire');
       $commentaires = $resultat->fetchAll();
       $resultat->closeCursor();

       // spécification de la langue utilisée pour l'affichage de la date et heure
       // grâce au trait Affichage utilisé par la classe mère
       $this->setHeureDateLocale();

       foreach ($commentaires as $commentaire){
           // il faut transformer l'attribut Date en objet DateTime
           $commentaire->setDate(new \DateTime($commentaire->date()));
           $commentaire->setDateModif(new \DateTime($commentaire->dateModif()));
       }
       return $commentaires;
    }

    /**
    *
    * Méthode ajouterCommentaire
    *
    * Méthode permettant d'ajouter un commentaire
    *
    * @param int $table identifiant de la table concernée dans la BDD
    * @param int $idAuteur identifiant de l'auteur dans la BDD User si Forum ou nom de l'auteur si Blog
    * @param int $idParent identifiant du Billet
    * @param string $contenu texte du commentaire
    * @throws \Framework\Exceptions\BDDException
    */
    public function ajouterCommentaire($table,$idParent, $idAuteur, $contenu)
    {
        switch ($table)
        {
            case 1 :
                    $sql = 'insert into T_COMMENTAIRE(BIL_ID, COM_DATE, USER_ID, COM_CONTENU)'
                         . ' values(?, ?, ?, ?)';
                         break;
            case 2 :
                    $sql = 'insert into T_COMMENTAIRE_ARTICLE(ART_ID, COM_ART_DATE, USER_ID, COM_ART_CONTENU)'
                         . ' values(?, ?, ?, ?)';
                         echo "ok";
                         break;
            default:
                throw new \Framework\Exceptions\BDDException ("mauvaise selection de la table de BDD");
        }

        // utilisation de la classe DateTime pour faire correspondre le format Php avec le format DateTime de MySql, time courant de la machine
        $odate = new \DateTime();

        // il faut formater la date en cha�ne de caract�re
        $date = $odate->format('Y-m-d H:i:s');

        $requeteSQL = $this->executerRequete($sql,array($idParent,$date,$idAuteur,$contenu),'\Framework\Entites\Commentaire');
    }

    /**
     * Méthode supprimerCommentaire
     *
     * cette méthode permet de supprimer un commentaire
     *
     * @param int $table identifiant de la table concernée en BDD
     * @param int $idCommentaire
     * @throws \Framework\Exceptions\BDDException
     */
    public function supprimerCommentaire($table, $idCommentaire)
    {
    	switch ($table)
    	{
    		case 1 :
    			$sql = 'delete from T_COMMENTAIRE where COM_ID = ?';
    			break;
    		case 2 :
    			$sql = 'delete from T_COMMENTAIRE_ARTICLE where COM_ART_ID = ?';
    			break;
    		default:
    			throw new \Framework\Exceptions\BDDException("mauvaise selection de la table de BDD");
    	}

        $requeteSQL = $this->executerRequete($sql, array($idCommentaire),'\Framework\Entites\Commentaire');

        return ($requeteSQL->rowcount()==1);
    }

    /**
     *
     * Méthode actualiserCommentaire
     *
     * cette méthode permet d'actualiser le contenu et la date d'un commentaire
     *
     * @param int $table
     * @param int $idCommentaire
     * @param array $donnees à actualiser
     */
    public function actualiserCommentaire ($table, $idCommentaire, $donnees)
    {
        if(array_key_exists('date', $donnees))
            $donnees['date']= $donnees['date']->format('Y-m-d H:i:s');

        if(array_key_exists('dateModif', $donnees))
            $donnees['dateModif']= $donnees['dateModif']->format('Y-m-d H:i:s');

        // création de la chaîne de caractère pour la requête SQL
        unset ($donnees['id']);  // retrait préalable l'identifiant qui n'est jamais actualisé
        $nbreModifications=count($donnees);
        $modification ='';
        $i=0;

    	switch ($table){
    		case 1 :
    		    foreach ($donnees as $attribut=>$valeur)
    		    {
    		        $i++;
    		        $modification.='COM_'.strtoupper($attribut).'=?';
    		        if ($i<$nbreModifications)
    		            $modification.=', ';
    		    }
    			$sql = 'update T_COMMENTAIRE set '.$modification.' where COM_ID=?';
    					break;
    		case 2 :
    		    foreach ($donnees as $attribut=>$valeur)
    		    {
    		        $i++;
    		        $modification.='COM_ART_'.strtoupper($attribut).'=?';
    		        if ($i<$nbreModifications)
    		            $modification.=', ';
    		    }
    			$sql = 'update T_COMMENTAIRE_ARTICLE set '.$modification.' where COM_ART_ID=?';
    					break;
    		default:
    			throw new \Framework\Exceptions\BDDException("mauvaise selection de la table de BDD");
    	}
        $donnees['id']= $idCommentaire; // ajout de l'identifiant pour l'exécution de la requête
    	$donnees = array_values($donnees); // transformation du tableau en tableau indexé
    	$resultat = $this->executerRequete($sql,$donnees,'\Framework\Entites\Commentaire');

    	if (is_array($resultat))
    		//TODO msg Flash non OK
    		throw new \Framework\Exceptions\BDDException($resultat[2]);
    }


    /**
     * Méthode getIdParent
     *
     * cette méthode permet de récupérer l'id du billet ou de l'article auquel le commentaire en paramètre est associé
     *
     * @param int $table
     * @param int $idCommentaire
     * @throws \Framework\Exceptions\BDDException en cas d'échec au choix de la table
     * @return array $tableauRésultat, à une entrée comportant l'id du Parent
     */
    public function getIdParent($table, $idCommentaire)
    {
    	switch ($table)
    	{
    		case 1 :
    			$sql = 'select BIL_ID as idParent, USER_ID as idUser from T_COMMENTAIRE where COM_ID = ?';
    			break;
    		case 2 :
    			$sql = 'select ART_ID as idParent, USER_ID as idUser from T_COMMENTAIRE_ARTICLE where COM_ART_ID = ?';
    			break;
    		default:
    			throw new \Exception ("mauvaise selection de la table");
    	}

    	$requeteSQL = $this->executerRequete($sql, array($idCommentaire),'\Framework\Entites\Commentaire');

    	if ($requeteSQL->rowcount()==1){
    		// modification du type de récupération des données de la BDD, ici sous forme de tableau
    		$tableauResultat = $requeteSQL->fetch(\PDO::FETCH_ASSOC);
    		$requeteSQL->closeCursor();
    		return $tableauResultat;
    	}
    	else throw new \Exception("Impossible d'obtenir l'identifiant du billet");
    }

    /**
     *
     * Méthode getNbComents
     *
     * Permet de connaître le nbre de commentaires d'un billet ou article
     * TODO sera utile pour le backend
     * @param int $table, table de BDD concernée
     * @param int $idParent, id du parent (billet ou article)
     * @throws \Framework\Exceptions\BDDException en cas d'erreur de sélection de la table de BDD concernée
     */
    public function getNbComents($table,$idParent)
    {
        switch ($table)
        {
            case 1 :
                $sql = 'select count(*) from T_COMMENTAIRE where BIL_ID=?';
                break;
            case 2 :
                $sql = 'select count(*) from T_COMMENTAIRE_ARTICLE where ART_ID=?';
                break;
            default:
                    throw new \Framework\Exceptions\BDDException("mauvaise selection de la table de BDD");
        }
        // instanciation d'objets "Modele\Commentaire" dont les attributs prennent pour valeur les donn�es de la BDD
        $requeteSQL = $this->executerRequete($sql,array($idParent),'\Framework\Entites\Commentaire');

        $count = $requeteSQL->fetchcolumn();
        $requeteSQL->closeCursor();
        return $count;
    }

    /**
     *
     * Méthode anonymiserCommentaires
     *
     * permet d'attribuer l'identifiant du membre virtuel "anonyme" à tous les commentaires d'un utilisateur
     *
     * @param int $idAnonyme
     * @param int $idUser
     * @throws \Framework\Exceptions\BDDException
     */
    public function anonymiserCommentaires($idAnonyme,$idUser)
    {
        $ids = array($idAnonyme,$idUser);
        $sql1 = 'update T_COMMENTAIRE set USER_ID= ? where USER_ID= ?';
        $sql2 = 'update T_COMMENTAIRE_ARTICLE set USER_ID= ? where USER_ID= ?';
        if (!$this->executerRequete($sql1,$ids,'\Framework\Entites\Commentaire'))
            throw new \Framework\Exceptions\BDDException("Echec anonymisation des commentaires de l'utilisateur '$idUser'");
        elseif (!$this->executerRequete($sql2,$ids,'\Framework\Entites\Commentaire'))
            throw new \Framework\Exceptions\BDDException("Echec anonymisation des commentaires de l'utilisateur '$idUser'");
    }
}

