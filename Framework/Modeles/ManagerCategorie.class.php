<?php
/**
 *
 * @author Frédéric Tarreau
 *
 * 16 juin 2015 - ManagerCategorie.class.php
 *
 * classe héritée de la classe abstraite Modèle dont le rôle est la gestion des accès à la base de données des categories du Forum
 *
 */
namespace Framework\Modeles ;
require_once './Framework/autoload.php';
class ManagerCategorie extends \Framework\Manager
{
    /**
     *
     * Méthode getAllCategories
     *
     * méthode renvoyant la liste de l'ensemble de toutes les catégories
     *
     * @return PDOStatement la liste des categories et leurs caractéristiques
     */
    public function getAllCategories()
    {
    	$sql = 'select CAT_NAME as _name, CAT_ID as id, CAT_ORDRE as _ordre from T_FORUM_CAT';

    	$requete = $this->executerRequete($sql,NULL,'\Framework\Entites\Categorie');
    	$topics = $requete->fetchAll();
    	$requete->closeCursor();
    	return $categories;
    }

    /**
    *
    * Méthode getCategorie
    *
    * méthode renvoyant l'ensemble des informations sur une catégorie
    *
    * @param int $idCat identifiant de la categorie
    * @return array la catégorie sélectionnée
    * @throws \Framework\Exceptions\BDDException si l'identifiant de la categorie est inconnu
    */
    public function getCategorie($idCat)
    {
        $sql = 'select CAT_ID as id, CAT_NAME as _name, CAT_ORDRE as _ordre from T_FORUM_CAT where CAT_ID=?';

        $resultat =$this->executerRequete($sql,array($idCat),'\Framework\Entites\Categorie');

        if ($resultat->rowcount()==1){
            $categorie = $resultat->fetch();
            $resultat->closeCursor();
        	return $categorie;
        }
        else throw new \Framework\Exceptions\BDDException("Aucune catégorie ne correspond à l'identifiant '$idCat'");
    }

    /**
     *
     * Méthode actualiserCategorie
     *
     * Permet d'actualiser le nom et/ou l'ordre d'une Categorie
     *
     * @param int $idCat
     * @param array $donnees à actualiser
     * @throws \Framework\Exceptions\BDDException si l'identifiant de la catégorie est inconnu
     */
    public function actualiserCategorie($idCat,$donnees)
    {
        // création de la chaîne de caractère pour la requête SQL
        $nbreModifications=count($donnees);
        $modification ='';
        $i=0;

        foreach ($donnees as $attribut=>$valeur){
            $i++;
            $modification.='CAT_'.strtoupper($attribut).'=?';
            if ($i<$nbreModifications)
                $modification.=', ';
        }

        $sql = 'update T_FORUM_CAT set '.$modification.' where CAT_ID=?';

        $donnees['id']= $idCat;// ajout de l'identifiant pour l'exécution de la requête
        $donnees = array_values($donnees);// transformation du tableau en tableau indexé

        $resultat = $this->executerRequete($sql,$donnees,NULL);

        if (is_array($resultat))
            //TODO msg Flash non OK
            throw new \Framework\Exceptions\BDDException($resultat[2]);
    }

    /**
     *
     * Méthode ajouterCategorie
     *
     * permet d'ajouter une catégorie au Forum
     *
     * @param int $idCat
     * @param string $name
     * @param int $ordre
     * @return int identifiant retourné par requête d'insertion
     */
    public function ajouterCategorie($name,$ordre=0)
    {
    	$sql = 'insert into T_FORUM_CAT(CAT_NAME, CAT_ORDRE) values(?, ?)';
    	$resultat = $this->executerRequete($sql,array($name,$ordre),'\Framework\Entites\Categorie');
    	return $resultat;
    }

    /**
     * Méthode supprimerCategorie
     *
     * permet de supprimer une catégorie de la BDD
     *
     * @param int $idCat
     * @return bool
     */
    public function supprimerCategorie($idCat)
    {
    	$sql = 'delete from T_FORUM_CAT where CAT_ID = ?';
    	$requeteSQL = $this->executerRequete($sql, array($idCat),'\Framework\Entites\Categorie');
    	return ($requeteSQL->rowcount()==1);
    }
}


