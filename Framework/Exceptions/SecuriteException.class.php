<?php
namespace Framework\Exceptions;
require_once './Framework/autoload.php';
/**
 *
 * @author Frédéric Tarreau
 *
 * 7 juil. 2015 - SecuriteException.class.php
 *
 * classe héritée de MyException - Exception levée lorsque qu'un comportement anormal décelé peut laisser supposer
 * une action malveillante (ex:sollicitation d'une action d'un controleur, accessible via un bouton de l'IHM qui ne s'affiche
 * que pour les utilisateurs connectés)
 *
 */

class SecuriteException extends \Framework\MyException
{
	public function __construct($message,$code=0, Exception $previous = null)
	{
		parent::__construct($message,$code,$previous);
		$this->message = __CLASS__ ." - ".$message;
	}

	public function addLog()
	{
		//TODO log dans un fichier spécifique sécurité
	}

	public function mailToAdmin()
	{
		//TODO mail pour l'admin
	}
}

