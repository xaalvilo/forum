<?php
namespace Framework\Exceptions;
require_once './Framework/autoload.php';
/**
 *
 * @author Frédéric Tarreau
 *
 * 30 juil. 2015 - UploadFileException.class.php
 *
 * Classe héritée de FileException pour les exceptions générées par des erreurs de téléchargement de fichier
 *
 */

class UploadFileException extends FileException
{
	public function __construct($code)
	{
		$message = $this->codeToMessage($code);
		parent::__construct($message, $code);
	}

	/**
	 *
	 * Méthode codeToMessage
	 *
	 * permet de transcrire le code erreur de téléchargement en message envoyé par l'exception
	 *
	 * @param string $code
	 * @return string $message
	 */
	private function codeToMessage($code)
	{
		switch ($code) {
			case UPLOAD_ERR_INI_SIZE:
				$message = "le fichier téléchargé excède la taille maximale autorisée dans le php.ini";
				break;

			case UPLOAD_ERR_FORM_SIZE:
				$message = "le fichier téléchargé excède la taille maximale autorisée par MAX_FILE_SIZE dans le formulaire HTML";
				break;

			case UPLOAD_ERR_PARTIAL:
				$message = "Le fichier a été partiellement téléchargé";
                break;

            case UPLOAD_ERR_NO_FILE:
                $message = "Aucun fichier téléchargé";
                break;

            case UPLOAD_ERR_NO_TMP_DIR:
                $message = "Il manque un répertoire temporaire de téléchargement";
                break;

            case UPLOAD_ERR_CANT_WRITE:
                $message = "Impossible d'écrire le fichier téléchargé sur le disque";
                break;

            case UPLOAD_ERR_EXTENSION:
                $message = "L'extension du fichier a téléchargé n'est pas compatible";
                break;

            default:
                $message = "Erreur de téléchargement inconnue";
                break;
        }
        return $message;
    }
}

