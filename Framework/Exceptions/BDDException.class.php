<?php
namespace Framework\Exceptions;
require_once './Framework/autoload.php';
/**
 *
 * @author Frédéric Tarreau
 *
 * 17 juil. 2015 - BDDException.class.php
 *
 */

class BDDException extends \Framework\MyException
{
	public function __construct($message,$code=0, Exception $previous = null)
	{
		parent::__construct($message,$code,$previous);
		$this->message = __CLASS__ ." - ".$message;
	}
}

