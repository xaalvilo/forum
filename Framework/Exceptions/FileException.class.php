<?php
namespace Framework\Exceptions;
require_once './Framework/autoload.php';
/**
 *
 * @author Frédéric Tarreau
 *
 * 7 juil. 2015 - FileException.class.php
 *
 */

class FileException extends \Framework\MyException
{
	public function __construct($message,$code=0, Exception $previous = null)
	{
		parent::__construct($message,$code,$previous);
		$this->message = __CLASS__ ." - ".$message;
	}
}

