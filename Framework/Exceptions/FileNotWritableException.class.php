<?php
namespace Framework\Exceptions;
require_once './Framework/autoload.php';
/**
 *
 * @author Frédéric Tarreau
 *
 * 7 juil. 2015 - FileNotWritableException.class.php
 *
 */

class FileNotWritableException extends FileException
{

}

