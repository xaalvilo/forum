<?php
namespace Framework\Exceptions;
require_once './Framework/autoload.php';
/**
 *
 * @author Frédéric Tarreau
 *
 * 8 juil. 2015 - MyPDOException.class.php
 *
 * classe héritée de PDOException permettant d'améliorer l'affichage du message d'erreur
 *
 */

class MyPDOException extends \PDOException
{
	public function __construct(\PDOException $e)
	{
		if(strstr($e->getMessage(), 'SQLSTATE[')) {
            preg_match('/SQLSTATE\[(\w+)\] \[(\w+)\] (.*)/', $e->getMessage(), $matches);
            $this->code = ($matches[1] == 'HT000' ? $matches[2] : $matches[1]);
            $this->message = $matches[3];
        }
	}

	public function addLog()
	{
		//TODO log dans un fichier spécifique sécurité
	}
}

