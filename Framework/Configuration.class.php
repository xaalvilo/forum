<?php
namespace Framework;
/**
 * @author Fr�d�ric Tarreau
 *
 * 11 sept. 2014 - Configuration.class.php
 *
 * Classe fille de la classe ApplicationComponent dont le r�le est la gestion de la configuration du site, permettant d'externaliser la configuration du site en dehors du code source
 * elle charge le fichier de configuration dev.ini ou prod.ini
 *
 */

class Configuration extends ApplicationComponent
{
    /**
    * cette classe encapsule un tableau associatif cl�s/valeurs (attribut $parametres) stockant les valeurs des paramètres de configuration. ce tableau est statique (un seul exemplaire par classe)
    * ce qui permet de l'utiliser sans instancier d'objet Configuration
    */
    private static $parametres;

    /**
    * M�thode statique publique get
    *
    * Cette méthode permet de rechercher la valeur d'un param�tre � partir de son nom ou l'ensemble des valeurs d'une section
    * du fichier de configuration. si le param�tre en question est trouvé dans le tableau associatif, la m�thode renvoie la valeur du parametre de configuration
    * sinon une valeur par d�faut est renvoy�e
    *
    * @param string $nom Nom du param�tre
    * @param string $valeurParDefaut Valeur à renvoyer par d�faut
    * @param string $nomSection
    * @return mixed Valeur du param�tre ou tableau de valeur des paramètres d'une section
    */
    public static function get($nom = null, $valeurParDefaut = null, $nomSection = null)
    {
    	$sections = self::getParametres();
    	if($nomSection && isset($sections[$nomSection])){
    		$valeur = $sections[$nomSection];
    	} else {
    		foreach($sections as $section){
    			if(array_key_exists($nom,$section))
    				$valeur = $section[$nom];
    		}
    	}
    	if(!isset($valeur))
    		$valeur = $valeurParDefaut;
        return $valeur;
    }


    /**
    * M�thode statique priv�e getParametres()
    *
    * Cette méthode effectue le chargement tardif du fichier contenant les paramètres de configuration. Afin de faire figurer sur
    * un m�me serveur une configuration de d�veloppement  et une configuration de production, 2 fichiers sont recherchés dans le répertoire Config du site : dev.ini et prod.ini.
    *
    * @return array $_parametres tableau associatif des parametres de configuration
    * @throws Exception Si aucun fichier de configuration n'est trouvé
    */
    private static function getParametres()
    {
        //si le fichier n'a pas d�jà �t� charg�
        if (self::$parametres == null) {
            //prendre le fichier de configuration de développement
            $cheminFichier = "Applications/Frontend/Config/dev.ini";

            // s'il n'existe pas , prendre le fichier de configuration de production
            if (!file_exists($cheminFichier))
                $cheminFichier = "Config/prod.ini ";

            // si ce dernier n'existe pas non plus, envoyer une erreur
            if (!file_exists($cheminFichier))
                throw new \Framework\Exceptions\FileNotFoundException ("Aucun fichier de configuration $fichierControleur trouvé");
            else
                self::$parametres = parse_ini_file($cheminFichier,true);
        }
        // si le fichier de configuration a d�j� �t� charg�, le renvoyer
        return self::$parametres;
    }
}


