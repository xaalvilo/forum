<?php
namespace Framework;
require_once './Framework/autoload.php';
/**
 *
 * @author Frédéric Tarreau
 *
 * 12 mai 2015 - SecuriteHandler.class.php
 *
 * Cette classe gère la securite du site et prévient de :
 * - utilisateur sur liste noire
 * - vol de session
 * - anti flood
 * - force brute
 *
 */
class SecuriteHandler extends ApplicationComponent implements \SplObserver
{
	const NOT_CONNECTED = 50;
	const NOT_AUTHORIZED = 51;
	const BLACKLIST_CREATION_FAILED = 52;
	const ALREADY_CONNECTED = 53;
	//TODO
	const MAX = 99;

    /* @var int intervalle antiflood */
    protected $_interval;

    /* @var objet BlackListHandler */
    protected $_blackListHandler;

    public function __construct(\Framework\Application $app)
    {
        parent::__construct($app);
        $this->_interval = \Framework\Configuration::get('intervalFlood',15);
        $this->_blackListHandler = new \Framework\BlackListHandler();
    }

    /**
     *
     * Méthode update
     *
     * actualise l'état de l'objet en fonction des notifications envoyées par l'objet observer, par exemple le controleur (cf. design pattern observer)
     *
     * @param \SPLSubject $subject
     * @param array $donnees parametres optionnels
     */
    public function update(\SPLSubject $subject,$donnees = array())
    {
    	$moduleName = str_replace("Controleur","",substr(strrchr(get_class($subject),"\\"), 1));
    	$donnees['moduleName']= $moduleName;
    	$this->block($donnees);
    	if(array_key_exists('code',$donnees))
    		$this->alert($donnees['code']);
    	else $this->alert();
    }

    /**
     *
     * Méthode balckListHandler
     *
     * getter de l'attribut blackListHandler
     *
     *@return \Framework\BlackListHandler
     */
    public function blackListHandler()
    {
    	return $this->_blackListHandler;
    }

    /**
     *
     * Méthode blocageInscription
     *
     * bloque les tentatives d'inscription en redirigeant vers la page d'accueil , avec avis de l'administrateur
     * - pour un utilisateur banni, créant un nouveau compte, mais avec une adresse Ip
     * et une version de Browser identique
     * - pour un utilisateur effectuant 5 tentatives successives en moins de XX secondes (force brute)
     *
     */
    public function blocageInscription($result)
    {
        //TODO alerte sécurité à l'admin
        $this->_app->routeur()->routerInterne($this->_app,array('controleur'=>'Accueil','action'=>'','id'=>''));
    }

    /**
     *
     * Méthode antiflood
     *
     * prévient le spam volontaire ou accidentel par envoi de commentaire ou de billet dans un délai
     * très court.  vérifie que l'intervalle entre envois successifs respecte la règle d'antiflood
     *
     * @param string $entite
     * @param int $idUser
     * @param string $table spécifique de la BDD (ex:plusieurs tables pour une même entité)
     * @return boolean TRUE si aucun flood detecté
     */
    public function antiflood($entite,$idUser,$table=NULL)
    {
    	$controleur = $this->_app->routeur()->controleur();
    	$managerName= 'manager' .ucfirst($entite);
    	$table = (empty($table)) ?  $entite : $table;
    	return 	($controleur->$managerName()->verifIntervalRequete($table, $idUser,$this->_interval) == 0);
    }

    /**
     *
     * Méthode verifConnexion
     *
     * vérifie si un utilisateur est connecté. La fonction appelée vérifie que le couple IP / version du navigateur de l'utilisateur
     * est le même que celui figurant en session. Permet de lutter contre le vol de session
     *
     * @return boolean TRUE si connecté
     */
    public function verifConnexion()
    {
        if ($this->_app->userHandler()->IsUserAuthenticated())
            return TRUE;
        else {
            // TODO SECU informé l'administrateur
            return FALSE;
        }
    }

    /**
     *
     * Méthode block
     *
     * suite à une action non autorisée , averti l'utilisateur et redirige sur le formulaire de
     * connexion, la page d'accueil, ou la page en court sans execution de l'action demandée
     *
     * @param array $donnees tableau associatif
     */
    public function block($donnees = array())
    {
    	//TODO à enrichir
    	if(!empty($donnees)){
    		if($key = array_search('antiflood',$donnees,true) && array_key_exists('moduleName', $donnees)){
    				$this->_app->userHandler()->setFlash("Edition de message trop rapprochée");
    				$this->_app->routeur()->routerInterne($this->_app,array('controleur'=>$donnees['moduleName'],'action'=>'','id'=>$donnees['id']));
    		}
    		elseif ($key = array_search('banni',$donnees,true)){
    			$this->_app->routeur()->routerInterne($this->_app,array('controleur'=>'Accueil','action'=>'','id'=>''));
    			echo "blocage utilisateur banni";
    		}
    	} else {
    		$this->_app->userHandler()->setFlash("Vous n'êtes pas autorisé à réaliser cette action");
    		$this->_app->routeur()->routerInterne($this->_app,array('controleur'=>'Accueil','action'=>'','id'=>''));
    	}
    }

    /**
     *
     * Méthode alert
     *
     * alerte l'administrateur suite à un fonctionnement anormal ou une action non autorisée et envoi
     * une exception qui mettra fin au script suivant le $code envoyé
     *
     * @param int $code
     */
    public function alert($code=0)
    {
    	//TODO
    	// mailhandler à créer
    	echo "alerte de l'administrateur";
    	if($code>=self::NOT_AUTHORIZED && $code<self::MAX)
    		throw new \Framework\Exceptions\SecuriteException('Alerte de sécurité - fonctionnement anormal',$code);
    }

    /**
     *
     * Méthode blacklist
     *
     * inscription sur liste noire d'un utilisateur
     *
     * @param array $signature
     * @throws \OutOfBoundsException
     */
    public function blacklist($signature)
    {
    	if(array_key_exists('ip', $signature) && array_key_exists('browser',$signature)){
    		$this->_blackListHandler->addToBlackList($signature['ip'], $signature['browser']);
    	}
    	else throw new \OutOfBoundsException("il n'y a pas de clé Ip ou Browser dans l'argument de blacklist" );
    }

    /**
     *
     * Méthode unBlacklist
     *
     * retrait d'un utilisateur de la liste noire
     *
     * @param array $signature
     * @throws \OutOfBoundsException
     */
    public function unBlacklist($signature)
    {
    	if(array_key_exists('ip', $signature) && array_key_exists('browser',$signature)){
    		$this->_blackListHandler->removeFromBlackList($signature['ip'], $signature['browser']);
    	}
    	else throw new \OutOfBoundsException("il n'y a pas de clé Ip ou Browser dans l'argument de unBlacklist" );
    }

    /**
     *
     * Méthode verifPermission
     *
     * vérifie si un membre est autorisé ou non à faire telle ou telle action
     *
     * @param string $niveau du membre
     * @return boolean TRUE si permission accordée
     */
    public function verifPermission($niveau)
    {
        if(is_string($niveau)) {
            $statutSeuil = \Framework\Configuration::get($niveau);
            $statut = $this->_app->userHandler()->user()->statut();
            if ($statut>=$statutSeuil)
                return TRUE;
            else {
                // TODO alerte sécu, viol de permission
                return FALSE;
            }
        } else {
            // TODO erreur fonctionnement anormal
            return FALSE;
        }
    }

    /**
     *
     * Méthode setCapcha
     *
     * return_type
     *
     */
    public function setCapcha()
    {

    }
}