<?php
namespace Framework;
require_once './Framework/autoload.php';
/**
 *
 * @author Frédéric Tarreau
 *
 * 12 0 05 2015 - MailHandler.class.php
 *
 * Classe héritée de ApplicationComponent
 * la classe MailHandler est un gestionnaire de mail personnalisé
 */

class MailHandler extends ApplicationComponent
{
    /* duree de vie */
    private $_lifeTime;

    /* modele d'accès à la BDD des sessions */
    private $_managerSession;

     /**
     * Méthode myMail
     *
     * Utilise la fonction mail() de PHP et envoie le contenu en HTML
     *
     * @param string $userMail
     * @param string $objet
     * @param string $contenu
     * @param array  $lien
     * @return boolean
     */
    public function myMail($userMail,$objet,$contenu,$lien=array())
    {
        //WARNING la config se fait au niveau du serveur SMTP (cf tuto redmine)
        $visibleMail = \Framework\Configuration::get('visibleMail','AtelierBlanc');
        $headers = 'MIME-Version: 1.0'                           ."\r\n";
        $headers .= 'Content-type: text/html; charset=utf-8'      ."\r\n";
        $headers .= 'From:'.$visibleMail. "\r\n";
        $headers .= 'Reply-To:'.$visibleMail. "\r\n";
        $headers .= 'X-Mailer: PHP/'. phpversion();
        $contenu = '<html><head></head><body><p>'.$contenu ;
        if (!empty($lien) && array_key_exists('nom', $lien) && array_key_exists('url', $lien))
            $contenu .='<a href="'.$lien['url'].'">'.$lien['nom'].'</a>';
        $contenu .='</p></body></html>';

        return mail($userMail,$objet,$contenu,$headers);
    }
}