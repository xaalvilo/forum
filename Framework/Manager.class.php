<?php
namespace Framework ;
require_once './Framework/autoload.php';
/**
 *
 * @author Frédéric Tarreau
 *
 * 6 mai 2015 - manager.class.php
 *
 *  Classe abstraite fournissant les services d'acc�s � une base de donn�es via des requetes SQL
 *  Elle utilise l'API PDO de PHP
 *
 */
abstract class Manager
{
	use Affichage;

    /**
    * objet PDO d'acc�s � la BDD
    * attribut de classe statique donc partag�e par toutes les instances des classes d�riv�es de Modele
    * ainsi l'op�ration de connexion � la BDD ne sera r�alis�e qu'une seule fois
    */
    protected static $_bdd ;

    /**
    * M�thode ex�cutant une requ�te SQL �ventuellement param�tr�e (valeur par d�faut nulle). la préparation des requ�tes permet
    * de se prémunir des injections SQL
    * le gestionnaire d'erreur PDO avec exception est retenu sauf pour les requêtes d'insertion permettant d'analyser le code erreur et d'informer l'utilisateur
    *
    * @param string $sql requete sql
    * @param array $params paramètres de la requete sql
    * @param \Framework\Entite $entite
    * @return PDOstatement r�sultat de la requete sql
    * @return int identifiant
    * @return array errorInfo 	avec Code erreur SQLSTATE (un identifiant de cinq caractères alphanumériques défini dans le standard ANSI SQL),
    * 								 Code erreur spécifique au driver.
    * 								 Message d'erreur spécifique au driver.
    * @throws \Framework\Exceptions\MyPDOException
    */
    protected function executerRequete($sql,$params=null,$entite=null)
    {
    	try {
    		if($params===null){
    			//execution directe de la requete s'il n'y a pas de paramètre
    			$resultat = self::getBdd()->query($sql);

    			// si erreur a l'execution, envoyer le code erreur sql ..ne marchera pas si error mode avec exceptions
    			if(!$resultat)
    				return $resultat->errorInfo();
    		} else {
    			//execution de la requete pr�par�e
    			$resultat = self::getBdd()->prepare($sql);

    			//si erreur a l'execution d'une insertion, envoyer le code erreur sql -utile pour vérification d'unicité d'un login par exemple
    			if (substr_count($sql,'insert') > 0 || substr_count($sql,'update')>0)
    				self::getBdd()->setAttribute(\PDO::ATTR_ERRMODE,\PDO::ERRMODE_SILENT);

    			if(!$resultat->execute($params))
    				return $resultat->errorInfo();

    			// s'il s'agit d'une requete d'insertion, renvoyer l'identifiant
    			if(substr_count($sql,'insert') > 0)
    				return self::getBdd()->lastInsertId();
    		}

    		//d�finit le mode de r�cup�ration par d�faut des donn�es en BDD : ici sous forme d'objets de la classe $entité ou tableau
    		if(!empty($entite))
    			$resultat->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE,$entite);
    		else $resultat->setFetchMode(\PDO::FETCH_ASSOC);

    		return $resultat;
    	} catch (\PDOException $e){
    		throw new \Framework\Exceptions\MyPDOException($e);
    	}
    }

    /**
    * M�thode statique renvoyant un objet de connexion � la BDD en initialisant la connexion au besoin
    *
    * @return PDO object PDO de connexion � la BDD
    * @throws \Framework\Exceptions\MyPDOException
    */
    private static function getBdd()
    {
    	try {
    		// creation de la connexion si elle n'existe pas
    		if(self::$_bdd == NULL){
    			// recupération des parametres de configuration
    			$dsn = Configuration::get("dsn");
    			$login = Configuration::get("login");
    			$mdp = Configuration::get("mdp");

    			// creation de la connexion
    			self::$_bdd = new \PDO($dsn, $login, $mdp, array(\PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION));
    		}
        	return self::$_bdd;
    	} catch (\PDOException $e) {
    		throw new \Framework\Exceptions\MyPDOException($e);
    	}
    }

    /**
     *
     * Méthode supprimerDoublons
     *
     * permet de supprimer les doublons relatifs d'une table
     *
     * @param string $table nom de la table SQL
     * @param string $distinct discriminant des doublons
     * @param array $equalColonnes tableau des colonnes constituant un doublon relatif
     * @throws \InvalidArgumentException
     */
    public function supprimerDoublons($table,$distinct,$equalColonnes)
    {
      if(!empty($table) && is_string($table)){
            $table = strtoupper($table);
            $distinct = strtoupper($distinct);
            $sql = 'delete DOUBLON.* from   T_' .$table .' as DOUBLON'
                . ' inner join (select';
            foreach($equalColonnes as $equalColonne){
                $equalColonne = strtoupper($equalColonne);
                $sql.= ' ' .$equalColonne  .',' ;
            }

            $sql.=' MAX(' .$distinct .') as PLUS_RECENT from T_' .$table
            . ' group by ' .strtoupper($equalColonnes[0])
            . ' having count(*) > 1) as CONSERVER'
            . ' on CONSERVER.PLUS_RECENT <> DOUBLON.' .$distinct;

            foreach($equalColonnes as $equalColonne){
               $equalColonne = strtoupper($equalColonne);
               $sql.=' and CONSERVER.' .$equalColonne  .' = DOUBLON.' .$equalColonne ;
           }
        $requeteSQL = $this->executerRequete($sql, NULL,NULL);
      } else throw new \InvalidArgumentException('nom de table de BDD invalide ou manquant');
    }

    /**
     *
     * Méthode verifIntervalRequete
     *
     * vérifie que l'intervalle de temps entre 2 requêtes d'un même utilisateur d'une même table
     * est supérieur à une valeur définie en configuration
     *
     * @param string $table nom de la table concernée
     * @param int $idUser
     * @return int nombre de lignes dans le résultat de la requête
     * @throws \InvalidArgumentException
     */
    public function verifIntervalRequete($table,$idUser,$interval)
    {
        if(!empty($table)
           && is_string($table) && is_int($idUser)
           && in_array($table,array('commentaire','billet','commentaire_article'))){

            $table = strtoupper($table);

            $sql = 'select count(*) from T_'.$table
                  .' where USER_ID = ? AND ';

            if(strpos($table,'ARTICLE') === FALSE)
                $sql.=  strtoupper(substr($table,0,3));
            else
                $sql.= strtoupper(substr($table,0,3)) .'_ART';

            $sql.= '_DATE > now() - interval ' .$interval .' second';

            $requeteSQL = $this->executerRequete($sql,array($idUser),NULL);
            return (int)$requeteSQL->fetchcolumn();
        } else throw new \InvalidArgumentException('nom de table inconnu ou identifiant incohérent');
    }
}
