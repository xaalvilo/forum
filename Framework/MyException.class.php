<?php
namespace Framework;
require_once './Framework/autoload.php';
/**
 *
 * @author Frédéric Tarreau
 *
 * 7 juil. 2015 - MyException.class.php
 *
 */

class MyException extends \Exception
{
	private $_timestamp;

	public function __construct($message,$code=0, Exception $previous = null)
	{
		parent::__construct($message,$code,$previous);
		$this->_timestamp = date("Y-m-d H:i:s");
	}

	public function getTimestamp()
	{
		return $this->_timestamp;
	}
}

