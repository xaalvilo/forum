<?php
namespace Framework;
require_once './Framework/autoload.php';
/**
 *  la classe Routeur hérite de la classe ApplicationComponent et a pour rôle d'analyser une requête entrante afin d'en déduire le contrôleur à utiliser ainsi que
 *  l'action (methode) à exécuter. Il s'agit d'un contrôleur frontal
 */

class Routeur extends ApplicationComponent
{
    /* constante de classe déterminant la page d'accueil */
    const PAGE_ACCUEIL = "Accueil";

    /* @var controleur généré */
    protected $_controleur;

    /**
     *
     * Méthode controleur
     *
     * getter du controleur généré
     *
     * @return \Framework\Controleur
     */
    public function controleur()
    {
        return $this->_controleur;
    }

    /**
    *
    *  Méthode routerRequête
    *
    *  cette méthode principale appelée par le contrôleur frontal examine la requête et exécute l'action appropriée
    *  elle affiche les éléments d'erreur si une exception est levée au cours des principales étapes de construction de la réponse à la requête
    *
    *  @param Application $app
    */
    public function routerRequete(Application $app)
    {
        try {
        	//throw new \Framework\Exceptions\SecuriteException('Alerte de sécurité',323);
        	//trigger_error('hello');

            // instanciation de l'objet Requête, Fusion des paramètres des tableaux superglobales
            // GET et POST de la requête pour gérer uniformément ces deux types de requête HTTP
            $requete = new Requete($app,array_merge($_GET,$_POST));
            $controleur = $this->creerControleur($requete);
            $action= $this->creerAction($requete);
            $controleur->executerAction($action);

        } catch (\PDOException $e){
        	$this->gererErreur($e);

        } catch (\OutOfRangeException $e){
        		$this->gererErreur($e);

        } catch (\OutOfBoundsException $e){
        		$this->gererErreur($e);

        } catch (\BadMethodCallException $e){
        		$this->gererErreur($e);

        } catch (\InvalidArgumentException $e){
        	$this->gererErreur($e);

        } catch (\BadFunctionCallException $e){
        	$this->gererErreur($e);

        } catch (\RuntimeException $e){
        	switch($e->getCode()){
        		case \Framework\MySessionHandler::SESSION_HANDLER_FAILED :
        			break;
        		case \Framework\Entites\Session::SET_SESSION_LIFETIME_FAILED :
        			break;
        		case \Framework\Entites\Session::SET_COOKIE_FAILED :
        			break;

        		default: echo "a faire";
        	}
        	$this->gererErreur($e);

        } catch (\Framework\Exceptions\SecuriteException $e) {
        	switch($code=$e->getCode()) {
        		case \Framework\SecuriteHandler::NOT_AUTHORIZED :
        			break;
        		case \Framework\SecuriteHandler::NOT_CONNECTED :
        			break;
        		case \Framework\SecuriteHandler::BLACKLIST_CREATION_FAILED :
        			break;
        	}
        	$this->gererErreur($e);

        } catch (\Framework\Exceptions\FileNotFoundException $e){
        	$this->gererErreur($e);

        } catch (\Framework\Exceptions\FileNotWritableException $e){
        	$this->gererErreur($e);

        } catch (\Framework\Exceptions\UploadFileException $e){
        	$this->gererErreur($e);

        } catch (\Framework\Exceptions\FileException $e){
        	$this->gererErreur($e);

        } catch (\Framework\Exceptions\BDDException $e){
        	$this->gererErreur($e);
        } catch (\ErrorException $e){
        	echo 'ok erreur routeur';
        	$this->gererErreur($e);
        } catch (\DomainException $e){
        	$this->gererErreur($e);
        } catch (\Exception $e) {
            $this->gererErreur($e);
        }
    }

    /**
     *
     * Méthode routerInterne
     *
     * permet d'effectuer une redirection entre controleurs
     *
     * @param array $parametres de la requête "interne"
     *
     */
    public function routerInterne(Application $app, $parametres = array())
    {
        try {
            $requete = new Requete($app,$parametres);
            $controleur = $this->creerControleur($requete);
            $action= $this->creerAction($requete);
            $controleur->executerAction($action,$parametres);
        } catch (\Exception $e){
            $this->gererErreur($e);
        }
    }

    /**
    * Méhode privée creerControleur
    *
    * Elle permet de créer le controleur adapté à la requete entrante et renvoyant une instance de la classe
    * associée. Grâce à la redirection dans la fichier .htaccess, toutes les URL entrantes sont du type :
    * index.php?controleur=XXX&action=YYY&id=ZZZ
    *
    * @param Requete $requete Requête reçue
    * @return Instance d'un contrôleur
    * @throws Exception Si la création du contrôleur échoue
    */
    private function creerControleur(Requete $requete)
    {
        // il s'agit de définir le controleur par defaut
        $controleur = self::PAGE_ACCUEIL;

        // puis de créer le nom du controleur à activer
        if ($requete->existeParametre('controleur')){
            $controleur=$requete->getParametre('controleur');

            // mettre la première lettre en majuscule par cohérence à la convention de nommage des fichiers
            // Controleur/Controleur<$controleur>.php , après avoir tout converti en minuscule
            $controleur = ucfirst(strtolower($controleur));
        }

        //  création du nom de la classe controleur correspondante
        $classeControleur = "Controleur".$controleur;

        // création du nom du fichier du controleur dans le répertoire suivant :
        // Applications/$nomApplication/Modules/$controleur/ de notre arborescence
        // cela nécessite la récuperation de l'application ex�cut�e
        $app = $requete->app();
        $nomApplication = $app->nom();

        $repertoireControleur = "Applications/".$nomApplication."/Modules/".$controleur."/";

        $fichierControleur =  $repertoireControleur.$classeControleur.".class.php" ;

        // si ce fichier existe, instanciation de l'objet controleur associé à l'action
        if (file_exists($fichierControleur)){
            // création de l'espace de nom correspondant
            $NamespaceClasseControleur = "\\Applications\\".$nomApplication."\\Modules\\".$controleur."\\".$classeControleur;

            // instanciation du controleur adapté à la requête
            $controleur = new $NamespaceClasseControleur($app);

            // y attacher les observers (design pattern observer)
            $controleur->attach($this->_app->securiteHandler());
            $this->_controleur = $controleur;

            // la requete en paramètre est associée à ce controleur */
            $controleur->setRequete($requete);
            return $controleur ;
        } else throw new \Framework\Exceptions\FileNotFoundException ("Fichier '$fichierControleur' introuvable");
    }

    /**
    *
    *  Méthode creerAction
    *
    *  Méthode permettant de déterminer l'action à exécuter en fonction de la requete entrante
    *
    * @param Requete $requete Requête reçue
    * @return string $action Action à exécuter
    */
    private function creerAction (Requete $requete)
    {
        $action = "index" ;//index est toujours l'action par defaut
        if ($requete->existeParametre('action'))
            $action = $requete->getParametre('action');
        return $action ;
    }

    /**
    *
    *  Méthode gererErreur
    *
    *  Méthode permettant de gérer les erreurs d'exécution et d'afficher les messages dans une vue dédiée aux erreurs
    *
    * @param \Exception $exception attrapée
    *
    */
    private function gererErreur (\Exception $exception)
    {
        // creation de la page de réponse
        $page = new Page($this->_app,'erreur');

        // TODO supprimer cette gestion de l'affichage des traces ligne par ligne
        $separator = '#';
        $one_line = str_replace($separator, "<br/>#", $exception->getTraceAsString());

        $params = array('code'=>$exception->getCode(),
        			'message'=>$exception->getMessage(),
        			'file'=>$exception->getFile(),
        			'line'=>$exception->getLine(),
        			'trace'=>$one_line,
        			'previous'=>$exception->getPrevious());

        if ($exception instanceof MyException)
        	$params['timestamp']=$exception->getTimestamp();

        if ($exception instanceof \ErrorException)
        	$params['severity']=$exception->getSeverity();

        // génération de la vue spécifique avec les données propres à l'erreur
        $vue = $page->generer($params);
        $this->_app->httpReponse()->send($vue);
    }
}
