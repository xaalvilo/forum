﻿<?php
//Portion de Vue spécifique à l'affichage d'une erreur
$this->_titre = "Erreur AtelierBlancNordOuest-Erreur";?>
<?php if(isset($timestamp)):?>
	<p>Time : <?=$this->nettoyer($timestamp);?><p>
<?php endif;?>
	<p>Code : <?=$this->nettoyer($code);?><p>
<?php if(isset($severity)):?>
	<p>Severity : <?=$this->nettoyer($severity);?><p>
<?php endif;?>
	<p>Message : <?=$this->nettoyer($message);?><p>
	<p>File : <?=$this->nettoyer($file);?><p>
	<p>Line : <?=$this->nettoyer($line);?><p>
	<p>Trace :<?=$this->nettoyer($trace);?><p>
	<p>Previous :<?=$this->nettoyer($previous);?><p>

